import { mapState } from 'vuex'
import IndexNames from '@/web-components-submodule/standardisation/IndexNames.js'
import DatasetStoreGenerator from '~/web-components-submodule/store/datasets/generators/DatasetStoreGenerator.js'
import EntitiesJoin from '~/web-components-submodule/standardisation/datasets/EntitiesJoin.js'
import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'

export const datasetMixin = {
  created() {
    const moduleWithSameNameExists = this.$store.hasModule(this.storeModuleName)
    if (moduleWithSameNameExists) {
      // If a module with the same name exists just unregister it and create it from scratch.
      // This is to avoid having werid behaviours with the datasets. It is responsibility of the code that
      // generates this dataset component to use unique names for the store module
      this.$store.unregisterModule(this.storeModuleName)
    }
    const theModule = DatasetStoreGenerator.generateDatasetStoreModule()
    this.$store.registerModule(this.storeModuleName, theModule)
  },
  props: {
    rawState: {
      type: String,
      default: () => undefined,
    },
    initialStateSavingParams: {
      type: Object,
      default: () => undefined,
    },
  },
  computed: {
    ...mapState({
      datasetState(state) {
        return state[this.storeModuleName]
      },
    }),
    parsedState() {
      if (this.rawState == null) {
        return undefined
      }
      const strState = Buffer.from(this.rawState, 'base64').toString('binary')
      const parsedState = JSON.parse(strState)
      return parsedState
    },
    starterParams() {
      const dataSetParamsFromState =
        this.parsedState == null ? null : this.parsedState.dataset

      // the drugs are a special case, they are in the chembl_molecule_index but they need a query to be able to filter them
      if (
        this.entityID === EntityNames.Drug.entityID &&
        dataSetParamsFromState != null
      ) {
        const initialQuery = dataSetParamsFromState.initialQuery

        if (initialQuery != null) {
          if (initialQuery.query == null) {
            initialQuery.query = {}
          }

          if (initialQuery.query.bool == null) {
            initialQuery.query.bool = {}
          }

          if (initialQuery.query.bool.must == null) {
            initialQuery.query.bool.must = []
          }

          initialQuery.query.bool.must.push({
            term: {
              '_metadata.drug.is_drug': true,
            },
          })
        }
      }

      // the precedence is: 1. parsedState, 2. value definded in the component
      const starterParams = {
        indexName: IndexNames.getIndexNameFromEntityID(this.entityID),
        propertiesGroups: this.propertiesGroups,
        facetsGroups: 'browser_facets',
        contextObj: this.contextObj,
      }
      for (const paramKey of [
        'initialQuery',
        'subsetHumanDescription',
        'initialFacetsState',
        'customFiltering',
        'quickSearchTerm',
        'exactTextFilters',
        'initialSort',
        'initialSortDesc',
      ]) {
        // this special case is because the key in the state is different for backwards compatibility
        const originKey =
          paramKey === 'initialFacetsState' ? 'facetsState' : paramKey
        if (dataSetParamsFromState != null) {
          if (dataSetParamsFromState[originKey] != null) {
            starterParams[paramKey] = dataSetParamsFromState[originKey]
            continue
          }
        }
        if (this[paramKey] != null) {
          starterParams[paramKey] = this[paramKey]
        }
      }

      if (starterParams.initialQuery == null) {
        starterParams.initialQuery = {}
      }

      return starterParams
    },
  },
  watch: {
    // if the starter params change after the initial load
    // trigger again the load to update the dataset.
    starterParams: {
      handler(newValue, oldValue) {
        const initialLoad = this.datasetState.initialLoad
        const itReallyChanged =
          JSON.stringify(oldValue) !== JSON.stringify(newValue)
        if (initialLoad && itReallyChanged) {
          this.triggerInitialLoad()
        }
      },
      deep: true,
    },
  },
  mounted() {
    this.triggerInitialLoad()
  },
  methods: {
    triggerInitialLoad() {
      this.$store.dispatch(`${this.storeModuleName}/setInitialDataNotLoaded`)

      const featuresAndSetters = [
        {
          feature: 'customDataRepresentationParams',
          setter: 'setCustomRepresentationParams',
        },
        {
          feature: 'querystringExamples',
          setter: 'setQuerystringExamples',
        },
        {
          feature: 'enableSelection',
          setter: 'setEnableSelection',
        },
        {
          feature: 'enableMultiSort',
          setter: 'setEnableMultiSort',
        },
        {
          feature: 'enableSimilarityMaps',
          setter: 'setEnableSimilarityMaps',
        },
        {
          feature: 'enableSubstructureHighlight',
          setter: 'setEnableSubstructureHighlight',
        },
        {
          feature: 'similarityMapsReferenceStructure',
          setter: 'setSimilarityMapsReferenceStructure',
        },
        {
          feature: 'substructureHighlightReferenceStructure',
          setter: 'setSubstructureHighlightReferenceStructure',
        },
        {
          feature: 'enableStateSaving',
          setter: 'setEnableStateSaving',
        },
        {
          feature: 'enableHeatmapGeneration',
          setter: 'setEnableHeatmapGeneration',
        },
        {
          feature: 'enableQuickTextSearch',
          setter: 'setEnableQuickTextSearch',
        },
        {
          feature: 'subsetHumanDescription',
          setter: 'setSubsetHumanDescription',
        },
        {
          feature: 'downloadPropertiesGroup',
          setter: 'setDownloadPropertiesGroup',
        },
        {
          feature: 'searchTerm',
          setter: 'setSearchTerm',
        },
        {
          feature: 'currentView',
          setter: 'setCurrentView',
        },
      ]

      for (const featureConfig of featuresAndSetters) {
        const feature = featureConfig.feature
        const setter = featureConfig.setter
        if (this[feature] != null) {
          this.$store.dispatch(
            `${this.storeModuleName}/${setter}`,
            this[feature]
          )
        }
      }

      // This is always retuired so it can't be null
      this.$store.dispatch(`${this.storeModuleName}/setEntityID`, this.entityID)

      const possibleJoinDestinations = EntitiesJoin.getPossibleDestinationEntityIDsAndNames(
        this.entityID
      ).map((item) => {
        return {
          ...item,
          processing: false,
        }
      })

      this.$store.dispatch(
        `${this.storeModuleName}/setPossibleJoinDestinations`,
        possibleJoinDestinations
      )

      this.$store.dispatch(
        `${this.storeModuleName}/setPluralEntityName`,
        this.pluralEntityName
      )

      this.$store.dispatch(
        `${this.storeModuleName}/fireItUp`,
        this.starterParams
      )

      this.$store.dispatch(
        `${this.storeModuleName}/setEmbeddingIDForParent`,
        this.idForParent
      )

      if (this.initialStateSavingParams != null) {
        this.$store.dispatch(
          `${this.storeModuleName}/setStateSavingParams`,
          this.initialStateSavingParams
        )

        const shouldGetHash =
          this.initialStateSavingParams.useHashInURL &&
          this.initialStateSavingParams.sateHash == null

        if (shouldGetHash) {
          this.$store.dispatch(`${this.storeModuleName}/getHashForCurrentState`)
        }
      }

      // add SDF Downloads for Compounds
      if (this.entityID === EntityNames.Compound.entityID) {
        this.$store.dispatch(`${this.storeModuleName}/addDownloadFormat`, 'SDF')
      }
    },
  },
}
