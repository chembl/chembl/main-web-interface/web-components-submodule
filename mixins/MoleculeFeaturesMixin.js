import ObjectPropertyAccess from '~/web-components-submodule/utils/ObjectPropertyAccess.js'

export const MoleculeFeaturesMixin = {
  data() {
    return {
      allPossibleFeatures: {
        row1: {
          features: {
            natural_product: {
              label: 'Natural Product',
              position: 2,
              possible_values: {
                // '1': ['active', 'Natural Product: Yes', 'top', 'natural_product']
                1: {
                  label: 'Yes',
                  iconFilename: 'natural.svg',
                  queryString: 'natural_product:1',
                  active: true,
                },
                // '0': ['off', 'Natural Product: No', 'top', 'natural_product']
                0: {
                  label: 'No',
                  iconFilename: 'natural.svg',
                  queryString: 'natural_product:0',
                  active: false,
                },
              },
              valueGetter: (compoundSource) => {
                return ObjectPropertyAccess.getPropertyPalue(
                  compoundSource,
                  'natural_product',
                  undefined,
                  true
                )
              },
            },
            chemical_probe: {
              label: 'Chemical Probe',
              position: 3,
              possible_values: {
                1: {
                  label: 'Yes',
                  iconFilename: 'chemical_probe.svg',
                  queryString: 'chemical_probe:1',
                  active: true,
                },
                0: {
                  label: 'No',
                  iconFilename: 'chemical_probe.svg',
                  queryString: 'chemical_probe:1',
                  active: false,
                },
              },
              valueGetter: (compoundSource) => {
                return ObjectPropertyAccess.getPropertyPalue(
                  compoundSource,
                  'chemical_probe',
                  undefined,
                  true
                )
              },
            },
            ro5: {
              label: 'Rule Of Five',
              position: 4,
              // 'true': ['active', 'Rule Of Five: Yes', 'top', 'rule_of_five']
              // 'false': ['off', 'Rule Of Five: No', 'top', 'rule_of_five']
              possible_values: {
                true: {
                  label: 'Yes',
                  iconFilename: 'rule_of_five.svg',
                  queryString:
                    '_exists_:molecule_properties.num_ro5_violations AND molecule_properties.num_ro5_violations:<=1',
                  active: true,
                },
                false: {
                  label: 'No',
                  iconFilename: 'rule_of_five.svg',
                  queryString:
                    '(NOT _exists_:molecule_properties.num_ro5_violations) OR molecule_properties.num_ro5_violations:>1',
                  active: false,
                },
              },
              valueGetter: (compoundSource) => {
                const numViolations = ObjectPropertyAccess.getPropertyPalue(
                  compoundSource,
                  'molecule_properties.num_ro5_violations',
                  undefined,
                  true
                )
                return numViolations != null && numViolations <= 1
              },
            },
            molecule_type: {
              label: 'Molecule Type',
              position: 1,
              possible_values: {
                'Small molecule': {
                  label: 'Small Molecule',
                  iconFilename: 'small-molecule.svg',
                  queryString: 'molecule_type:"Small molecule"',
                  active: true,
                },
                'Small molecule polymer': {
                  label: 'Small Molecule Polymer',
                  iconFilename: 'polymer.svg',
                  queryString: 'polymer_flag:1',
                  active: true,
                },
                // 'Antibody': ['active', 'Antibody', 'top', 'antibody']
                Antibody: {
                  label: 'Antibody',
                  iconFilename: 'antibody.svg',
                  queryString: 'molecule_type:"Antibody"',
                  active: true,
                },
                // 'Enzyme': ['active', 'Enzyme', 'top', 'enzyme']
                Enzyme: {
                  label: 'Enzyme',
                  iconFilename: 'enzyme.svg',
                  queryString: 'molecule_type:"Enzyme"',
                  active: true,
                },
                // 'Oligosaccharide': ['active', 'Oligosaccharide', 'top', 'oligosaccharide']
                Oligosaccharide: {
                  label: 'Oligosaccharide',
                  iconFilename: 'oligosaccharide.svg',
                  queryString: 'molecule_type:"Oligosaccharide"',
                  active: true,
                },
                // 'Protein': ['active', 'Oligopeptide', 'top', 'protein']
                Protein: {
                  label: 'Oligopeptide',
                  iconFilename: 'protein.svg',
                  queryString: 'molecule_type:"Protein"',
                  active: true,
                },
                // 'Oligonucleotide': ['active', 'Oligonucleotide', 'top', 'oligonucleotide']
                Oligonucleotide: {
                  label: 'Oligonucleotide',
                  iconFilename: 'oligonucleotide.svg',
                  queryString: 'molecule_type:"Oligonucleotide"',
                  active: true,
                },
                // 'Cell': ['active', 'Cell Based', 'top', 'cell']
                Cell: {
                  label: 'Cell Based',
                  iconFilename: 'cell.svg',
                  queryString: 'molecule_type:"Cell"',
                  active: true,
                },
                // 'Unknown': ['active', 'Unknown', 'top', 'unknown']
                Unknown: {
                  label: 'Unknown',
                  iconFilename: 'unknown.svg',
                  queryString: 'molecule_type:"Unknown"',
                  active: true,
                },
                // 'Inorganic': ['active', 'Inorganic','top', 'inorganic']
                Inorganic: {
                  label: 'Inorganic',
                  iconFilename: 'inorganic.svg',
                  queryString: 'inorganic_flag:1',
                  active: true,
                },
                // 'Gene': ['active', 'Gene','top', 'gene']
                Gene: {
                  label: 'Gene',
                  iconFilename: 'gene.svg',
                  queryString: 'molecule_type:"Gene"',
                  active: true,
                },
                'Antibody drug conjugate': {
                  label: 'Antibody Drug Conjugate',
                  iconFilename: 'antibody_drug_conjugate.svg',
                  queryString: 'molecule_type:"Antibody drug conjugate"',
                  active: true,
                },
              },
              valueGetter: (compoundSource) => {
                const moleculeType = ObjectPropertyAccess.getPropertyPalue(
                  compoundSource,
                  'molecule_type',
                  undefined,
                  true
                )

                const polymerFlag = ObjectPropertyAccess.getPropertyPalue(
                  compoundSource,
                  'polymer_flag',
                  undefined,
                  true
                )

                if (polymerFlag === 1) {
                  return 'Small molecule polymer'
                }

                const inorganicFlag = ObjectPropertyAccess.getPropertyPalue(
                  compoundSource,
                  'inorganic_flag',
                  undefined,
                  true
                )

                if (inorganicFlag === 1) {
                  return 'Inorganic'
                }

                return moleculeType
              },
            },
            first_in_class: {
              label: 'First in Class',
              position: 5,
              possible_values: {
                // '1': ['active', 'First in Class: Yes', 'top', 'first_in_class']
                1: {
                  label: 'Yes',
                  iconFilename: 'first_in_class.svg',
                  queryString: 'first_in_class:1',
                  active: true,
                },
                // '0': ['off', 'First in Class: No', 'top', 'first_in_class']
                0: {
                  label: 'No',
                  iconFilename: 'first_in_class.svg',
                  queryString: 'first_in_class:0',
                  active: false,
                },
                // '-1': ['off', 'First in Class: Undefined', 'top', 'first_in_class']
                '-1': {
                  label: 'Undefined',
                  iconFilename: 'first_in_class.svg',
                  queryString: 'first_in_class:"-1"',
                  active: false,
                },
              },
              valueGetter: (compoundSource) => {
                return ObjectPropertyAccess.getPropertyPalue(
                  compoundSource,
                  'first_in_class',
                  undefined,
                  true
                )
              },
            },
            chirality: {
              label: 'Chirality',
              position: 6,
              // '-1': ['off', 'Chirality: Undefined', 'top', 'racemic_mixture']
              // '0': ['active', 'Chirality: Racemic Mixture', 'top', 'racemic_mixture']
              // '1': ['active', 'Chirality: Single Stereoisomer', 'top', 'chirally_pure']
              // '2': ['off', 'Chirality: Achiral Molecule', 'top', 'chirally_pure']
              possible_values: {
                '-1': {
                  label: 'Undefined',
                  iconFilename: 'racemic_mixture.svg',
                  queryString: 'chirality:"-1"',
                  active: false,
                },
                0: {
                  label: 'Racemic Mixture',
                  iconFilename: 'racemic_mixture.svg',
                  queryString: 'chirality:0',
                  active: true,
                },
                1: {
                  label: 'Single Stereoisomer',
                  iconFilename: 'chirally_pure.svg',
                  queryString: 'chirality:1',
                  active: true,
                },
                2: {
                  label: 'Achiral Molecule',
                  iconFilename: 'chirally_pure.svg',
                  queryString: 'chirality:2',
                  active: false,
                },
              },
              valueGetter: (compoundSource) => {
                return ObjectPropertyAccess.getPropertyPalue(
                  compoundSource,
                  'chirality',
                  undefined,
                  true
                )
              },
            },
            prodrug: {
              label: 'Prodrug',
              position: 7,
              // '-1': ['off', 'Prodrug: Undefined', 'top', 'prodrug']
              // '0': ['off', 'Prodrug: No', 'top', 'prodrug']
              // '1': ['active', 'Prodrug: Yes',  'top', 'prodrug']
              possible_values: {
                '-1': {
                  label: 'Undefined',
                  iconFilename: 'prodrug.svg',
                  queryString: 'prodrug:"-1"',
                  active: false,
                },
                0: {
                  label: 'No',
                  iconFilename: 'prodrug.svg',
                  queryString: 'prodrug:0',
                  active: false,
                },
                1: {
                  label: 'Yes',
                  iconFilename: 'prodrug.svg',
                  queryString: 'prodrug:1',
                  active: true,
                },
              },
              valueGetter: (compoundSource) => {
                return ObjectPropertyAccess.getPropertyPalue(
                  compoundSource,
                  'prodrug',
                  undefined,
                  true
                )
              },
            },
          },
        },
        row2: {
          features: {
            oral: {
              label: 'Oral',
              position: 1,
              possible_values: {
                // 'true': ['active', 'Oral: Yes', 'bottom', 'oral']
                true: {
                  label: 'Yes',
                  iconFilename: 'oral.svg',
                  queryString: 'oral:true',
                  active: true,
                },
                // 'false': ['off', 'Oral: No', 'bottom', 'oral']
                false: {
                  label: 'No',
                  iconFilename: 'oral.svg',
                  queryString: 'oral:false',
                  active: false,
                },
              },
              valueGetter: (compoundSource) => {
                return ObjectPropertyAccess.getPropertyPalue(
                  compoundSource,
                  'oral',
                  undefined,
                  true
                )
              },
            },
            parenteral: {
              label: 'Parenteral',
              position: 2,
              // 'true': ['active', 'Parenteral: Yes', 'bottom', 'parenteral']
              // 'false': ['off', 'Parenteral: No', 'bottom', 'parenteral']
              possible_values: {
                true: {
                  label: 'Yes',
                  iconFilename: 'parenteral.svg',
                  queryString: 'parenteral:true',
                  active: true,
                },
                false: {
                  label: 'No',
                  iconFilename: 'parenteral.svg',
                  queryString: 'parenteral:false',
                  active: false,
                },
              },
              valueGetter: (compoundSource) => {
                return ObjectPropertyAccess.getPropertyPalue(
                  compoundSource,
                  'parenteral',
                  undefined,
                  true
                )
              },
            },
            topical: {
              label: 'Topical',
              position: 3,
              // 'true': ['active', 'Topical: Yes', 'bottom', 'topical']
              // 'false': ['off', 'Topical: No', 'bottom', 'topical']
              possible_values: {
                true: {
                  label: 'Yes',
                  iconFilename: 'topical.svg',
                  queryString: 'topical:true',
                  active: true,
                },
                false: {
                  label: 'No',
                  iconFilename: 'topical.svg',
                  queryString: 'topical:false',
                  active: false,
                },
              },
              valueGetter: (compoundSource) => {
                return ObjectPropertyAccess.getPropertyPalue(
                  compoundSource,
                  'topical',
                  undefined,
                  true
                )
              },
            },
            black_box_warning: {
              label: 'Black Box',
              position: 4,
              // '0': ['off', 'Black Box: No', 'bottom', 'black_box']
              // '1': ['active', 'Black Box: Yes', 'bottom', 'black_box']
              possible_values: {
                0: {
                  label: 'No',
                  iconFilename: 'black_box.svg',
                  queryString: 'black_box:0',
                  active: false,
                },
                1: {
                  label: 'Yes',
                  iconFilename: 'black_box.svg',
                  queryString: 'black_box:1',
                  active: true,
                },
              },
              valueGetter: (compoundSource) => {
                return ObjectPropertyAccess.getPropertyPalue(
                  compoundSource,
                  'black_box_warning',
                  undefined,
                  true
                )
              },
            },
            availability_type: {
              label: 'Availability',
              position: 5,
              // '-2': ['active', 'Availability: Withdrawn', 'bottom', 'withdrawn']
              // '-1': ['off', 'Availability: Undefined', 'bottom', 'prescription']
              // '0': ['active', 'Availability: Discontinued', 'bottom', 'discontinued']
              // '1': ['active', 'Availability: Prescription Only', 'bottom', 'prescription']
              // '2': ['active', 'Availability: Over the Counter', 'bottom', 'otc']
              possible_values: {
                '-2': {
                  label: 'Withdrawn',
                  iconFilename: 'withdrawn.svg',
                  queryString: 'availability_type:"-2"',
                  active: true,
                },
                '-1': {
                  label: 'Undefined',
                  iconFilename: 'prescription.svg',
                  queryString: 'availability_type:"-1"',
                  active: false,
                },
                0: {
                  label: 'Discontinued',
                  iconFilename: 'discontinued.svg',
                  queryString: 'availability_type:0',
                  active: true,
                },
                1: {
                  label: 'Prescription Only',
                  iconFilename: 'prescription.svg',
                  queryString: 'availability_type:1',
                  active: true,
                },
                2: {
                  label: 'Over the Counter',
                  iconFilename: 'otc.svg',
                  queryString: 'availability_type:2',
                  active: true,
                },
              },
              valueGetter: (compoundSource) => {
                return ObjectPropertyAccess.getPropertyPalue(
                  compoundSource,
                  'availability_type',
                  undefined,
                  true
                )
              },
            },
          },
        },
      },
    }
  },
}
