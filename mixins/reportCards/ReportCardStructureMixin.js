import { mapState } from 'vuex'
import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'

export const ReportCardStructureMixin = {
  computed: {
    ...mapState({
      reportCardStructure(state) {
        return state[this.storeModuleName].reportCardStructure
      },
    }),
  },

  methods: {
    setFinalPageStructure(reportCardTitle, basePageStructure) {
      const finalPageStructure = {
        title: reportCardTitle,
        entityName: EntityNames[this.entityID].singularEntityName,
        sections: [],
      }

      let index = 0

      for (const section of basePageStructure) {
        if (section.include) {
          section.index = index
          finalPageStructure.sections.push(section)
          index++
        }
      }

      this.$store.dispatch(
        `${this.storeModuleName}/setReportCardStructure`,
        finalPageStructure
      )
      this.$store.dispatch(`${this.storeModuleName}/setStructureReady`, true)
    },
  },
}
