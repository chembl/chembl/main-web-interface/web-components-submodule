import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'

export const ReportCardMixin = {
  computed: {
    chemblID() {
      return this.$route.params.chembl_id
    },
    titleText() {
      const titleText = `${EntityNames[this.entityID].singularEntityName}: ${
        this.idText
      }`
      const maxLength = 149
      return titleText.length > maxLength
        ? titleText.slice(0, maxLength) + '...'
        : titleText
    },
    canonicalURL() {
      return `https://${process.env.canonicalDomain}` + this.$route.path
    },
  },
  head() {
    const head = {
      title: this.titleText,
      meta: [
        {
          hid: 'description',
          name: 'description',
          content: this.descriptionText,
        },
        { hid: 'og:title', name: 'og:title', content: this.titleText },
        {
          hid: 'og:description',
          name: 'og:description',
          content: this.descriptionText,
        },
        { hid: 'og:type', name: 'og:type', content: 'object' },
        { hid: 'og:url', name: 'og:url', content: this.canonicalURL },
      ],
      link: [
        {
          rel: 'canonical',
          href: this.canonicalURL,
        },
      ],
    }
    if (this.bioschemasMetadata != null) {
      if (head.script == null) {
        head.script = []
      }
      head.script.push({
        id: 'JSON_LD',
        type: 'application/ld+json',
        json: this.bioschemasMetadata,
      })
    }

    return head
  },
}
