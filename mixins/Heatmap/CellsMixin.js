import HeatmapStd from '~/web-components-submodule/standardisation/HeatmapStd.js'
import ObjectPropertyAccess from '~/web-components-submodule/utils/ObjectPropertyAccess.js'

export const CellsMixin = {
  computed: {
    xAxisVisualWindow() {
      return this.heatmapDatasetState.visualWindow[
        HeatmapStd.axesNames.xAxis.axisID
      ]
    },
    yAxisVisualWindow() {
      return this.heatmapDatasetState.visualWindow[
        HeatmapStd.axesNames.yAxis.axisID
      ]
    },
    dataQuadrants() {
      return this.heatmapDatasetState[HeatmapStd.axesNames.cells.axisID]
        .dataQuadrants
    },
    quadrantSideLength() {
      return this.heatmapDatasetState[HeatmapStd.axesNames.cells.axisID]
        .quadrantSideLength
    },
    xDataLimit() {
      return this.heatmapDatasetState.xAxis.totalItems - 1
    },
    yDataLimit() {
      return this.heatmapDatasetState.yAxis.totalItems - 1
    },
    currentCellProperty() {
      const cellProperties = this.heatmapDatasetState.description.cells
        .properties
      const currentCellPropertyIndex = this.heatmapDatasetState.cells
        .currentPropertyIndex
      return cellProperties[currentCellPropertyIndex]
    },
    currentPropertyRange() {
      const ranges = this.heatmapDatasetState.cells.contextualData.ranges
      return ObjectPropertyAccess.getPropertyPalue(
        ranges,
        this.currentCellProperty,
        undefined,
        true
      )
    },
  },
  methods: {
    getCell(y, x) {
      // cells outside the data range are unknowns
      if (y < 0 || x < 0 || x > this.xDataLimit || y > this.yDataLimit) {
        return {
          y,
          x,
          isUnknown: true,
        }
      }

      // get to which data row should it belong
      const dataRowIndex = Math.floor(y / this.quadrantSideLength)
      const dataRowChunkStart = dataRowIndex * this.quadrantSideLength
      const dataRowChunk = this.dataQuadrants[dataRowChunkStart]

      if (dataRowChunk == null) {
        return {
          y,
          x,
          isUnknown: true,
        }
      }

      const dataColIndex = Math.floor(x / this.quadrantSideLength)
      const dataColChunkStart = dataColIndex * this.quadrantSideLength
      const dataColChunk = dataRowChunk[dataColChunkStart]

      if (dataColChunk == null) {
        return {
          y,
          x,
          isUnknown: true,
        }
      }

      if (dataColChunk.pending) {
        return {
          y,
          x,
          isPending: true,
        }
      }

      if (dataColChunk.loading) {
        return {
          y,
          x,
          isLoading: true,
        }
      }

      if (dataColChunk.error) {
        return {
          y,
          x,
          isErrored: true,
        }
      }

      const actualQuadrantWidth = this.getActualQuadrantWidth(x)

      const relativeY = y % this.quadrantSideLength
      const relativeX = x % this.quadrantSideLength
      const indexInChunk = relativeY * actualQuadrantWidth + relativeX

      return {
        y,
        x,
        ...dataColChunk.items[indexInChunk],
      }
    },
    getActualQuadrantWidth(x) {
      // this takes into account that the last quadrants might not be a square of side length this.quadrantSideLength
      const totalItemsX = this.heatmapDatasetState.xAxis.totalItems
      const numFullChunksX = Math.floor(totalItemsX / this.quadrantSideLength)
      const lastFullChunkX = numFullChunksX * this.quadrantSideLength - 1
      return x <= lastFullChunkX
        ? this.quadrantSideLength
        : totalItemsX % this.quadrantSideLength
    },
  },
}
