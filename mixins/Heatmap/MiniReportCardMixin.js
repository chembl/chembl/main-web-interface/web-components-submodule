import RequestNotifications from '@/web-components-submodule/utils/RequestNotifications.js'
import HeatmapStd from '~/web-components-submodule/standardisation/HeatmapStd.js'
import IndexNames from '~/web-components-submodule/standardisation/IndexNames.js'
import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'
import ObjectPropertyAccess from '~/web-components-submodule/utils/ObjectPropertyAccess.js'

export const MiniReportCardMixin = {
  props: {
    itemID: {
      type: String,
      default: () => '',
    },
    heatmapDatasetState: {
      type: Object,
      default: () => {},
    },
    heatmapStoreModuleName: {
      type: String,
      default: () => `heatmap`,
    },
    axisID: {
      type: String,
      default: () => HeatmapStd.axesNames.yAxis.axisID,
    },
  },
  data() {
    return {
      loadingPropsDescription: true,
      loadingItemData: true,
      propsDescription: {},
      itemData: {},
    }
  },
  computed: {
    miniReportCardProperties() {
      return this.heatmapDatasetState.description[this.axisID].headers
        .mini_report_card_properties
    },
    entityID() {
      return this.heatmapDatasetState.description[this.axisID].entityID
    },
    loading() {
      return this.loadingPropsDescription || this.loadingItemData
    },
  },
  watch: {
    itemID: {
      immediate: true,
      handler(newValue, oldValue) {
        this.loadData()
      },
    },
  },
  methods: {
    loadData() {
      this.resetData()

      const indexName = IndexNames.getIndexNameFromEntityID(this.entityID)

      ESProxyService.getPopertyListConfiguration(
        indexName,
        this.miniReportCardProperties
      )
        .then((response) => {
          this.propsDescription = response.data
          this.loadingPropsDescription = false
        })
        .catch((error) => {
          RequestNotifications.dispatchRequestErrorNotification(
            error,
            this.$store.dispatch,
            `There was an error while loading the properties labels for the details card`
          )
        })

      ESProxyService.getESDocument(
        indexName,
        this.itemID,
        this.miniReportCardProperties
      )
        .then((response) => {
          this.itemData = response.data._source
          this.loadingItemData = false
        })
        .catch((error) => {
          RequestNotifications.dispatchRequestErrorNotification(
            error,
            this.$store.dispatch,
            `There was an error while loading the details for for ${this.itemID}`
          )
        })
    },
    resetData() {
      this.loadingPropsDescription = true
      this.loadingItemData = true
      this.propsDescription = {}
      this.itemData = {}
    },
    getLabelForProperty(propID) {
      const propDescription = this.propsDescription.find(
        (item) => item.prop_id === propID
      )

      return propDescription == null ? '' : propDescription.label
    },
    getBooleanTextValue(propID) {
      const rawValue = ObjectPropertyAccess.getPropertyPalue(
        this.itemData,
        propID,
        false
      )
      return rawValue ? 'yes' : 'no'
    },
    getTextValue(propID) {
      const textValue = ObjectPropertyAccess.getPropertyPalue(
        this.itemData,
        propID
      )
      return textValue
    },
  },
}
