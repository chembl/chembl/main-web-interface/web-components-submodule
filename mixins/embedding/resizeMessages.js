export const resizeMessagesMixin = {
  props: {
    idForParent: {
      type: String,
      default: () => undefined,
    },
  },
  methods: {
    handleResize(resizeData) {
      const msgData = {
        idForParent: this.idForParent,
        ...resizeData,
      }
      window.parent.postMessage(
        { msg: 'EMBEDDED_VUE_RESIZED', data: msgData },
        process.env.iframeTargetOrigin
      )
    },
  },
}
