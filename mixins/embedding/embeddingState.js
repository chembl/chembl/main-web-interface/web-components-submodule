import { mapState } from 'vuex'

export const embeddingStateMixin = {
  props: {
    idForParent: {
      type: String,
      default: () => undefined,
    },
  },
  computed: mapState({
    isEmbedded: (state) => state.embedding.isEmbedded,
  }),
}
