import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'

export const entityBrowserMixin = {
  async asyncData({ $axios, route, error, params }) {
    try {
      const rawState = params.state
      // ------------------------------------------------------------
      // When no state was passed
      // ------------------------------------------------------------
      if (rawState == null) {
        return {
          rawState: undefined,
          initialStateSavingParams: {
            useHashInURL: false,
            loadingHash: false,
            sateHash: undefined,
            b64State: undefined,
            expires: undefined,
          },
        }
      }

      // ------------------------------------------------------------
      // When it is a querystring. It simulates the state that it should start with.
      // ------------------------------------------------------------

      const queryStringPrefix = 'QUERYSTRING:'
      const isQueryString = rawState.startsWith(queryStringPrefix)

      if (isQueryString) {
        const queryString = rawState
          .replace(queryStringPrefix, '')
          .replace('__SLASH__', '/')
        const desiredState = {
          dataset: {
            customFiltering: queryString,
            initialQuery: {
              query: {
                bool: {
                  must: [
                    {
                      query_string: {
                        query: queryString,
                      },
                    },
                  ],
                },
              },
            },
            subsetHumanDescription: route.query.description,
          },
        }
        const strState = JSON.stringify(desiredState)
        const b64State = Buffer.from(strState, 'binary').toString('base64')

        const initialState = {
          initialStateSavingParams: {
            useHashInURL: true,
            loadingHash: true,
            sateHash: undefined,
            b64State,
            expires: undefined,
          },
          rawState: b64State,
        }

        return initialState
      }

      const stateIDPrefix = 'STATE_ID:'
      const isFullState = !rawState.startsWith(stateIDPrefix)

      // ------------------------------------------------------------
      // When it is a raw full state
      // ------------------------------------------------------------
      if (isFullState) {
        return {
          initialStateSavingParams: {
            useHashInURL: true,
            loadingHash: true,
            sateHash: undefined,
            b64State: undefined,
            expires: undefined,
          },
          rawState,
        }
      } else {
        // ------------------------------------------------------------
        // When it is a  state id
        // ------------------------------------------------------------
        const hash = rawState.replace(stateIDPrefix, '')
        const hashParams = await ESProxyService.getLongURLForHash(hash)
        const expires = hashParams.data.expires
        const state = hashParams.data.long_url

        return {
          initialStateSavingParams: {
            useHashInURL: true,
            loadingHash: false,
            sateHash: hash,
            b64State: state,
            expires,
          },
          rawState: state,
        }
      }
    } catch (e) {
      error(e)
    }
  },
}
