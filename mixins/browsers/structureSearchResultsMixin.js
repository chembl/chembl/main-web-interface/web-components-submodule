import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'
import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'
import IndexNames from '~/web-components-submodule/standardisation/IndexNames.js'
import ObjectPropertyAccess from '~/web-components-submodule/utils/ObjectPropertyAccess.js'

export const structureSearchResultsMixin = {
  data() {
    return {
      smiles: undefined,
      loading: true,
      showError: false,
      errorMsg: undefined,
    }
  },
  methods: {
    async loadSmilesFromChEMBLID() {
      const entityID = EntityNames.Compound.entityID
      const indexName = IndexNames.getIndexNameFromEntityID(entityID)
      const chemblID = this.term

      try {
        const docResponse = await ESProxyService.getESDocument(
          indexName,
          chemblID,
          ['molecule_structures.canonical_smiles']
        )

        const docData = docResponse.data._source
        this.smiles = ObjectPropertyAccess.getPropertyPalue(
          docData,
          'molecule_structures.canonical_smiles',
          undefined,
          true
        )

        if (this.smiles == null) {
          this.loading = false
          this.showError = true
          this.errorMsg = `No smiles found for the given ChEMBL ID (${chemblID})`
          return
        }
        this.useSmiles()
      } catch (error) {
        this.loading = false
        this.showError = true
        this.errorMsg = error.message
      }
    },
  },
  mounted() {
    if (this.isChEMBLID) {
      this.loadSmilesFromChEMBLID()
      return
    }
    this.smiles = this.term
    this.useSmiles()
  },
  computed: {
    isChEMBLID() {
      return this.term.startsWith('CHEMBL')
    },
  },
}
