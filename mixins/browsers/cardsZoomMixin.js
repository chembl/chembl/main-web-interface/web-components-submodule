export const cardsZoomMixin = {
  data() {
    return {
      colsPerRowIndex: 0,
      availableColsPerRow: [1, 2, 3, 4],
    }
  },
  computed: {
    showZoomControl() {
      return (
        !this.$vuetify.breakpoint.xs &&
        !this.$vuetify.breakpoint.sm &&
        !this.$vuetify.breakpoint.md
      )
    },
    disableZoomOut() {
      return this.colsPerRowIndex >= this.availableColsPerRow.length - 1
    },
    disableZoomIn() {
      return this.colsPerRowIndex <= 0
    },
    colsPerRow() {
      return this.availableColsPerRow[this.colsPerRowIndex]
    },
  },
  mounted() {
    this.$nextTick(() => {
      this.calculateInitialZoom()
    })
  },
  methods: {
    calculateInitialZoom() {
      if (this.$vuetify.breakpoint.lg) {
        this.colsPerRowIndex = 1
      } else if (this.$vuetify.breakpoint.xl) {
        this.colsPerRowIndex = 2
      } else {
        this.colsPerRowIndex = 0
      }
    },
    zoomOut() {
      if (this.disableZoomOut) {
        return
      }

      this.colsPerRowIndex += 1
    },
    resetZoom() {
      this.calculateInitialZoom()
    },
    zoomIn() {
      if (this.disableZoomIn) {
        return
      }

      this.colsPerRowIndex -= 1
    },
  },
}
