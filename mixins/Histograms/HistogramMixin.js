import LinksToBrowsers from '~/web-components-submodule/standardisation/LinksToBrowsers.js'
import IndexNames from '~/web-components-submodule/standardisation/IndexNames.js'
import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'

export const HistogramMixin = {
  data() {
    return {
      initialLoad: true,
      showError: false,
      errorMsg: undefined,
      loadingHistogramData: false,
      showHistogramDataError: false,
      histogramErrorMsg: undefined,
      numItems: 0,
      minXValue: undefined,
      maxXValue: undefined,
      minRange: 0.1,
      minInterval: undefined,
      maxInterval: undefined,
      currentInterval: undefined,
      intervalStep: undefined,
      numIntervalSteps: 10,
      missingLabel: 'N/A',
      otherLabel: 'Other',
      histogramData: undefined,
      numItemsWithNoCategory: 0,
      entityID: undefined,
      xPropertyLabel: undefined,
    }
  },
  computed: {
    countQuery() {
      return {
        size: 0,
        track_total_hits: true,
        query: {
          query_string: {
            query: this.allDataQuerystring,
          },
        },
      }
    },
    minAggName() {
      return `min_${this.categoriesField}`
    },
    maxAggName() {
      return `max_${this.categoriesField}`
    },
    minMaxQuery() {
      return {
        size: 0,
        query: {
          query_string: {
            query: this.allDataQuerystring,
          },
        },
        aggs: {
          [this.minAggName]: {
            min: {
              field: this.categoriesField,
            },
          },
          [this.maxAggName]: {
            max: {
              field: this.categoriesField,
            },
          },
        },
      }
    },
    aggName() {
      return `histogram_${this.categoriesField}`
    },
    aggQuery() {
      return {
        size: 0,
        query: {
          query_string: {
            query: this.allDataQuerystring,
          },
        },
        aggs: {
          [this.aggName]: {
            histogram: {
              field: this.categoriesField,
              interval: this.currentInterval,
            },
          },
        },
      }
    },
    noCategoryQuery() {
      return {
        size: 0,
        track_total_hits: true,
        query: {
          bool: {
            must: [
              {
                query_string: {
                  query: this.itemsWithNoCategoryQuerystring,
                },
              },
            ],
          },
        },
      }
    },
    itemsWithNoCategoryQuerystring() {
      return `${this.allDataQuerystring} AND NOT _exists_:${this.categoriesField}`
    },
    itemsWithNoCategoryDescriptionText() {
      if (this.histogramConfig == null) return 'Items with no category'
      return `${this.histogramConfig.datasetDescriptionText} - Items with no ${this.xPropertyLabel}`
    },
    itemsWithNoCategoryLinkText() {
      return `Items with no ${this.xPropertyLabel} (${this.numItemsWithNoCategory})`
    },
    linkToItemsWithNoCategory() {
      return LinksToBrowsers.buildURLForEntityBrowser(
        this.entityID,
        this.itemsWithNoCategoryQuerystring,
        'querystring',
        this.itemsWithNoCategoryDescriptionText
      )
    },
    disableLessBars() {
      return this.currentInterval >= this.maxInterval
    },
    disableMoreBars() {
      return this.currentInterval <= this.minInterval
    },
    isMobile() {
      return this.$vuetify.breakpoint.xs
    },
    showHistogramSettings() {
      return !this.isMobile
    },
    noDataAvailable() {
      if (this.histogramData == null) return true
      return this.histogramData.categories.length === 0
    },
  },
  watch: {
    currentInterval() {
      this.loadHistogramData()
    },
  },
  mounted() {
    this.loadInitialDetails()
  },
  methods: {
    increaseInterval() {
      if (this.currentInterval < this.maxInterval) {
        this.currentInterval += this.intervalStep
      }
    },
    decreaseInterval() {
      if (this.currentInterval > this.minInterval) {
        this.currentInterval -= this.intervalStep
      }
    },
    updateInterval(newInterval) {
      this.currentInterval = newInterval
    },
    calculateInitialInterval() {
      const range = this.maxXValue - this.minXValue
      const minBins = 2
      this.maxInterval = range / minBins

      let numDesiredBins

      if (this.$vuetify.breakpoint.lgAndUp) {
        numDesiredBins = 8
      } else if (this.$vuetify.breakpoint.md) {
        numDesiredBins = 6
      } else if (this.$vuetify.breakpoint.sm) {
        numDesiredBins = 4
      } else {
        numDesiredBins = 3
      }
      const maxDesiredBins = 2 * numDesiredBins
      this.minInterval = range / maxDesiredBins
      const desiredInterval = range / numDesiredBins
      this.intervalStep =
        (this.maxInterval - this.minInterval) / this.numIntervalSteps
      const intervalCandidate = Math.max(
        Math.min(this.maxInterval, desiredInterval),
        this.minInterval
      )

      this.currentInterval = this.calibrateInterval(
        this.minInterval,
        this.maxInterval,
        this.numIntervalSteps,
        this.intervalStep,
        intervalCandidate
      )
    },
    calibrateInterval(
      minInterval,
      maxInterval,
      numIntervalSteps,
      intervalStep,
      intervalCandidate
    ) {
      // generates the numIntervalSteps numbers from minInterval to maxInterval,
      // advancing by intervalStep each time then gets the closes number to intervalCandidate
      const intervals = []
      let currentInterval = minInterval
      for (let i = 0; i < numIntervalSteps; i++) {
        intervals.push(currentInterval)
        currentInterval += intervalStep
      }
      const closest = intervals.reduce((prev, curr) =>
        Math.abs(curr - intervalCandidate) < Math.abs(prev - intervalCandidate)
          ? curr
          : prev
      )
      return closest
    },
    async loadInitialDetails() {
      try {
        const itemsIndexName = IndexNames.getIndexNameFromEntityID(
          this.entityID
        )

        const countResponse = await ESProxyService.getESData(
          itemsIndexName,
          this.countQuery
        )
        this.numItems = countResponse.data.es_response.hits.total.value

        const minMaxResponse = await ESProxyService.getESData(
          itemsIndexName,
          this.minMaxQuery
        )

        this.minXValue =
          minMaxResponse.data.es_response.aggregations[this.minAggName].value
        this.maxXValue =
          minMaxResponse.data.es_response.aggregations[this.maxAggName].value

        // if min and max are the same, add minRange to max
        if (this.minXValue === this.maxXValue) {
          this.maxXValue += this.minRange
        }

        this.calculateInitialInterval()
        this.initialLoad = false
      } catch (error) {
        this.initialLoad = false
        this.showError = true
        this.errorMsg = error
      }
    },
    async loadHistogramData() {
      this.loadingHistogramData = true

      const itemsIndexName = IndexNames.getIndexNameFromEntityID(this.entityID)

      try {
        const histogramResponse = await ESProxyService.getESData(
          itemsIndexName,
          this.aggQuery
        )
        const buckets =
          histogramResponse.data.es_response.aggregations[this.aggName].buckets

        const categories = this.parseBuckets(buckets)
        this.histogramData = { categories, xPropertyLabel: this.xPropertyLabel }

        const itemsWithNoCategoryResponse = await ESProxyService.getESData(
          itemsIndexName,
          this.noCategoryQuery
        )

        this.numItemsWithNoCategory =
          itemsWithNoCategoryResponse.data.es_response.hits.total.value

        this.loadingHistogramData = false
      } catch (error) {
        this.loadingHistogramData = false
        this.showHistogramDataError = true
        this.histogramErrorMsg = error
      }
    },
    parseBuckets(buckets) {
      const categories = []

      for (let i = 0; i < buckets.length; i++) {
        const bucket = buckets[i]
        const currentXValue = bucket.key
        const generalDatasetDescriptionText = this.histogramConfig
          .datasetDescriptionText

        let label
        let querystring
        let datasetDescriptionText

        if (this.currentInterval === 1) {
          label = currentXValue
          querystring = `${this.allDataQuerystring} AND ${this.categoriesField}:${currentXValue}`
          datasetDescriptionText = `${generalDatasetDescriptionText} | ${this.xPropertyLabel} ${currentXValue}`
        } else {
          const nextXValue =
            i + 1 < buckets.length ? buckets[i + 1].key : this.maxXValue

          label =
            i + 1 < buckets.length
              ? `[${currentXValue.toFixed(2)}, ${nextXValue.toFixed(2)})`
              : `[${currentXValue.toFixed(2)}, ${nextXValue.toFixed(2)}]`

          const valueRange =
            i + 1 < buckets.length
              ? `[${currentXValue} TO ${nextXValue}}`
              : `[${currentXValue} TO ${nextXValue}]`

          querystring = `${this.allDataQuerystring} AND ${this.categoriesField}:${valueRange}`
          datasetDescriptionText = `${generalDatasetDescriptionText} | ${
            this.xPropertyLabel
          } from ${currentXValue.toFixed(2)} to ${nextXValue.toFixed(2)}`
        }

        const link = LinksToBrowsers.buildURLForEntityBrowser(
          this.entityID,
          querystring,
          'querystring',
          datasetDescriptionText
        )

        const category = {
          label,
          querystring,
          datasetDescriptionText,
          link,
          count: bucket.doc_count,
        }

        categories.push(category)
      }

      return categories
    },
  },
}
