export default {
  dispatchRequestErrorNotification(error, dispatch, baseMsg, detailsLink) {
    const notification = {
      type: 'error',
      message: baseMsg + error.message,
      detailsLink,
    }
    dispatch('notification/add', notification, { root: true })
  },
  dispatchRequestSuccessNotification(dispatch, msg, detailsLink) {
    const notification = {
      type: 'success',
      message: msg,
      detailsLink,
    }
    dispatch('notification/add', notification, { root: true })
  },
  dispatchRequestInfoNotification(dispatch, msg, detailsLink) {
    const notification = {
      type: 'info',
      message: msg,
      detailsLink,
    }
    dispatch('notification/add', notification, { root: true })
  },
}
