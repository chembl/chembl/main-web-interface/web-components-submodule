const methods = {
  sendStateUpdateMessage(idForParent, b64State) {
    const msgData = {
      idForParent,
      b64State,
    }

    window.parent.postMessage(
      { msg: 'EMBEDDED_VUE_DATASET_STATE_UPDATED', data: msgData },
      process.env.iframeTargetOrigin
    )
  },
}

export default methods
