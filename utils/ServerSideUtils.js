// Module that provides functionalities that are useful but not fully available in server side JS
const methods = {
  addQueryParamsToURL(baseURL, params) {
    const stringParams = []
    Object.entries(params).forEach(([key, value]) => {
      let strValue = ''
      if (Array.isArray(value)) {
        strValue = value.join(',')
      } else {
        strValue = String(value)
      }
      stringParams.push(`${key}=${encodeURIComponent(strValue)}`)
    })
    if (stringParams.length === 0) {
      return baseURL
    }
    const thereAreStringParams = stringParams.length > 0
    const allParamsStr = thereAreStringParams
      ? `?${stringParams.join('&')}`
      : ''
    return `${baseURL}${allParamsStr}`
  },
}

export default methods
