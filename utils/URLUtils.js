const methods = {
  sanitiseTextForEscaping(text) {
    // This solves the problem when nginx unescapes a forward slash
    // https://stackoverflow.com/questions/8264239/nginx-unescapes-2f-to-a-forward-slash-how-can-i-stop-it
    return text.replace(/\//g, '__SLASH__')
  },
  unSanitiseTextForEscaping(text) {
    // This solves the problem when nginx unescapes a forward slash
    // https://stackoverflow.com/questions/8264239/nginx-unescapes-2f-to-a-forward-slash-how-can-i-stop-it
    return text.replace(/__SLASH__/g, '/')
  },
}

export default methods
