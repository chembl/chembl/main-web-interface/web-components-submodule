const methods = {
  getPropertyPalue(
    theObj,
    strProperty,
    defaultNullValue = '---',
    returnUndefined = false
  ) {
    // return null or the default value if the object is null
    if (theObj == null) {
      if (returnUndefined) {
        return undefined
      }
      return defaultNullValue
    }

    const propParts = strProperty.split('.')
    const currentProp = propParts[0]

    if (propParts.length > 1) {
      const currentObj = theObj[currentProp]
      if (currentObj == null) {
        if (returnUndefined) {
          return undefined
        }
        return defaultNullValue
      }

      if (Array.isArray(currentObj)) {
        return currentObj
          .map((item) =>
            methods.getPropertyPalue(
              item,
              propParts.slice(1).join('.'),
              defaultNullValue,
              returnUndefined
            )
          )
          .flat()
      }

      return methods.getPropertyPalue(
        currentObj,
        propParts.slice(1).join('.'),
        defaultNullValue,
        returnUndefined
      )
    }

    if (Array.isArray(theObj)) {
      return theObj.map((item) =>
        methods.getPropertyPalue(
          item,
          currentProp,
          defaultNullValue,
          returnUndefined
        )
      )
    }

    let value = theObj[currentProp]
    if (returnUndefined && value == null) {
      return undefined
    }
    value = value == null ? defaultNullValue : value
    return value
  },
}

export default methods
