const methods = {
  truncateString(theString, maxLength) {
    if (theString.lenth <= maxLength) {
      return theString
    }
    return `${theString.slice(0, maxLength - 3)}...`
  },
}

export default methods
