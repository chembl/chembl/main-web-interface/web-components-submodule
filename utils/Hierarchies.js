import LinksToBrowsers from '~/web-components-submodule/standardisation/LinksToBrowsers.js'

const methods = {
  getURLForTargetsBelongingToClass(
    queryString,
    itemClass,
    classesPath,
    entityID
  ) {
    const pathText =
      classesPath.length > 1 ? ` (${classesPath.join(' / ')})` : ''

    const datasetDescription = `Item that belong to the class ${itemClass}.${pathText}`

    return LinksToBrowsers.buildURLForEntityBrowser(
      entityID,
      queryString,
      'filter',
      datasetDescription
    )
  },
}

export default methods
