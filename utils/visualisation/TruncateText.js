const truncateText = (fullText, availableLength, charLenght) => {
  const lenghtOccupied = fullText.length * charLenght
  if (lenghtOccupied <= availableLength) {
    return fullText
  }
  const lastChar = parseInt(availableLength / charLenght) - 4
  const truncated = `${fullText.substring(0, lastChar)}...`

  return truncated
}

export default {
  truncateText,
}
