import * as d3 from 'd3'

const parseRGBColor = (rgbColor) => {
  const parsedColor = rgbColor
    .replace(/^rgb\(/, '')
    .replace(/\)$/, '')
    .replace(/ /g, '')

  const colorParts = parsedColor.split(',')

  return {
    red: parseInt(colorParts[0]),
    green: parseInt(colorParts[1]),
    blue: parseInt(colorParts[2]),
    alpha: parseInt(colorParts[3]),
  }
}

const getFinalColorAfterOpacity = (color, opacity) => {
  const parsedColor = parseRGBColor(color)
  const transformedColor = {
    red: parseInt(255 - opacity * (255 - parsedColor.red)),
    green: parseInt(255 - opacity * (255 - parsedColor.green)),
    blue: parseInt(255 - opacity * (255 - parsedColor.blue)),
  }

  return `rgb(${transformedColor.red},${transformedColor.green},${transformedColor.blue})`
}

// see https://www.nbdtech.com/Blog/archive/2008/04/27/Calculating-the-Perceived-Brightness-of-a-Color.aspx
// and https://stackoverflow.com/questions/11867545/change-text-color-based-on-brightness-of-the-covered-background-area
const getBlackOrWhiteForBackround = (
  backgroundColor,
  darkColor = 'black',
  lightColor = 'white'
) => {
  const color = parseRGBColor(backgroundColor)

  const brightness = Math.round(
    (color.red * 299 + color.green * 587 + color.blue * 114) / 1000
  )

  return brightness > 128 ? darkColor : lightColor
}

const getColorLuminance = (color) => {
  const red = color.red
  const green = color.green
  const blue = color.blue

  const a = [red, green, blue].map(function (v) {
    v /= 255
    return v <= 0.03928 ? v / 12.92 : Math.pow((v + 0.055) / 1.055, 2.4)
  })
  return a[0] * 0.2126 + a[1] * 0.7152 + a[2] * 0.0722
}

function getColorContrast(color1, color2) {
  const lum1 = getColorLuminance(color1)
  const lum2 = getColorLuminance(color2)
  const brightest = Math.max(lum1, lum2)
  const darkest = Math.min(lum1, lum2)
  return (brightest + 0.05) / (darkest + 0.05)
}

const rgba2rgb = (backgroundColor, rgbaColor) => {
  const alpha = rgbaColor.alpha

  return {
    red: parseInt((1 - alpha) * backgroundColor.red + alpha * rgbaColor.red),
    green: parseInt(
      (1 - alpha) * backgroundColor.green + alpha * rgbaColor.green
    ),
    blue: parseInt((1 - alpha) * backgroundColor.blue + alpha * rgbaColor.blue),
  }
}

const calculateForegroundColorFromContrastRatio = (
  rawColor,
  backgroundOpacity
) => {
  const backgroundColor = parseRGBColor(rawColor)
  backgroundColor.alpha = backgroundOpacity
  const realBackgroundColor = rgba2rgb(
    { red: 255, green: 255, blue: 255 },
    backgroundColor
  )

  const targetCRatio = 8

  let candidateForegroundColorrHSL = d3.hsl(
    `rgb(${realBackgroundColor.red}, ${realBackgroundColor.green}, ${realBackgroundColor.blue})`
  )

  let searchSpace = {
    minS: 0,
    maxS: 1,
    minL: 0,
    maxL: 1,
  }

  let maxContrastFound = -1
  let i = 0
  let foundCompliantColor = false
  while (!foundCompliantColor && i < 10) {
    i += 1
    const space1 = {
      minS: searchSpace.minS,
      maxS: searchSpace.maxS / 2,
      minL: searchSpace.minL,
      maxL: searchSpace.maxL / 2,
    }

    const space2 = {
      minS: searchSpace.maxS / 2,
      maxS: searchSpace.maxS,
      minL: searchSpace.minL,
      maxL: searchSpace.maxL / 2,
    }

    const space3 = {
      minS: searchSpace.minS,
      maxS: searchSpace.maxS / 2,
      minL: searchSpace.maxL / 2,
      maxL: searchSpace.maxL,
    }

    const space4 = {
      minS: searchSpace.maxS / 2,
      maxS: searchSpace.maxS,
      minL: searchSpace.maxL / 2,
      maxL: searchSpace.maxL,
    }

    const allSpaces = [space1, space2, space3, space4]

    allSpaces.forEach((space) => {
      const middleS = (space.minS + space.maxS) / 2

      const middleL = (space.minL + space.maxL) / 2

      const candidateHSL = d3.hsl(
        candidateForegroundColorrHSL.h,
        middleS,
        middleL
      )

      const candidateRGB = candidateHSL.rgb()

      const candidateColorContrast = getColorContrast(realBackgroundColor, {
        red: candidateRGB.r,
        green: candidateRGB.g,
        blue: candidateRGB.b,
      })

      if (candidateColorContrast > maxContrastFound) {
        searchSpace = space
        candidateForegroundColorrHSL = candidateHSL
        maxContrastFound = candidateColorContrast
        if (maxContrastFound >= targetCRatio) {
          foundCompliantColor = true
        }
      }
    })
  }

  // check if black or white are better anyway, sometimes the search does not find a foreground good color
  const contrastWithBlack = getColorContrast(realBackgroundColor, {
    red: 0,
    green: 0,
    blue: 0,
  })

  const contrastWithWhite = getColorContrast(realBackgroundColor, {
    red: 255,
    green: 255,
    blue: 255,
  })

  const maxContrastAvailable = Math.max(
    contrastWithBlack,
    contrastWithWhite,
    maxContrastFound
  )

  let finalForegroundColorText
  if (maxContrastAvailable === contrastWithBlack) {
    finalForegroundColorText = `rgb(0, 0, 0)`
  } else if (maxContrastAvailable === contrastWithWhite) {
    finalForegroundColorText = `rgb(255, 255, 255)`
  } else {
    const finalForegroundRGB = candidateForegroundColorrHSL.rgb()
    finalForegroundColorText = `rgb(${parseInt(
      finalForegroundRGB.r
    )}, ${parseInt(finalForegroundRGB.g)}, ${parseInt(finalForegroundRGB.b)})`
  }
  return finalForegroundColorText
}

export default {
  getBlackOrWhiteForBackround,
  getFinalColorAfterOpacity,
  calculateForegroundColorFromContrastRatio,
}
