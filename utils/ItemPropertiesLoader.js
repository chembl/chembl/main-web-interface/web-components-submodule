import IndexNames from '~/web-components-submodule/standardisation/IndexNames.js'
import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'
import ObjectPropertyAccess from '~/web-components-submodule/utils/ObjectPropertyAccess.js'

const methods = {
  getItemProperties(entityID, itemID, groupName) {
    const resultsPromise = new Promise((resolve, reject) => {
      const indexName = IndexNames.getIndexNameFromEntityID(entityID)
      ESProxyService.getPopertyGroupConfiguration(indexName, groupName)
        .then((response) => {
          const defaultProperties =
            response.data.properties.default != null
              ? response.data.properties.default
              : []
          const stickyProperties =
            response.data.properties.sticky != null
              ? response.data.properties.sticky
              : []
          const propertiesConfig = defaultProperties.concat(stickyProperties)

          // Elasticsearch doesn't care if there are duplicates
          const propertiesToFetch = propertiesConfig.map((propConfig) => {
            return {
              label: propConfig.label,
              prop_id: propConfig.prop_id,
            }
          })

          const docSource = propertiesToFetch.map(
            (propConfig) => propConfig.prop_id
          )

          ESProxyService.getESDocument(indexName, itemID, docSource)
            .then((response) => {
              const sourceObtained = response.data._source
              const obtainedProperties = propertiesToFetch.map((propConfig) => {
                const isDefault =
                  defaultProperties.find(
                    (item) => item.prop_id === propConfig.prop_id
                  ) != null

                return {
                  propID: propConfig.prop_id,
                  label: propConfig.label,
                  value: ObjectPropertyAccess.getPropertyPalue(
                    sourceObtained,
                    propConfig.prop_id,
                    '--'
                  ),
                  show: isDefault,
                }
              })
              resolve(obtainedProperties)
            })
            .catch((error) => {
              reject(error)
            })
        })
        .catch((error) => {
          reject(error)
        })
    })

    return resultsPromise
  },
}

export default methods
