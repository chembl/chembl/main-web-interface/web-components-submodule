import TextAlignment from '~/web-components-submodule/utils/textHighlighting/TextAlignment.js'

const splitTextByHighlights = (highlightedText, fullText) => {
  if (highlightedText == null) {
    return [{ text: fullText, isMatch: false }]
  }

  const pHighlightedText = String(highlightedText)
  const pFullText = String(fullText)

  if (pHighlightedText.trim() === '' || pHighlightedText === '*') {
    return [{ text: pFullText, isMatch: false }]
  }

  if (fullText.trim() === '') {
    return [{ text: pFullText, isMatch: false }]
  }

  const alignmentCandidates = []

  const highlightedTextParts = splitHighlightedText(highlightedText)
  for (const currentHighlightedText of highlightedTextParts) {
    const termSize = Math.min(currentHighlightedText.length, pFullText.length)
    // Maximum number of edits allowed to be considered
    const distanceThreshold = Math.floor(termSize * 0.4)
    for (let i = 0; i < pFullText.length - termSize + 1; i++) {
      const start = i
      const end = i + termSize
      const currentSubstring = pFullText.substring(start, end)
      const bestAlignment = TextAlignment.getBestAlignment(
        currentHighlightedText,
        currentSubstring,
        distanceThreshold
      )

      if (bestAlignment.minimumEditDistance <= distanceThreshold) {
        bestAlignment.start = start
        bestAlignment.end = end
        alignmentCandidates.push(bestAlignment)
      }
    }
  }

  const alignmentsWithNoOverlaps = getAlignmentsWithoutOverlaps(
    alignmentCandidates
  )

  const finalSplit = splitTextByAlignments(pFullText, alignmentsWithNoOverlaps)

  return finalSplit
}

const textHasHighlight = (highlightedText, fullText) => {
  if (highlightedText == null) {
    return false
  }

  const pHighlightedText = String(highlightedText)
  const pFullText = String(fullText)

  if (pHighlightedText === '' || pHighlightedText === '*' || pFullText === '') {
    return false
  }

  const highlightedTextParts = splitHighlightedText(highlightedText)
  for (const currentHighlightedText of highlightedTextParts) {
    const termSize = Math.min(currentHighlightedText.length, pFullText.length)
    // Maximum number of edits allowed to be considered
    const distanceThreshold = Math.floor(termSize * 0.3)

    for (let i = 0; i < pFullText.length - termSize + 1; i++) {
      const start = i
      const end = i + termSize
      const currentSubstring = pFullText.substring(start, end)
      const bestAlignment = TextAlignment.getBestAlignment(
        currentHighlightedText,
        currentSubstring,
        distanceThreshold
      )
      if (bestAlignment.minimumEditDistance <= distanceThreshold) {
        return true
      }
    }
  }

  return false
}

const splitTextByAlignments = (fullText, alignments) => {
  if (alignments.length === 0) {
    return [
      {
        text: fullText,
        isMatch: false,
      },
    ]
  }

  const textSplit = []
  let leftDisplacement = 0
  for (let i = 0; i < alignments.length; i++) {
    const currentAlignment = alignments[i]
    const alignmentStart = currentAlignment.start
    const alignmentEnd = currentAlignment.end
    const leftSubstring = fullText.substring(leftDisplacement, alignmentStart)

    if (leftSubstring !== '') {
      const textPartLeft = {
        text: leftSubstring,
        isMatch: false,
      }
      textSplit.push(textPartLeft)
    }

    const highlightSubstring = fullText.substring(alignmentStart, alignmentEnd)
    const textPartHighlight = {
      text: highlightSubstring,
      isMatch: true,
      alignment: currentAlignment,
    }
    textSplit.push(textPartHighlight)
    leftDisplacement = alignmentEnd

    // add last right part
    if (i === alignments.length - 1) {
      const rightSubstring = fullText.substring(alignmentEnd, fullText.length)

      if (rightSubstring !== '') {
        const textPartRight = {
          text: rightSubstring,
          isMatch: false,
        }
        textSplit.push(textPartRight)
      }
    }
  }

  return textSplit
}

const splitHighlightedText = (highlightedText) => {
  const parts = highlightedText
    .split(' ')
    .filter(
      (text) =>
        text.length > 1 &&
        text.trim() !== '' &&
        ![
          'an',
          'and',
          'any',
          'are',
          'as',
          'at',
          'be',
          'but',
          'by',
          'can',
          'did',
          'do',
          'for',
          'so',
          'the',
          'to',
          'of',
        ].includes(text.toLowerCase())
    )
  return parts
}

const getAlignmentsWithoutOverlaps = (alignmentCandidates) => {
  const noOverlaps = []
  let currentIndex = 0
  let alignment1 = alignmentCandidates[currentIndex]
  let alignment2 = alignmentCandidates[currentIndex + 1]

  let cont = true

  while (cont) {
    if (alignment1 == null) {
      cont = false
      break
    }

    if (alignment2 == null) {
      noOverlaps.push(alignment1)
      cont = false
      break
    }

    if (!alignmentsOverlap(alignment1, alignment2)) {
      noOverlaps.push(alignment1)
      alignment1 = alignment2
      currentIndex++
      alignment2 = alignmentCandidates[currentIndex + 1]
      continue
    }

    alignment1 =
      alignment1.minimumEditDistance < alignment2.minimumEditDistance
        ? alignment1
        : alignment2
    currentIndex++
    alignment2 = alignmentCandidates[currentIndex + 1]
  }

  noOverlaps.sort(
    (alignmentA, alignmentB) => alignmentA.start - alignmentB.start
  )
  return noOverlaps
}

const alignmentsOverlap = (alignment1, alignment2) => {
  const start1 = alignment1.start
  const end1 = alignment1.end

  const start2 = alignment2.start
  const end2 = alignment2.end

  if (start1 >= start2 && start1 < end2) {
    return true
  }

  if (end1 > start2 && end1 <= end2) {
    return true
  }

  return false
}

export default {
  splitTextByHighlights,
  textHasHighlight,
  splitHighlightedText,
}
