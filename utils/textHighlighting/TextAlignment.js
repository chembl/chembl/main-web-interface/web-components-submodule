const characterOps = {
  REPLACE: 'REPLACE',
  DELETE: 'DELETE',
  INSERT: 'INSERT',
  DO_NOTHING: 'DO_NOTHING',
}

const getBestAlignment = (queryText, destinationText, maxDistanceAllowed) => {
  // This is based on the Levenshtein Distance see https://www.youtube.com/watch?v=MiqoA-yF-0M
  const parsedQueryTest = queryText.toLowerCase()
  const parsedDestinationText = destinationText.toLowerCase()

  const distanceMatrix = []
  for (let i = 0; i < parsedDestinationText.length; i++) {
    const charI = parsedDestinationText[i]
    distanceMatrix[i] = []

    for (let j = 0; j < parsedQueryTest.length; j++) {
      const charJ = parsedQueryTest[j]
      distanceMatrix[i][j] = {}

      const minimumOpFromNeighbors = getNewOpFromNeighbors(
        distanceMatrix,
        i,
        j,
        charI,
        charJ
      )

      distanceMatrix[i][j] = minimumOpFromNeighbors
      // This is an optimization, if along the diagonal the edit distance is already greater than the maximum
      // allowed, I return and don't continue calculating anything.
      if (i === j) {
        if (minimumOpFromNeighbors > maxDistanceAllowed) {
          return {
            minimumEditDistance: minimumOpFromNeighbors,
            bestAlignmentPath: [],
          }
        }
      }
    }
  }

  const minimumEditDistance =
    distanceMatrix[parsedDestinationText.length - 1][parsedQueryTest.length - 1]
      .value

  const bestAlignmentPath = getBestAlignmentPath(
    distanceMatrix,
    parsedDestinationText.length,
    parsedQueryTest.length
  )

  return {
    minimumEditDistance,
    bestAlignmentPath,
  }
}

const getNewOpFromNeighbors = (distanceMatrix, i, j, charI, charJ) => {
  const neighborOperations = []

  if (i > 0) {
    const upperNeighbor = distanceMatrix[i - 1][j]
    neighborOperations.push(upperNeighbor)
  }

  if (j > 0) {
    const leftNeighbor = distanceMatrix[i][j - 1]
    neighborOperations.push(leftNeighbor)
  }

  if (i > 0 && j > 0) {
    const upperLeftNeighbor = distanceMatrix[i - 1][j - 1]
    neighborOperations.push(upperLeftNeighbor)
  }

  if (i <= 0 && j <= 0) {
    const initialMockNeighbor = {
      operation: characterOps.DO_NOTHING,
      value: 0,
      sourceI: undefined,
      sourceJ: undefined,
      i: -1,
      j: -1,
    }
    neighborOperations.push(initialMockNeighbor)
  }

  const minimumOP = getMinimumOp(neighborOperations)

  return getNewOpFromMinNeighbor(minimumOP, charI, charJ, i, j)
}

const getMinimumOp = (operationsList) => {
  let minOP = {
    value: Number.MAX_SAFE_INTEGER,
  }
  for (let i = 0; i < operationsList.length; i++) {
    const currentOp = operationsList[i]
    if (currentOp.value < minOP.value) {
      minOP = currentOp
    }
  }
  return minOP
}

const getNewOpFromMinNeighbor = (minimumOP, charI, charJ, i, j) => {
  const stepValue = charI === charJ ? 0 : 1
  let newOperation
  if (charI === charJ) {
    newOperation = characterOps.DO_NOTHING
  } else if (minimumOP.i === i && minimumOP.j === j - 1) {
    newOperation = characterOps.DELETE
  } else if (minimumOP.i === i - 1 && minimumOP.j === j) {
    newOperation = characterOps.INSERT
  } else if (minimumOP.i === i - 1 && minimumOP.j === j - 1) {
    newOperation = characterOps.REPLACE
  }
  const newOP = {
    operation: newOperation,
    value: minimumOP.value + stepValue,
    sourceI: minimumOP.i,
    sourceJ: minimumOP.j,
    i,
    j,
  }
  return newOP
}

const getBestAlignmentPath = (
  distanceMatrix,
  destinationTextLength,
  queryTextLength
) => {
  const bestPath = []
  let currentNode =
    distanceMatrix[destinationTextLength - 1][queryTextLength - 1]

  bestPath.unshift(currentNode)
  while (currentNode.sourceI >= 0 && currentNode.sourceJ >= 0) {
    currentNode = distanceMatrix[currentNode.sourceI][currentNode.sourceJ]
    bestPath.unshift(currentNode)
  }
  return bestPath
}

export default {
  getBestAlignment,
  characterOps,
}
