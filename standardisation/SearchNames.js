const searchNames = {
  EubopenTextSearch: {
    searchID: 'EubopenTextSearch',
  },
  ChemblTextSearch: {
    searchID: 'ChemblTextSearch',
  },
  ChemblSimilaritySearch: {
    searchID: 'ChemblSimilaritySearch',
  },
  ChemblConnectivitySearch: {
    searchID: 'ChemblConnectivitySearch',
  },
  ChemblSubstructureSearch: {
    searchID: 'ChemblSubstructureSearch',
  },
  ChemblBLASTSearch: {
    searchID: 'ChemblBLASTSearch',
  },
  ChemblSearchByIDs: {
    searchID: 'ChemblSearchByIDs',
  },
}

export default searchNames
