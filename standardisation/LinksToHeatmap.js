import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'
import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'

const methods = {
  cleanUpBaseQuery(query) {
    const cleanQuery = {
      ...query,
    }
    delete cleanQuery.from
    delete cleanQuery.size
    delete cleanQuery._source
    return cleanQuery
  },
  getAxisHeadersConfig(axisEntityID) {
    if (axisEntityID === EntityNames.Compound.entityID) {
      return {
        label_property: 'pref_name',
        additional_properties: ['molecule_chembl_id'],
        mini_report_card_properties: [
          'molecule_chembl_id',
          'pref_name',
          'molecule_synonyms',
          'molecule_type',
          'max_phase',
          'molecule_properties.full_mwt',
        ],
      }
    }
    return {
      label_property: 'pref_name',
      additional_properties: ['target_chembl_id'],
      mini_report_card_properties: [
        'target_chembl_id',
        'pref_name',
        'target_synonyms',
        'target_type',
        'organism',
      ],
    }
  },
  getAxisFootersConfig(axisEntityID) {
    if (axisEntityID === EntityNames.Compound.entityID) {
      return {
        label_properties: ['molecule_type', 'molecule_properties.mw_freebase'],
      }
    }
    return {
      label_properties: ['organism', 'target_type'],
    }
  },
  generateHeatmapDescription(generationParams) {
    return {
      xAxis: {
        entityID: generationParams.xAxis.entityID,
        initialQuery: methods.cleanUpBaseQuery(generationParams.xAxis.query),
        headers: methods.getAxisHeadersConfig(generationParams.xAxis.entityID),
        footers: methods.getAxisFootersConfig(generationParams.xAxis.entityID),
      },
      yAxis: {
        entityID: generationParams.yAxis.entityID,
        initialQuery: methods.cleanUpBaseQuery(generationParams.yAxis.query),
        headers: methods.getAxisHeadersConfig(generationParams.yAxis.entityID),
        footers: methods.getAxisFootersConfig(generationParams.yAxis.entityID),
      },
      cells: {
        entityID: EntityNames.Activity.entityID,
        properties: ['pchembl_value_avg', 'num_activities'],
        properties_links_types: {
          num_activities: 'activities_from_compounds_and_targets',
        },
      },
    }
  },
}

const linksToHeatmap = {
  buildURLForHeatmap(generationParams) {
    const description = methods.generateHeatmapDescription(generationParams)
    return new Promise((resolve, reject) => {
      ESProxyService.Heatmap.getHeatmapID(description)
        .then((response) => {
          const heamapID = response.data.hash
          const heatmapURL = process.env.heatmapUrlTemplate.replace(
            '<HEATMAP_ID>',
            heamapID
          )
          resolve(heatmapURL)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
}

export default linksToHeatmap
