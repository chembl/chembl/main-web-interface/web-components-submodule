import SearchNames from '~/web-components-submodule/standardisation/SearchNames.js'

export default {
  [SearchNames.EubopenTextSearch.searchID]: {
    getLinkToSearch(term) {
      return `${process.env.eubopenTextSearchBaseUrl}/${encodeURIComponent(
        term
      )}`
    },
  },
  [SearchNames.ChemblTextSearch.searchID]: {
    getLinkToSearch(term, fragment) {
      const fullURL = process.env.freeTextsearchesURLTemplate.replace(
        '<TERM>',
        encodeURIComponent(term)
      )

      if (!fragment) {
        return fullURL
      }

      const protocol = window.location.protocol
      const host = window.location.host
      const basePath = process.env.basePath
      const baseUrl = `${protocol}//${host}${basePath}`
      return fullURL.replace(baseUrl, '')
    },
    getLinkToSearchWithState(term, stateID, fragment) {
      const fullURL = process.env.freeTextsearchesWithStateURLTemplate
        .replace('<TERM>', encodeURIComponent(term))
        .replace('<STATE_ID>', stateID)

      if (!fragment) {
        return fullURL
      }

      const protocol = window.location.protocol
      const host = window.location.host
      const basePath = process.env.basePath
      const baseUrl = `${protocol}//${host}${basePath}`

      return fullURL.replace(baseUrl, '')
    },
  },
  [SearchNames.ChemblSimilaritySearch.searchID]: {
    getLinkToSearch(term, threshold) {
      return process.env.similaritySearchUrlTemplate
        .replace('<TERM>', term)
        .replace('<THRESHOLD>', threshold)
    },
    getLinkToAPICall(term, threshold) {
      return `${process.env.chemblWSBaseUrl}/similarity/${term}/${threshold}.json`
    },
  },
  [SearchNames.ChemblConnectivitySearch.searchID]: {
    getLinkToSearch(term) {
      return process.env.connectivitySearchUrlTemplate.replace('<TERM>', term)
    },
  },
  [SearchNames.ChemblSubstructureSearch.searchID]: {
    getLinkToSearch(term) {
      return process.env.substructureSearchUrlTemplate.replace('<TERM>', term)
    },
  },
  [SearchNames.ChemblBLASTSearch.searchID]: {
    getLinkToSearch(descriptor) {
      return process.env.blastSearchUrlTemplate.replace(
        '<DESCRIPTOR>',
        descriptor
      )
    },
    getLinkToSearchFromParams(searchParams) {
      const descriptor = btoa(JSON.stringify(searchParams))
      return process.env.blastSearchUrlTemplate.replace(
        '<DESCRIPTOR>',
        descriptor
      )
    },
  },
  [SearchNames.ChemblSearchByIDs.searchID]: {
    getLinkToSearch(jobID) {
      return process.env.searchByIDsUrlTemplate.replace('<JOB_ID>', jobID)
    },
  },
}
