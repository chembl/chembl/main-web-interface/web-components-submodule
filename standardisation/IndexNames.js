import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'

const methods = {
  getMoleculeIndexName() {
    return `${process.env.esIndexPrefix}molecule`
  },
  getAssayIndexName() {
    return `${process.env.esIndexPrefix}assay`
  },
  getDocumentIndexName() {
    return `${process.env.esIndexPrefix}document`
  },
  getTargetIndexName() {
    return `${process.env.esIndexPrefix}target`
  },
  getCellIndexName() {
    return `${process.env.esIndexPrefix}cell_line`
  },
  getTissueIndexName() {
    return `${process.env.esIndexPrefix}tissue`
  },
  getDrugWarningInfoByParentIndexName() {
    return `${process.env.esIndexPrefix}drug_warning_by_parent`
  },
  getDrugIndicationsyParentIndexName() {
    return `${process.env.esIndexPrefix}drug_indication_by_parent`
  },
  getMechanismOfActionIndexName() {
    return `${process.env.esIndexPrefix}mechanism_by_parent_target`
  },
  getIDLookupIndexName() {
    return `${process.env.esIndexPrefix}chembl_id_lookup`
  },
  getEubopenTargetIndexName() {
    return `${process.env.esIndexPrefix}eubopen_target`
  },
  getEubopenCompoundIndexName() {
    return `${process.env.esIndexPrefix}eubopen_molecule`
  },
  getActivityIndexName() {
    return `${process.env.esIndexPrefix}activity`
  },
  getEubopenMechanismOfActionIndexName() {
    return `${process.env.esIndexPrefix}eubopen_mechanism`
  },
  getEubopenActivityIndexName() {
    return `${process.env.esIndexPrefix}eubopen_activity`
  },
  getEubopenAssayIndexName() {
    return `${process.env.esIndexPrefix}eubopen_assay`
  },
  getEubopenMainTargetName() {
    return `${process.env.esIndexPrefix}eubopen_main_target`
  },

  getIndexNameFromLookupEntityName(rawName) {
    if (rawName === 'COMPOUND') {
      return methods.getMoleculeIndexName()
    }
    if (rawName === 'ASSAY') {
      return methods.getAssayIndexName()
    }
    if (rawName === 'DOCUMENT') {
      return methods.getDocumentIndexName()
    }
    if (rawName === 'TARGET') {
      return methods.getTargetIndexName()
    }
    if (rawName === 'CELL') {
      return methods.getCellIndexName()
    }
    if (rawName === 'TISSUE') {
      return methods.getTissueIndexName()
    }
  },
  getIndexNameFromEntityID(entityID) {
    if (entityID === EntityNames.EubopenTarget.entityID) {
      return methods.getEubopenTargetIndexName()
    }
    if (entityID === EntityNames.EubopenCompound.entityID) {
      return methods.getEubopenCompoundIndexName()
    }
    if (entityID === EntityNames.Compound.entityID) {
      return methods.getMoleculeIndexName()
    }
    if (entityID === EntityNames.Drug.entityID) {
      return methods.getMoleculeIndexName()
    }
    if (entityID === EntityNames.Target.entityID) {
      return methods.getTargetIndexName()
    }
    if (entityID === EntityNames.Compound.entityID) {
      return methods.getMoleculeIndexName()
    }
    if (entityID === EntityNames.Assay.entityID) {
      return methods.getAssayIndexName()
    }
    if (entityID === EntityNames.Document.entityID) {
      return methods.getDocumentIndexName()
    }
    if (entityID === EntityNames.CellLine.entityID) {
      return methods.getCellIndexName()
    }
    if (entityID === EntityNames.Tissue.entityID) {
      return methods.getTissueIndexName()
    }
    if (entityID === EntityNames.Activity.entityID) {
      return methods.getActivityIndexName()
    }
    if (entityID === EntityNames.EubopenMechanismOfAction.entityID) {
      return methods.getEubopenMechanismOfActionIndexName()
    }
    if (entityID === EntityNames.EubopenActivity.entityID) {
      return methods.getEubopenActivityIndexName()
    }
    if (entityID === EntityNames.EubopenAssay.entityID) {
      return methods.getEubopenAssayIndexName()
    }
    if (entityID === EntityNames.DrugWarning.entityID) {
      return methods.getDrugWarningInfoByParentIndexName()
    }
    if (entityID === EntityNames.DrugMechanism.entityID) {
      return methods.getMechanismOfActionIndexName()
    }
    if (entityID === EntityNames.DrugIndication.entityID) {
      return methods.getDrugIndicationsyParentIndexName()
    }
    if (entityID === EntityNames.CellLine.entityID) {
      return methods.getCellIndexName()
    }
    if (entityID === EntityNames.EubopenMainTarget.entityID) {
      return methods.getEubopenMainTargetName()
    }
  },
}

export default methods
