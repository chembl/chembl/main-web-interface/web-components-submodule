import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'

const params = {
  [EntityNames.Document.entityID]: {
    'chembl_release.chembl_release': {
      histogramParams: {
        maxBars: 100,
        customBucketSortFunction: (a, b) => {
          const numA = parseInt(a.key.split('_')[1])
          const numB = parseInt(b.key.split('_')[1])
          return numA - numB
        },
      },
    },
  },
  [EntityNames.DrugIndication.entityID]: {
    'drug_indication.max_phase_for_ind_label': {
      histogramParams: {
        customBucketSortFunction: (a, b) => {
          const customOrder = [
            'Approved',
            'Phase 3',
            'Phase 2',
            'Phase 1',
            'Early Phase 1',
            'Preclinical',
            'Unknown',
          ]

          const indexA = customOrder.indexOf(a.key)
          const indexB = customOrder.indexOf(b.key)
          return indexA - indexB
        },
      },
    },
  },
  [EntityNames.DrugMechanism.entityID]: {
    'mechanism_of_action.max_phase_label': {
      histogramParams: {
        customBucketSortFunction: (a, b) => {
          const customOrder = [
            'Approved',
            'Phase 3',
            'Phase 2',
            'Phase 1',
            'Early Phase 1',
            'Preclinical',
            'Unknown',
          ]

          const indexA = customOrder.indexOf(a.key)
          const indexB = customOrder.indexOf(b.key)
          return indexA - indexB
        },
      },
    },
  },
  [EntityNames.Activity.entityID]: {
    '_metadata.parent_molecule_data.max_phase_label': {
      histogramParams: {
        customBucketSortFunction: (a, b) => {
          const customOrder = [
            'Approved',
            'Phase 3',
            'Phase 2',
            'Phase 1',
            'Early Phase 1',
            'Preclinical',
            'Unknown',
          ]

          const indexA = customOrder.indexOf(a.key)
          const indexB = customOrder.indexOf(b.key)
          return indexA - indexB
        },
      },
    },
  },
  [EntityNames.EubopenAssay.entityID]: {
    '_metadata.eubopen.assay_type.type.description': {
      histogramParams: {
        defaultNumBars: 20,
        numBars: 20,
        hideNumbers: true,
      },
    },
  },
  [EntityNames.EubopenTarget.entityID]: {
    '_metadata.eubopen.protein_family.label': {
      histogramParams: {
        nullValuesLabel: 'Unassigned',
      },
    },
  },
}

const methods = {
  getInitialHistogramParamsForFilter(entityID, propID) {
    const entityConfig = params[entityID]
    if (entityConfig == null) {
      return {}
    }

    const propConfig = entityConfig[propID]
    if (propConfig == null) {
      return {}
    }

    return propConfig.histogramParams == null ? {} : propConfig.histogramParams
  },
}

export default {
  methods,
  params,
}
