import IndexNames from '~/web-components-submodule/standardisation/IndexNames.js'
import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'

export default {
  [IndexNames.getMoleculeIndexName()]: {
    getLinkToReportCard(chemblID) {
      return `${process.env.compoundReportCardsBaseUrl}/${chemblID}`
    },
  },
  [IndexNames.getAssayIndexName()]: {
    getLinkToReportCard(chemblID) {
      return `${process.env.assayReportCardsBaseUrl}/${chemblID}`
    },
  },
  [IndexNames.getDocumentIndexName()]: {
    getLinkToReportCard(chemblID) {
      return `${process.env.documentReportCardsBaseUrl}/${chemblID}`
    },
  },
  [IndexNames.getTargetIndexName()]: {
    getLinkToReportCard(chemblID) {
      return `${process.env.targetReportCardsBaseUrl}/${chemblID}`
    },
  },
  [IndexNames.getCellIndexName()]: {
    getLinkToReportCard(chemblID) {
      return `${process.env.cellLineReportCardsBaseUrl}/${chemblID}`
    },
  },
  [IndexNames.getTissueIndexName()]: {
    getLinkToReportCard(chemblID) {
      return `${process.env.tissueReportCardsBaseUrl}/${chemblID}`
    },
  },
  [EntityNames.Compound.entityID]: {
    getLinkToReportCard(itemID) {
      return `${process.env.compoundReportCardsBaseUrl}/${itemID}`
    },
  },
  [EntityNames.EubopenCompound.entityID]: {
    getLinkToReportCard(itemID) {
      return `${process.env.eubopenCompoundReportCardsBaseUrl}/${itemID}`
    },
  },
  [EntityNames.EubopenTarget.entityID]: {
    getLinkToReportCard(itemID) {
      const ultraMegaSafeID = encodeURIComponent(itemID.replace(/\//g, '___'))
      return `${process.env.eubopenTargetReportCardsBaseUrl}/${ultraMegaSafeID}`
    },
  },
  [EntityNames.EubopenAssay.entityID]: {
    getLinkToReportCard(itemID) {
      const ultraMegaSafeID = encodeURIComponent(itemID.replace(/\//g, '___'))
      return `${process.env.eubopenAssayReportCardsBaseUrl}/${ultraMegaSafeID}`
    },
  },
  [EntityNames.Assay.entityID]: {
    getLinkToReportCard(itemID) {
      return `${process.env.assayReportCardsBaseUrl}/${itemID}`
    },
  },
  [EntityNames.CellLine.entityID]: {
    getLinkToReportCard(chemblID) {
      return `${process.env.cellLineReportCardsBaseUrl}/${chemblID}`
    },
  },
  [EntityNames.Document.entityID]: {
    getLinkToReportCard(chemblID) {
      return `${process.env.documentReportCardsBaseUrl}/${chemblID}`
    },
  },
  [EntityNames.Target.entityID]: {
    getLinkToReportCard(chemblID) {
      return `${process.env.targetReportCardsBaseUrl}/${chemblID}`
    },
  },
  [EntityNames.Tissue.entityID]: {
    getLinkToReportCard(chemblID) {
      return `${process.env.tissueReportCardsBaseUrl}/${chemblID}`
    },
  },
}
