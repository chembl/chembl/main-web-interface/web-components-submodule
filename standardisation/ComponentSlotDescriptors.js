import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'
import DefaultValues from '~/web-components-submodule/standardisation/DefaultValues.js'

const config = {
  [EntityNames.DrugWarning.entityID]: {
    'drug_warning.parent_molecule_chembl_id': (generatorParams) => {
      return {
        entityID: EntityNames.Compound.entityID,
        itemIDProperty: 'drug_warning.parent_molecule_chembl_id',
        linkTextProperty: 'drug_warning.parent_molecule_chembl_id',
        showMoleculeStructure: true,
        enableStructureSearch: true,
        moleculeStructure: {
          showOptions: true,
        },
      }
    },
    'drug_warning.molecule_chembl_id': (generatorParams) => {
      return {
        entityID: EntityNames.Compound.entityID,
        itemIDProperty: 'drug_warning.molecule_chembl_id',
        linkTextProperty: 'drug_warning.molecule_chembl_id',
        showMoleculeStructure: true,
        enableStructureSearch: true,
        moleculeStructure: {
          showOptions: true,
        },
      }
    },
    'drug_warning.where': (generatorParams) => {
      return {
        valueAccessor: generatorParams.valueAccessor,
      }
    },
    'drug_warning.efo_id': (generatorParams) => {
      return {
        linkTextProperty: 'drug_warning.efo_id',
        linkHrefProperty: 'drug_warning.efo_id',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://www.ebi.ac.uk/ols/search?q=${encodeURI(hrefValue)}`,
      }
    },
    'drug_warning.efo_id_for_warning_class': (generatorParams) => {
      return {
        linkTextProperty: 'drug_warning.efo_id_for_warning_class',
        linkHrefProperty: 'drug_warning.efo_id_for_warning_class',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://www.ebi.ac.uk/ols/search?q=${encodeURI(hrefValue)}`,
      }
    },
    'drug_warning.warning_refs_by_molecule': (generatorParams) => {
      return {
        baseChemblIDProperty: 'drug_warning.molecule_chembl_id',
        parentChemblIDProperty: 'drug_warning.parent_molecule_chembl_id',
      }
    },
  },
  [EntityNames.EubopenCompound.entityID]: {
    molecule_eubopen_id: (generatorParams) => {
      return {
        entityID: EntityNames.EubopenCompound.entityID,
        itemIDProperty: 'molecule_eubopen_id',
        linkTextProperty: 'molecule_eubopen_id',
        secondaryIDProperty: 'molecule_chembl_id',
        secondaryLinkTextProperty: 'molecule_chembl_id',
        showMoleculeStructure: true,
        structureIDProperty: 'molecule_chembl_id',
        highlightText: generatorParams.highlightText,
        moleculeStructure: {
          showOptions: true,
        },
      }
    },
    molecule_synonyms: (generatorParams) => {
      return {
        itemIDProperty: 'molecule_eubopen_id',
        entityID: EntityNames.EubopenCompound.entityID,
        synonymsType: 'molecule_synonyms',
        textOnly: true,
        highlightText: generatorParams.highlightText,
      }
    },
    '_metadata.related_assays.count': (generatorParams) => {
      return {
        itemIDProperty: 'molecule_eubopen_id',
        valueAccessor: '_metadata.related_assays.count',
        originEntityID: EntityNames.EubopenCompound.entityID,
        destinationEntityID: EntityNames.EubopenAssay.entityID,
      }
    },
    qt_data: (generatorParams) => {
      return {
        sectionKey: 'QualityControl',
      }
    },
    ac_data: (generatorParams) => {
      return {
        sectionKey: 'ActivityProfile',
      }
    },
  },
  [EntityNames.EubopenTarget.entityID]: {
    target_eubopen_id: (generatorParams) => {
      return {
        entityID: EntityNames.EubopenTarget.entityID,
        itemIDProperty: 'target_eubopen_id',
        linkTextProperty: 'target_eubopen_id',
        highlightText: generatorParams.highlightText,
      }
    },
    target_synonyms: (generatorParams) => {
      return {
        itemIDProperty: 'target_eubopen_id',
        entityID: EntityNames.EubopenTarget.entityID,
        valueAccessor: generatorParams.valueAccessor,
        synonymsType: 'target_synonyms',
        textOnly: true,
        highlightText: generatorParams.highlightText,
      }
    },
  },
  [EntityNames.EubopenActivity.entityID]: {
    target_pref_name: (generatorParams) => {
      return {
        entityID: EntityNames.EubopenTarget.entityID,
        itemIDProperty: 'target_chembl_id', // should change these to eubopen ids when they are in the index
        linkTextProperty: 'target_pref_name',
        fallbackLinkTextProperty: 'target_chembl_id',
      }
    },
    molecule_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.EubopenCompound.entityID,
        itemIDProperty: 'molecule_chembl_id',
        linkTextProperty: 'molecule_chembl_id',
        showMoleculeStructure: true,
        moleculeStructure: {
          showOptions: true,
        },
      }
    },
    assay_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.EubopenAssay.entityID,
        itemIDProperty: 'assay_chembl_id',
        linkTextProperty: 'assay_chembl_id',
      }
    },
    target_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.EubopenTarget.entityID,
        itemIDProperty: 'target_chembl_id',
        linkTextProperty: 'target_chembl_id',
      }
    },
    molecule_pref_name: (generatorParams) => {
      return {
        entityID: EntityNames.EubopenCompound.entityID,
        itemIDProperty: 'molecule_chembl_id',
        linkTextProperty: 'molecule_pref_name',
        fallbackLinkTextProperty: 'molecule_chembl_id',
      }
    },
  },
  [EntityNames.EubopenAssay.entityID]: {
    assay_eubopen_id: (generatorParams) => {
      return {
        entityID: EntityNames.EubopenAssay.entityID,
        itemIDProperty: 'assay_eubopen_id',
        linkTextProperty: 'assay_eubopen_id',
      }
    },
    target_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.EubopenTarget.entityID,
        itemIDProperty: 'target_chembl_id',
        linkTextProperty: 'target_chembl_id',
      }
    },
    document_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.Document.entityID,
        itemIDProperty: 'document_chembl_id',
        linkTextProperty: 'document_chembl_id',
      }
    },
    '_metadata.related_compounds.count': (generatorParams) => {
      return {
        itemIDProperty: 'assay_eubopen_id',
        valueAccessor: '_metadata.related_compounds.count',
        originEntityID: EntityNames.EubopenAssay.entityID,
        destinationEntityID: EntityNames.EubopenCompound.entityID,
      }
    },
  },
  [EntityNames.EubopenMechanismOfAction.entityID]: {
    'mechanism_of_action.ref_url': (generatorParams) => {
      return {
        linkTextProperty: 'mechanism_of_action.ref_url',
        linkHrefProperty: 'mechanism_of_action.ref_url',
        fallbackLinkTextProperty: 'mechanism_of_action.ref_url',
      }
    },
    'molecule.pref_name': (generatorParams) => {
      return {
        entityID: EntityNames.EubopenCompound.entityID,
        itemIDProperty: 'molecule.molecule_eubopen_id',
        linkTextProperty: 'molecule.pref_name',
        fallbackLinkTextProperty: 'molecule.molecule_eubopen_id',
      }
    },
  },
  [EntityNames.Compound.entityID]: {
    molecule_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.Compound.entityID,
        linkTextProperty: 'molecule_chembl_id',
        linkHrefProperty: 'molecule_chembl_id',
        itemIDProperty: 'molecule_chembl_id',
        showMoleculeStructure: true,
        enableStructureSearch: true,
        moleculeStructure: {
          showOptions: true,
        },
        highlightText: generatorParams.highlightText,
      }
    },
    molecule_synonyms: (generatorParams) => {
      return {
        itemIDProperty: 'molecule_chembl_id',
        entityID: EntityNames.Compound.entityID,
        synonymsType: 'molecule_synonyms',
        textOnly: true,
        highlightText: generatorParams.highlightText,
      }
    },
    '_metadata.related_targets.count': (generatorParams) => {
      return {
        itemIDProperty: 'molecule_chembl_id',
        valueAccessor: '_metadata.related_targets.count',
        originEntityID: EntityNames.Compound.entityID,
        destinationEntityID: EntityNames.Target.entityID,
      }
    },
    '_metadata.related_activities.count': (generatorParams) => {
      return {
        itemIDProperty: 'molecule_chembl_id',
        valueAccessor: '_metadata.related_activities.count',
        originEntityID: EntityNames.Compound.entityID,
        destinationEntityID: EntityNames.Activity.entityID,
      }
    },
    trade_names: (generatorParams) => {
      return {
        itemIDProperty: 'molecule_chembl_id',
        entityID: EntityNames.Compound.entityID,
        synonymsType: 'molecule_synonyms',
        textOnly: true,
        tradeNames: true,
      }
    },
    similarity: (generatorParams) => {
      return {
        parseFunction: (value) => {
          if (value !== DefaultValues.DefaultNullTextValue) {
            const number = parseFloat(value)
            const formattedNumber = number.toFixed(2)
            return `${formattedNumber}%`
          }
          return DefaultValues.DefaultNullTextValue
        },
      }
    },
  },
  [EntityNames.Drug.entityID]: {
    drug_parent_molecule_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.Compound.entityID,
        linkTextProperty: 'molecule_chembl_id',
        linkHrefProperty: 'molecule_chembl_id',
        itemIDProperty: 'molecule_chembl_id',
        showMoleculeStructure: true,
        enableStructureSearch: true,
        moleculeStructure: {
          showOptions: true,
        },
        highlightText: generatorParams.highlightText,
      }
    },
    molecule_synonyms: (generatorParams) => {
      return {
        itemIDProperty: 'molecule_chembl_id',
        entityID: EntityNames.Compound.entityID,
        synonymsType: 'molecule_synonyms',
        textOnly: true,
        drugSynonyms: true,
        highlightText: generatorParams.highlightText,
      }
    },
    research_codes: (generatorParams) => {
      return {
        itemIDProperty: 'molecule_chembl_id',
        entityID: EntityNames.Compound.entityID,
        synonymsType: 'molecule_synonyms',
        textOnly: true,
        drugResearchCodes: true,
        highlightText: generatorParams.highlightText,
      }
    },
    drug_atc_codes: (generatorParams) => {
      return {
        parseFunction: (value) => {
          if (value !== DefaultValues.DefaultNullTextValue) {
            return value.map((data) => data.level5).join(', ')
          }
          return DefaultValues.DefaultNullTextValue
        },
      }
    },
    drug_atc_codes_level_4: (generatorParams) => {
      return {
        parseFunction: (value) => {
          if (value !== DefaultValues.DefaultNullTextValue) {
            return value.map((data) => data.level4_description).join(', ')
          }
          return DefaultValues.DefaultNullTextValue
        },
      }
    },
    drug_atc_codes_level_3: (generatorParams) => {
      return {
        parseFunction: (value) => {
          if (value !== DefaultValues.DefaultNullTextValue) {
            return value.map((data) => data.level3_description).join(', ')
          }
          return DefaultValues.DefaultNullTextValue
        },
      }
    },
    drug_atc_codes_level_2: (generatorParams) => {
      return {
        parseFunction: (value) => {
          if (value !== DefaultValues.DefaultNullTextValue) {
            return value.map((data) => data.level2_description).join(', ')
          }
          return DefaultValues.DefaultNullTextValue
        },
      }
    },
    drug_atc_codes_level_1: (generatorParams) => {
      return {
        parseFunction: (value) => {
          if (value !== DefaultValues.DefaultNullTextValue) {
            return value.map((data) => data.level1_description).join(', ')
          }
          return DefaultValues.DefaultNullTextValue
        },
      }
    },
  },
  [EntityNames.Target.entityID]: {
    target_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.Target.entityID,
        linkTextProperty: 'target_chembl_id',
        linkHrefProperty: 'target_chembl_id',
        itemIDProperty: 'target_chembl_id',
        highlightText: generatorParams.highlightText,
      }
    },
    target_synonyms: (generatorParams) => {
      return {
        itemIDProperty: 'target_chembl_id',
        entityID: EntityNames.Target.entityID,
        synonymsType: 'target_synonyms',
        textOnly: true,
      }
    },
    tax_id: (generatorParams) => {
      return {
        linkTextProperty: 'tax_id',
        linkHrefProperty: 'tax_id',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=${hrefValue}`,
      }
    },
    '_metadata.related_compounds.count': (generatorParams) => {
      return {
        itemIDProperty: 'target_chembl_id',
        valueAccessor: '_metadata.related_compounds.count',
        originEntityID: EntityNames.Target.entityID,
        destinationEntityID: EntityNames.Compound.entityID,
      }
    },
    '_metadata.related_activities.count': (generatorParams) => {
      return {
        itemIDProperty: 'target_chembl_id',
        valueAccessor: '_metadata.related_activities.count',
        originEntityID: EntityNames.Target.entityID,
        destinationEntityID: EntityNames.Activity.entityID,
      }
    },
  },
  [EntityNames.Assay.entityID]: {
    assay_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.Assay.entityID,
        linkTextProperty: 'assay_chembl_id',
        linkHrefProperty: 'assay_chembl_id',
        itemIDProperty: 'assay_chembl_id',
        highlightText: generatorParams.highlightText,
      }
    },
    document_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.Document.entityID,
        linkTextProperty: 'document_chembl_id',
        linkHrefProperty: 'document_chembl_id',
        itemIDProperty: 'document_chembl_id',
      }
    },
    tissue_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.Tissue.entityID,
        linkTextProperty: 'tissue_chembl_id',
        linkHrefProperty: 'tissue_chembl_id',
        itemIDProperty: 'tissue_chembl_id',
      }
    },
    '_metadata.document_data.pubmed_id': (generatorParams) => {
      return {
        linkTextProperty: '_metadata.document_data.pubmed_id',
        linkHrefProperty: '_metadata.document_data.pubmed_id',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://www.ncbi.nlm.nih.gov/pubmed/${hrefValue}`,
      }
    },
    '_metadata.document_data.doi': (generatorParams) => {
      return {
        linkTextProperty: '_metadata.document_data.doi',
        linkHrefProperty: '_metadata.document_data.doi',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://dx.doi.org/${encodeURIComponent(hrefValue)}`,
      }
    },
    'variant_sequence.accession': (generatorParams) => {
      return {
        linkTextProperty: 'variant_sequence.accession',
        linkHrefProperty: 'variant_sequence.accession',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://www.uniprot.org/uniprot/${hrefValue}`,
      }
    },
    '_metadata.related_compounds.count': (generatorParams) => {
      return {
        itemIDProperty: 'assay_chembl_id',
        valueAccessor: '_metadata.related_compounds.count',
        originEntityID: EntityNames.Assay.entityID,
        destinationEntityID: EntityNames.Compound.entityID,
      }
    },
    '_metadata.related_activities.count': (generatorParams) => {
      return {
        itemIDProperty: 'assay_chembl_id',
        valueAccessor: '_metadata.related_activities.count',
        originEntityID: EntityNames.Assay.entityID,
        destinationEntityID: EntityNames.Activity.entityID,
      }
    },
  },
  [EntityNames.Document.entityID]: {
    document_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.Document.entityID,
        linkTextProperty: 'document_chembl_id',
        linkHrefProperty: 'document_chembl_id',
        itemIDProperty: 'document_chembl_id',
        highlightText: generatorParams.highlightText,
      }
    },
    pubmed_id: (generatorParams) => {
      return {
        linkTextProperty: 'pubmed_id',
        linkHrefProperty: 'pubmed_id',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://www.ncbi.nlm.nih.gov/pubmed/${hrefValue}`,
      }
    },
    doi: (generatorParams) => {
      return {
        linkTextProperty: 'doi',
        linkHrefProperty: 'doi',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://dx.doi.org/${encodeURI(hrefValue)}`,
      }
    },
    patent_id: (generatorParams) => {
      return {
        linkTextProperty: 'patent_id',
        linkHrefProperty: 'patent_id',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://www.surechembl.org/document/${encodeURI(hrefValue)}`,
      }
    },
    '_metadata.related_activities.count': (generatorParams) => {
      return {
        itemIDProperty: 'document_chembl_id',
        valueAccessor: '_metadata.related_activities.count',
        originEntityID: EntityNames.Document.entityID,
        destinationEntityID: EntityNames.Activity.entityID,
      }
    },
    '_metadata.related_compounds.count': (generatorParams) => {
      return {
        itemIDProperty: 'document_chembl_id',
        valueAccessor: '_metadata.related_compounds.count',
        originEntityID: EntityNames.Document.entityID,
        destinationEntityID: EntityNames.Compound.entityID,
      }
    },
    '_metadata.related_targets.count': (generatorParams) => {
      return {
        itemIDProperty: 'document_chembl_id',
        valueAccessor: '_metadata.related_targets.count',
        originEntityID: EntityNames.Document.entityID,
        destinationEntityID: EntityNames.Target.entityID,
      }
    },
    '_metadata.similar_documents.pubmed_id': (generatorParams) => {
      return {
        linkTextProperty: '_metadata.similar_documents.pubmed_id',
        linkHrefProperty: '_metadata.similar_documents.pubmed_id',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://www.ncbi.nlm.nih.gov/pubmed/${hrefValue}`,
      }
    },
    '_metadata.similar_documents.doi': (generatorParams) => {
      return {
        linkTextProperty: '_metadata.similar_documents.doi',
        linkHrefProperty: '_metadata.similar_documents.doi',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://dx.doi.org/${encodeURIComponent(hrefValue)}`,
      }
    },
  },
  [EntityNames.CellLine.entityID]: {
    cell_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.CellLine.entityID,
        linkTextProperty: 'cell_chembl_id',
        linkHrefProperty: 'cell_chembl_id',
        itemIDProperty: 'cell_chembl_id',
        highlightText: generatorParams.highlightText,
      }
    },
    cell_source_tax_id: (generatorParams) => {
      return {
        linkTextProperty: 'cell_source_tax_id',
        linkHrefProperty: 'cell_source_tax_id',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=${hrefValue}`,
      }
    },
    clo_id: (generatorParams) => {
      return {
        linkTextProperty: 'clo_id',
        linkHrefProperty: 'clo_id',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://purl.obolibrary.org/obo/${hrefValue}`,
      }
    },
    efo_id: (generatorParams) => {
      return {
        linkTextProperty: 'efo_id',
        linkHrefProperty: 'efo_id',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `http://www.ebi.ac.uk/efo/${encodeURI(hrefValue)}`,
      }
    },
    cellosaurus_id: (generatorParams) => {
      return {
        linkTextProperty: 'cellosaurus_id',
        linkHrefProperty: 'cellosaurus_id',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://web.expasy.org/cellosaurus/${encodeURI(hrefValue)}`,
      }
    },
    cl_lincs_id: (generatorParams) => {
      return {
        linkTextProperty: 'cl_lincs_id',
        linkHrefProperty: 'cl_lincs_id',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://life.ccs.miami.edu/life/summary?mode=CellLine&source=LINCS&input=${hrefValue}`,
      }
    },
    '_metadata.related_activities.count': (generatorParams) => {
      return {
        itemIDProperty: 'cell_chembl_id',
        valueAccessor: '_metadata.related_activities.count',
        originEntityID: EntityNames.CellLine.entityID,
        destinationEntityID: EntityNames.Activity.entityID,
      }
    },
    '_metadata.related_compounds.count': (generatorParams) => {
      return {
        itemIDProperty: 'cell_chembl_id',
        valueAccessor: '_metadata.related_compounds.count',
        originEntityID: EntityNames.CellLine.entityID,
        destinationEntityID: EntityNames.Compound.entityID,
      }
    },
  },
  [EntityNames.Tissue.entityID]: {
    tissue_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.Tissue.entityID,
        linkTextProperty: 'tissue_chembl_id',
        linkHrefProperty: 'tissue_chembl_id',
        itemIDProperty: 'tissue_chembl_id',
        highlightText: generatorParams.highlightText,
      }
    },
    uberon_id: (generatorParams) => {
      return {
        linkTextProperty: 'uberon_id',
        linkHrefProperty: 'uberon_id',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://www.ebi.ac.uk/ols/search?q=${encodeURI(hrefValue)}`,
      }
    },
    efo_id: (generatorParams) => {
      return {
        linkTextProperty: 'efo_id',
        linkHrefProperty: 'efo_id',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://www.ebi.ac.uk/ols/search?q=${encodeURI(hrefValue)}`,
      }
    },
    bto_id: (generatorParams) => {
      return {
        linkTextProperty: 'bto_id',
        linkHrefProperty: 'bto_id',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://www.ebi.ac.uk/ols/search?q=${encodeURI(hrefValue)}`,
      }
    },
    caloha_id: (generatorParams) => {
      return {
        linkTextProperty: 'caloha_id',
        linkHrefProperty: 'caloha_id',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://www.nextprot.org/term/${encodeURI(hrefValue)}`,
      }
    },
    '_metadata.related_activities.count': (generatorParams) => {
      return {
        itemIDProperty: 'tissue_chembl_id',
        valueAccessor: '_metadata.related_activities.count',
        originEntityID: EntityNames.Tissue.entityID,
        destinationEntityID: EntityNames.Activity.entityID,
      }
    },
    '_metadata.related_compounds.count': (generatorParams) => {
      return {
        itemIDProperty: 'tissue_chembl_id',
        valueAccessor: '_metadata.related_compounds.count',
        originEntityID: EntityNames.Tissue.entityID,
        destinationEntityID: EntityNames.Compound.entityID,
      }
    },
  },
  [EntityNames.DrugIndication.entityID]: {
    'parent_molecule.molecule_chembl_id': (generatorParams) => {
      return {
        entityID: EntityNames.Compound.entityID,
        itemIDProperty: 'parent_molecule.molecule_chembl_id',
        linkTextProperty: 'parent_molecule.molecule_chembl_id',
        showMoleculeStructure: true,
        enableStructureSearch: true,
        moleculeStructure: {
          showOptions: true,
        },
      }
    },
    'drug_indication.mesh_id': (generatorParams) => {
      return {
        linkTextProperty: 'drug_indication.mesh_id',
        linkHrefProperty: 'drug_indication.mesh_id',
        highlightText: generatorParams.highlightText,
        hrefGenerator: (hrefValue) =>
          `https://id.nlm.nih.gov/mesh/${hrefValue}.html`,
      }
    },
    efo_ids: (generatorParams) => {
      return {
        valueAccessor: 'drug_indication.efo',
        inArrayAccessor: 'id',
        showOnlyHighlights: false,
        hrefGenerator: (hrefValue) =>
          `http://www.ebi.ac.uk/efo/${encodeURI(hrefValue.replace(':', '_'))}`,
      }
    },
    efo_terms: (generatorParams) => {
      return {
        valueAccessor: 'drug_indication.efo',
        inArrayAccessor: 'term',
        showOnlyHighlights: false,
      }
    },
    'drug_indication.efo': (generatorParams) => {
      return {
        valueAccessor: 'drug_indication.efo',
        inArrayAccessor: 'term',
        showOnlyHighlights: false,
      }
    },
    'parent_molecule._metadata.drug.drug_data.synonyms': (generatorParams) => {
      return {
        itemIDProperty: 'parent_molecule.molecule_chembl_id',
        entityID: EntityNames.Compound.entityID,
        synonymsType: 'molecule_synonyms',
        textOnly: true,
        highlightText: generatorParams.highlightText,
      }
    },
    'drug_indication.indication_refs_by_molecule': (generatorParams) => {
      return {
        baseChemblIDProperty: 'drug_indication.molecule_chembl_id',
        parentChemblIDProperty: 'parent_molecule.molecule_chembl_id',
      }
    },
  },

  [EntityNames.DrugMechanism.entityID]: {
    'parent_molecule.molecule_chembl_id': (generatorParams) => {
      return {
        entityID: EntityNames.Compound.entityID,
        itemIDProperty: 'parent_molecule.molecule_chembl_id',
        linkTextProperty: 'parent_molecule.molecule_chembl_id',
        showMoleculeStructure: true,
        enableStructureSearch: true,
        moleculeStructure: {
          showOptions: true,
        },
      }
    },
    'target.target_chembl_id': (generatorParams) => {
      return {
        entityID: EntityNames.Target.entityID,
        itemIDProperty: 'target.target_chembl_id',
        linkTextProperty: 'target.target_chembl_id',
      }
    },
    'mechanism_of_action.mechanism_refs_by_molecule': (generatorParams) => {
      return {
        baseChemblIDProperty: 'mechanism_of_action.molecule_chembl_id',
        parentChemblIDProperty: 'parent_molecule.molecule_chembl_id',
      }
    },
    drug_atc_codes: (generatorParams) => {
      return {
        inArrayAccessor: 'level5',

        showAsUL: true,
      }
    },
    drug_atc_codes_level_4: (generatorParams) => {
      return {
        inArrayAccessor: 'level4_description',

        showAsUL: true,
      }
    },
    drug_atc_codes_level_3: (generatorParams) => {
      return {
        inArrayAccessor: 'level3_description',
        showAsUL: true,
      }
    },
    drug_atc_codes_level_2: (generatorParams) => {
      return {
        inArrayAccessor: 'level2_description',
        showAsUL: true,
      }
    },
    drug_atc_codes_level_1: (generatorParams) => {
      return {
        inArrayAccessor: 'level1_description',
        showAsUL: true,
      }
    },
  },
  [EntityNames.EubopenMainTarget.entityID]: {
    'target.pref_name': (generatorParams) => {
      return {
        entityID: EntityNames.EubopenTarget.entityID,
        itemIDProperty: 'target.target_eubopen_id',
        linkTextProperty: 'target.pref_name',
        fallbackLinkTextProperty: 'target.target_eubopen_id',
      }
    },
    'molecule.pref_name': (generatorParams) => {
      return {
        entityID: EntityNames.EubopenCompound.entityID,
        itemIDProperty: 'molecule.molecule_eubopen_id',
        linkTextProperty: 'molecule.pref_name',
        fallbackLinkTextProperty: 'molecule.molecule_eubopen_id',
      }
    },
    'molecule.molecule_eubopen_id': (generatorParams) => {
      return {
        entityID: EntityNames.EubopenCompound.entityID,
        itemIDProperty: 'molecule.molecule_eubopen_id',
        structureIDProperty: 'molecule.molecule_chembl_id',
        showMoleculeStructure: true,
        moleculeStructure: {
          showOptions: true,
        },
      }
    },
  },
  [EntityNames.Activity.entityID]: {
    molecule_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.Compound.entityID,
        linkTextProperty: 'molecule_chembl_id',
        linkHrefProperty: 'molecule_chembl_id',
        itemIDProperty: 'molecule_chembl_id',
        showMoleculeStructure: true,
        enableStructureSearch: true,
        moleculeStructure: {
          showOptions: true,
        },
        highlightText: generatorParams.highlightText,
      }
    },
    assay_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.Assay.entityID,
        itemIDProperty: 'assay_chembl_id',
        linkTextProperty: 'assay_chembl_id',
      }
    },
    target_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.Target.entityID,
        linkTextProperty: 'target_chembl_id',
        linkHrefProperty: 'target_chembl_id',
        itemIDProperty: 'target_chembl_id',
        highlightText: generatorParams.highlightText,
      }
    },
    document_chembl_id: (generatorParams) => {
      return {
        entityID: EntityNames.Document.entityID,
        linkTextProperty: 'document_chembl_id',
        linkHrefProperty: 'document_chembl_id',
        itemIDProperty: 'document_chembl_id',
      }
    },
    '_metadata.assay_data.cell_chembl_id': (generatorParams) => {
      return {
        entityID: EntityNames.CellLine.entityID,
        linkTextProperty: '_metadata.assay_data.cell_chembl_id',
        linkHrefProperty: '_metadata.assay_data.cell_chembl_id',
        itemIDProperty: '_metadata.assay_data.cell_chembl_id',
        highlightText: generatorParams.highlightText,
      }
    },
    '_metadata.assay_data.tissue_chembl_id': (generatorParams) => {
      return {
        entityID: EntityNames.Tissue.entityID,
        linkTextProperty: '_metadata.assay_data.tissue_chembl_id',
        linkHrefProperty: '_metadata.assay_data.tissue_chembl_id',
        itemIDProperty: '_metadata.assay_data.tissue_chembl_id',
      }
    },
    '_metadata.assay_data.assay_parameters': (generatorParams) => {
      return {
        assayParamsProperty: '_metadata.assay_data.assay_parameters',
      }
    },
  },
}

const defaultFunction = (generatorParams) => {
  return generatorParams
}

const methods = {
  getGeneratorFunctionForPoperty(entityID, propID) {
    const configForEntity = config[entityID]
    if (configForEntity == null) {
      return defaultFunction
    }

    const functionForProperty = configForEntity[propID]
    if (functionForProperty == null) {
      return defaultFunction
    }

    return (generatorParams) => {
      const customParams = functionForProperty(generatorParams)

      return {
        ...generatorParams,
        ...customParams,
      }
    }
  },
}

export default {
  config,
  methods,
}
