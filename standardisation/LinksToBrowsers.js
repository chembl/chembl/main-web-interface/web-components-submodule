import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'
import URLUtils from '~/web-components-submodule/utils/URLUtils.js'

const browserNames = {
  [EntityNames.DrugWarning.entityID]: 'drug_warnings',
  [EntityNames.DrugIndication.entityID]: 'drug_indications',
  [EntityNames.DrugMechanism.entityID]: 'drug_mechanisms',
  [EntityNames.EubopenTarget.entityID]: 'target',
  [EntityNames.EubopenCompound.entityID]: 'compound',
  [EntityNames.EubopenAssay.entityID]: 'assay',
  [EntityNames.Compound.entityID]: 'compounds',
  [EntityNames.Drug.entityID]: 'drugs',
  [EntityNames.Target.entityID]: 'targets',
  [EntityNames.Assay.entityID]: 'assays',
  [EntityNames.Document.entityID]: 'documents',
  [EntityNames.CellLine.entityID]: 'cell_lines',
  [EntityNames.Tissue.entityID]: 'tissues',
  [EntityNames.Activity.entityID]: 'activities',
}

const linksToBrowsers = {
  entityJoinDestinationTemplate: process.env.entityJoinDestinationTemplate,
  buildURLForEntityBrowser(
    destinationEntityID,
    desiredState,
    stateType = 'state_id',
    description,
    fragment = false
  ) {
    const browserName = browserNames[destinationEntityID]

    let generatedState =
      desiredState == null
        ? ''
        : encodeURIComponent(URLUtils.sanitiseTextForEscaping(desiredState))

    if (['filter', 'querystring'].includes(stateType)) {
      generatedState = `QUERYSTRING:${generatedState}`
    }
    if (stateType === 'state_id') {
      generatedState =
        desiredState !== '' && desiredState != null
          ? `STATE_ID:${generatedState}`
          : ''
    }

    let browserURL = process.env.entityBrowserStateUrlTemplateNew
      .replace('<BROWSER_NAME>', browserName)
      .replace('<GENERATED_STATE>', generatedState)

    const descriptionString =
      description != null
        ? `?description=${encodeURIComponent(description)}`
        : ''
    browserURL = `${browserURL}${descriptionString}`

    if (fragment) {
      const protocol = window.location.protocol
      const host = window.location.host
      const basePath = process.env.basePath
      const baseUrl = `${protocol}//${host}${basePath}`

      return browserURL.replace(baseUrl, '')
    }

    return browserURL
  },
  buildURLForEntityBrowserStateIDAndRedirect(
    destinationEntityID,
    desiredStateHash,
    isEmbedded
  ) {
    const destinationURL = linksToBrowsers.buildURLForEntityBrowser(
      destinationEntityID,
      desiredStateHash
    )

    if (isEmbedded) {
      window.parent.location.href = destinationURL
    } else {
      window.location.href = destinationURL
    }
  },
  getEntityIDFromBrowserName(browserName) {
    return Object.keys(browserNames).find(
      (entityID) => browserNames[entityID] === browserName
    )
  },
  getBrowserNameFromEntityID(entityID) {
    return browserNames[entityID]
  },
}

export default linksToBrowsers
