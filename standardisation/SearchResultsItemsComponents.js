import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'
import SlotComponentNames from '~/web-components-submodule/standardisation/SlotComponentNames.js'

const searchResultsItemsComponents = {
  [EntityNames.Compound
    .entityID]: SlotComponentNames.getCompoundsResultsItemComponentName(),
  [EntityNames.Document
    .entityID]: SlotComponentNames.getDocumentResultsItemComponentName(),
  [EntityNames.Target
    .entityID]: SlotComponentNames.getTargetResultsItemComponentName(),
  [EntityNames.Assay
    .entityID]: SlotComponentNames.getAssayResultsItemComponentName(),
  [EntityNames.CellLine
    .entityID]: SlotComponentNames.getCellLineResultsItemComponentName(),
  [EntityNames.Tissue
    .entityID]: SlotComponentNames.getTissueResultsItemComponentName(),
  [EntityNames.EubopenCompound
    .entityID]: SlotComponentNames.getEubopenCompoundsResultsItemComponentName(),
  [EntityNames.EubopenTarget
    .entityID]: SlotComponentNames.getEubopenTargetsResultsItemComponentName(),
}

export default searchResultsItemsComponents
