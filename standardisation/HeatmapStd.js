const axesNames = {
  xAxis: {
    axisID: 'xAxis',
    wsString: 'x_axis',
  },
  yAxis: {
    axisID: 'yAxis',
    wsString: 'y_axis',
  },
  cells: {
    axisID: 'cells',
    wsString: 'cells',
  },
}

const dataWindowsNames = {
  headersDataWindow: {
    windowID: 'headersDataWindow',
    wsString: 'headers',
  },
  footersDataWindow: {
    windowID: 'footersDataWindow',
    wsString: 'footers',
  },
}

const MAX_ITEMS_FOR_HEATMAP = 100

export default { axesNames, dataWindowsNames, MAX_ITEMS_FOR_HEATMAP }
