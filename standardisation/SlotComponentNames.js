export default {
  getDefaultComponentName() {
    return 'Default'
  },
  getATCClassificationsComponentName() {
    return 'ATCClassifications'
  },
  getPatentComponentName() {
    return 'Patent'
  },
  getTargetComponentsComponentName() {
    return 'TargetComponents'
  },
  getProteinClassificationsComponentName() {
    return 'ProteinClassifications'
  },
  getChEMBLLinkName() {
    return 'ChEMBL-Link'
  },
  getWarningWhereName() {
    return 'WarningWhere'
  },
  getReferencesComponentName() {
    return 'References'
  },
  getReferencesByParentComponentName() {
    return 'ReferencesByParent'
  },
  getCompoundsResultsItemComponentName() {
    return 'CompoundsSearchResultsItem'
  },
  getDocumentResultsItemComponentName() {
    return 'DocumentSearchResultsItem'
  },
  getTargetResultsItemComponentName() {
    return 'TargetSearchResultsItem'
  },
  getAssayResultsItemComponentName() {
    return 'AssaySearchResultsItem'
  },
  getCellLineResultsItemComponentName() {
    return 'CellLineSearchResultsItem'
  },
  getTissueResultsItemComponentName() {
    return 'TissueSearchResultsItem'
  },
  getEubopenCompoundsResultsItemComponentName() {
    return 'EubopenCompoundsSearchResultsItem'
  },
  getEubopenTargetsResultsItemComponentName() {
    return 'EubopenTargetsSearchResultsItem'
  },
  getSynonymsName() {
    return 'Synonyms'
  },
  getTextComponentName() {
    return 'Text'
  },
  getExternalLinkComponentName() {
    return 'ExternalLinkComponent'
  },
  getBooleanValueComponentName() {
    return 'BooleanValue'
  },
  getEubopenChemicalStructureTextComponentName() {
    return 'EubopenChemicalStructureText'
  },
  getEubopenMoleculeClassComponentName() {
    return 'EubopenMoleculeClass'
  },
  getUniprotAcessionsComponentName() {
    return 'UniprotAcessions'
  },
  getAssayParametersComponentName() {
    return 'AssayParameters'
  },
  getActivityPropertiesComponentName() {
    return 'ActivityProperties'
  },
  getSourcesComponentName() {
    return 'Sources'
  },
  getRelatedEntitiesNumberComponentName() {
    return 'RelatedEntitiesNumberComponent'
  },
  getAlternateFormsComponentName() {
    return 'AlternateFormsComponent'
  },
  getTextsListComponentName() {
    return 'TextsListComponent'
  },
  getOrganismTaxonomyComponentName() {
    return 'OrganismTaxonomyComponent'
  },
  getMultipleOrganismTaxonomiesComponentName() {
    return 'MultipleOrganismTaxonomies'
  },
  getMaxPhaseSlotComponentName() {
    return 'MaxPhaseSlotComponent'
  },
  getEubopenAssaysForCompoundComponentName() {
    return 'EubopenAssaysForCompoundComponent'
  },
  getEubopenCompoundACComponentName() {
    return 'EubopenCompoundACComponent'
  },
  getDrugIconsComponentName() {
    return 'DrugIconsComponent'
  },
}
