const joinModes = {
  allItems: 'all_items',
  selection: 'selection',
}

export default joinModes
