const generateBLASTSearchContext = (searchJobID) => {
  return {
    context_type: 'BLAST',
    context_id: searchJobID,
    delayed_jobs_base_url: process.env.delayedJobsBaseUrl,
  }
}
const generateSimilaritySearchContext = (term, threshold) => {
  return {
    context_type: 'DIRECT_SIMILARITY',
    context_id: term,
    similarity_threshold: threshold,
    web_services_base_url: process.env.chemblWSBaseUrl,
  }
}

const generateSubstructureSearchContext = (searchJobID) => {
  return {
    context_type: 'SUBSTRUCTURE',
    context_id: searchJobID,
    delayed_jobs_base_url: process.env.delayedJobsBaseUrl,
  }
}

const generateSearchByIDsContext = (searchJobID, destination) => {
  return {
    context_type:
      destination === 'CHEMBL_COMPOUNDS' ? 'TO_COMPOUNDS' : 'TO_TARGETS',
    context_id: searchJobID,
    delayed_jobs_base_url: process.env.delayedJobsBaseUrl,
  }
}

export default {
  generateBLASTSearchContext,
  generateSimilaritySearchContext,
  generateSubstructureSearchContext,
  generateSearchByIDsContext,
}
