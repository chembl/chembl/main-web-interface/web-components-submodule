const selectionModes = {
  allItemsExcept: 'allItemsExcept',
  noItemsExcept: 'noItemsExcept',
}

export default selectionModes
