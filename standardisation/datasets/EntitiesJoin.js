import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'

const originsDestinationsConfig = {
  from: {
    [EntityNames.Compound.entityID]: {
      originParamName: 'CHEMBL_COMPOUNDS',
      to: {
        [EntityNames.Activity.entityID]: {
          destinationParamName: 'CHEMBL_ACTIVITIES',
        },
        [EntityNames.Drug.entityID]: {
          destinationParamName: 'CHEMBL_DRUGS',
        },
        [EntityNames.DrugMechanism.entityID]: {
          destinationParamName: 'CHEMBL_DRUG_MECHANISMS',
        },
        [EntityNames.DrugIndication.entityID]: {
          destinationParamName: 'CHEMBL_DRUG_INDICATIONS',
        },
        [EntityNames.Target.entityID]: {
          destinationParamName: 'CHEMBL_TARGETS',
          hiddenFromGeneralJoin: true,
        },
      },
    },
    [EntityNames.Drug.entityID]: {
      originParamName: 'CHEMBL_DRUGS',
      to: {
        [EntityNames.Activity.entityID]: {
          destinationParamName: 'CHEMBL_ACTIVITIES',
        },
        [EntityNames.DrugMechanism.entityID]: {
          destinationParamName: 'CHEMBL_DRUG_MECHANISMS',
        },
        [EntityNames.DrugIndication.entityID]: {
          destinationParamName: 'CHEMBL_DRUG_INDICATIONS',
        },
        [EntityNames.Target.entityID]: {
          destinationParamName: 'CHEMBL_TARGETS',
          hiddenFromGeneralJoin: true,
        },
      },
    },
    [EntityNames.Target.entityID]: {
      originParamName: 'CHEMBL_TARGETS',
      to: {
        [EntityNames.Activity.entityID]: {
          destinationParamName: 'CHEMBL_ACTIVITIES',
        },
        [EntityNames.DrugMechanism.entityID]: {
          destinationParamName: 'CHEMBL_DRUG_MECHANISMS',
        },
        [EntityNames.Compound.entityID]: {
          destinationParamName: 'CHEMBL_COMPOUNDS',
          hiddenFromGeneralJoin: true,
        },
      },
    },
    [EntityNames.Assay.entityID]: {
      originParamName: 'CHEMBL_ASSAYS',
      to: {
        [EntityNames.Activity.entityID]: {
          destinationParamName: 'CHEMBL_ACTIVITIES',
        },
        [EntityNames.Compound.entityID]: {
          destinationParamName: 'CHEMBL_COMPOUNDS',
          hiddenFromGeneralJoin: true,
        },
      },
    },
    [EntityNames.Document.entityID]: {
      originParamName: 'CHEMBL_DOCUMENTS',
      to: {
        [EntityNames.Activity.entityID]: {
          destinationParamName: 'CHEMBL_ACTIVITIES',
        },
        [EntityNames.Compound.entityID]: {
          destinationParamName: 'CHEMBL_COMPOUNDS',
          hiddenFromGeneralJoin: true,
        },
        [EntityNames.Target.entityID]: {
          destinationParamName: 'CHEMBL_TARGETS',
          hiddenFromGeneralJoin: true,
        },
      },
    },
    [EntityNames.CellLine.entityID]: {
      originParamName: 'CHEMBL_CELL_LINES',
      to: {
        [EntityNames.Activity.entityID]: {
          destinationParamName: 'CHEMBL_ACTIVITIES',
        },
        [EntityNames.Compound.entityID]: {
          destinationParamName: 'CHEMBL_COMPOUNDS',
          hiddenFromGeneralJoin: true,
        },
      },
    },
    [EntityNames.Tissue.entityID]: {
      originParamName: 'CHEMBL_TISSUES',
      to: {
        [EntityNames.Activity.entityID]: {
          destinationParamName: 'CHEMBL_ACTIVITIES',
        },
        [EntityNames.Compound.entityID]: {
          destinationParamName: 'CHEMBL_COMPOUNDS',
          hiddenFromGeneralJoin: true,
        },
      },
    },
    [EntityNames.DrugWarning.entityID]: {
      originParamName: 'CHEMBL_DRUG_WARNINGS',
      to: {
        [EntityNames.Activity.entityID]: {
          destinationParamName: 'CHEMBL_ACTIVITIES',
        },
        [EntityNames.Compound.entityID]: {
          destinationParamName: 'CHEMBL_COMPOUNDS',
        },
        [EntityNames.Drug.entityID]: {
          destinationParamName: 'CHEMBL_DRUGS',
        },
        [EntityNames.Drug.entityID]: {
          destinationParamName: 'CHEMBL_DRUGS',
        },
        [EntityNames.DrugMechanism.entityID]: {
          destinationParamName: 'CHEMBL_DRUG_MECHANISMS',
        },
        [EntityNames.DrugIndication.entityID]: {
          destinationParamName: 'CHEMBL_DRUG_INDICATIONS',
        },
      },
    },
    [EntityNames.DrugIndication.entityID]: {
      originParamName: 'CHEMBL_DRUG_INDICATIONS',
      to: {
        [EntityNames.Drug.entityID]: {
          destinationParamName: 'CHEMBL_DRUGS',
        },
        [EntityNames.Compound.entityID]: {
          destinationParamName: 'CHEMBL_COMPOUNDS',
        },
      },
    },
    [EntityNames.DrugMechanism.entityID]: {
      originParamName: 'CHEMBL_DRUG_MECHANISMS',
      to: {
        [EntityNames.Drug.entityID]: {
          destinationParamName: 'CHEMBL_DRUGS',
        },
        [EntityNames.Compound.entityID]: {
          destinationParamName: 'CHEMBL_COMPOUNDS',
        },
        [EntityNames.Target.entityID]: {
          destinationParamName: 'CHEMBL_TARGETS',
        },
      },
    },
    [EntityNames.Activity.entityID]: {
      originParamName: 'CHEMBL_ACTIVITIES',
      to: {
        [EntityNames.Compound.entityID]: {
          destinationParamName: 'CHEMBL_COMPOUNDS',
        },
        [EntityNames.Target.entityID]: {
          destinationParamName: 'CHEMBL_TARGETS',
        },
        [EntityNames.Assay.entityID]: {
          destinationParamName: 'CHEMBL_ASSAYS',
        },
        [EntityNames.Document.entityID]: {
          destinationParamName: 'CHEMBL_DOCUMENTS',
        },
      },
    },
    [EntityNames.EubopenCompound.entityID]: {
      originParamName: 'EUBOPEN_COMPOUND',
      to: {
        [EntityNames.EubopenAssay.entityID]: {
          destinationParamName: 'EUBOPEN_ASSAY',
          hiddenFromGeneralJoin: true,
        },
      },
    },
    [EntityNames.EubopenTarget.entityID]: {
      originParamName: 'EUBOPEN_TARGET',
      to: {},
    },
    [EntityNames.EubopenAssay.entityID]: {
      originParamName: 'EUBOPEN_ASSAY',
      to: {
        [EntityNames.EubopenCompound.entityID]: {
          destinationParamName: 'EUBOPEN_COMPOUND',
          hiddenFromGeneralJoin: true,
        },
      },
    },
    [EntityNames.EubopenMechanismOfAction.entityID]: {
      originParamName: 'EUBOPEN_MECHANISM_OF_ACTION',
      to: {},
    },
    [EntityNames.EubopenActivity.entityID]: {
      originParamName: 'EUBOPEN_ACTIVITY',
      to: {},
    },
    [EntityNames.EubopenMainTarget.entityID]: {
      originParamName: 'EUBOPEN_MAIN_TARGET',
      to: {},
    },
  },
}

const getPossibleDestinationEntityIDsAndNames = function (originEntityID) {
  const possibleDestinations = []
  for (const [key, toConfig] of Object.entries(
    originsDestinationsConfig.from[originEntityID].to
  )) {
    if (!toConfig.hiddenFromGeneralJoin) {
      possibleDestinations.push({
        entityID: key,
        pluralEntityName: EntityNames[key].pluralEntityName,
      })
    }
  }

  return possibleDestinations
}

const getOriginParamName = function (originEntityID) {
  return originsDestinationsConfig.from[originEntityID].originParamName
}

const getDestinationParamName = function (originEntityID, destinationEntityID) {
  return originsDestinationsConfig.from[originEntityID].to[destinationEntityID]
    .destinationParamName
}

const JoinModes = {
  selection: 'selection',
  allItems: 'all_items',
}

const MAX_ITEMS_TO_JOIN = 1024

export default {
  getPossibleDestinationEntityIDsAndNames,
  getOriginParamName,
  getDestinationParamName,
  JoinModes,
  MAX_ITEMS_TO_JOIN,
}
