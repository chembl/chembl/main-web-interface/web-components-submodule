const filtersViewsNames = {
  histogram: 'histogram',
  range: 'range',
  findTerm: 'find_term',
}

export default filtersViewsNames
