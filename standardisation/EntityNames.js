const entityNames = {
  DrugWarning: {
    pluralEntityName: 'Drug Warnings',
    singularEntityName: 'Drug Warning',
    entityID: 'DrugWarning',
  },
  Compound: {
    pluralEntityName: 'Compounds',
    singularEntityName: 'Compound',
    entityID: 'Compound',
  },
  CellLine: {
    pluralEntityName: 'Cell Lines',
    singularEntityName: 'Cell Line',
    entityID: 'CellLine',
  },
  Document: {
    pluralEntityName: 'Documents',
    singularEntityName: 'Document',
    entityID: 'Document',
  },
  Tissue: {
    pluralEntityName: 'Tissues',
    singularEntityName: 'Tissue',
    entityID: 'Tissue',
  },
  Drug: {
    pluralEntityName: 'Drugs',
    singularEntityName: 'Drug',
    entityID: 'Drug',
  },
  DrugMechanism: {
    pluralEntityName: 'Drug Mechanisms',
    singularEntityName: 'Drug Mechanism',
    entityID: 'DrugMechanism',
  },
  DrugIndication: {
    pluralEntityName: 'Drug Indications',
    singularEntityName: 'Drug Indication',
    entityID: 'DrugIndication',
  },
  Target: {
    pluralEntityName: 'Targets',
    singularEntityName: 'Target',
    entityID: 'Target',
  },
  Activity: {
    pluralEntityName: 'Activities',
    singularEntityName: 'Activity',
    entityID: 'Activity',
  },
  Assay: {
    pluralEntityName: 'Assays',
    singularEntityName: 'Assay',
    entityID: 'Assay',
  },
  EubopenCompound: {
    pluralEntityName: 'Compounds',
    entityID: 'EubopenCompound',
    singularEntityName: 'Compound',
  },
  EubopenTarget: {
    pluralEntityName: 'Targets / Cell Lines',
    entityID: 'EubopenTarget',
    singularEntityName: 'Target / Cell Line',
  },
  EubopenMechanismOfAction: {
    pluralEntityName: 'Mechanisms of Action',
    entityID: 'EubopenMechanismOfAction',
    singularEntityName: 'Mechanisms of Action',
  },
  EubopenActivity: {
    pluralEntityName: 'Activities',
    entityID: 'EubopenActivity',
    singularEntityName: 'Activity',
  },
  EubopenAssay: {
    pluralEntityName: 'Assays',
    entityID: 'EubopenAssay',
    singularEntityName: 'Assays',
  },
  EubopenMainTarget: {
    pluralEntityName: 'Main Targets',
    entityID: 'EubopenMainTarget',
    singularEntityName: 'Main Targets',
  },
}

export default entityNames
