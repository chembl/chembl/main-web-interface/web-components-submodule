import IndexNames from '~/web-components-submodule/standardisation/IndexNames.js'
import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'
import LinksToEntities from '~/web-components-submodule/standardisation/LinksToEntities.js'
import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'
import ObjectPropertyAccess from '~/web-components-submodule/utils/ObjectPropertyAccess.js'

const methods = {
  getMetadataForEntity(entityID, itemID, docSource) {
    const indexName = IndexNames.getIndexNameFromEntityID(entityID)
    return ESProxyService.getESDocument(indexName, itemID, docSource)
  },
  getSynonymsString(rawSynonyms) {
    if (rawSynonyms == null) {
      return ''
    }
    let synonyms = rawSynonyms.map((rawSyn) => rawSyn.synonyms)
    synonyms = [...new Set(synonyms)]
    return synonyms.join(', ')
  },
  getImgURL(itemID, compoundData, imgBaseURL) {
    const placeHolderImg = ObjectPropertyAccess.getPropertyPalue(
      compoundData,
      '_metadata.compound_generated.image_file',
      null,
      true
    )
    return placeHolderImg == null ? `${imgBaseURL}/${itemID}.svg` : null
  },
  getMolecularFormula(compoundData) {
    return ObjectPropertyAccess.getPropertyPalue(
      compoundData,
      'molecule_properties.full_molformula',
      null,
      true
    )
  },
  getMolecularWeight(compoundData) {
    return ObjectPropertyAccess.getPropertyPalue(
      compoundData,
      'molecule_properties.full_mwt',
      null,
      true
    )
  },
  getMonoisotopicMolecularWeight(compoundData) {
    return ObjectPropertyAccess.getPropertyPalue(
      compoundData,
      'molecule_properties.mw_monoisotopic',
      null,
      true
    )
  },
  getSmiles(compoundData) {
    return ObjectPropertyAccess.getPropertyPalue(
      compoundData,
      'molecule_structures.canonical_smiles',
      null,
      true
    )
  },

  async getBioschemasMetadataForCompound(chemblID) {
    const entityID = EntityNames.Compound.entityID
    const itemURL = LinksToEntities[entityID].getLinkToReportCard(chemblID)

    const indexName = IndexNames.getIndexNameFromEntityID(entityID)
    const docSource = [
      'pref_name',
      'molecule_synonyms',
      '_metadata.compound_generated.image_file',
      'molecule_properties.full_molformula',
      'molecule_properties.full_mwt',
      'molecule_properties.mw_monoisotopic',
      'molecule_structures.canonical_smiles',
    ]

    const docResponse = await ESProxyService.getESDocument(
      indexName,
      chemblID,
      docSource
    )

    const docData = docResponse.data._source
    const imgBaseURL = 'https://www.ebi.ac.uk/chembl/api/data/image'
    const metadata = {
      '@context': 'http://schema.org',
      '@type': 'MolecularEntity',
      identifier: chemblID,
      url: itemURL,
      name: docData.pref_name,
      alternateName: methods.getSynonymsString(docData.molecule_synonyms),
      image: methods.getImgURL(chemblID, docData, imgBaseURL),
      molecularFormula: methods.getMolecularFormula(docData),
      molecularWeight: methods.getMolecularWeight(docData),
      smiles: methods.getSmiles(docData),
      monoisotopicMolecularWeight: methods.getMonoisotopicMolecularWeight(
        docData
      ),
    }

    return metadata
  },
}

export default methods
