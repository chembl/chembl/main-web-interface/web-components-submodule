import axios from 'axios'

const djApiClient = axios.create({
  baseURL: process.env.delayedJobsBaseUrl,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
  },
})

export { djApiClient }
