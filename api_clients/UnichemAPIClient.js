import axios from 'axios'

const unichemApiClient = axios.create({
  baseURL: process.env.unichemAPIBaseUrl,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
})

export { unichemApiClient }
