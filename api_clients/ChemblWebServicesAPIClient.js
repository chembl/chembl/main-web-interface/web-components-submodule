import axios from 'axios'

const chemblWebServicesApiClient = axios.create({
  baseURL: process.env.chemblWSBaseUrl,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
  },
})

export { chemblWebServicesApiClient }
