import axios from 'axios'

const interfaceDataClient = axios.create({
  baseURL: 'interface_data',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
  },
})

export { interfaceDataClient }
