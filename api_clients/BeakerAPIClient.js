import axios from 'axios'

const beakerApiClient = axios.create({
  baseURL: process.env.beakerBaseURL,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
  },
})

export { beakerApiClient }
