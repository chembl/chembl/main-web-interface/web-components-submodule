export default {
  trackDatasetTermFiltered(context, entityID, filterKey, term) {
    if (context.$gtag == null) {
      return
    }
    context.$gtag('event', 'filter_applied', {
      event_category: 'toggle_term',
      event_label: `${entityID}-${filterKey}`,
      value: term,
    })
  },
  trackDatasetSpecificTermFiltered(context, entityID, filterKey, terms) {
    if (context.$gtag == null) {
      return
    }
    context.$gtag('event', 'filter_applied', {
      event_category: 'specific_terms',
      event_label: `${entityID}-${filterKey}`,
      value: terms.join(','),
    })
  },
  trackDatasetRangeFiltered(context, entityID, filterKey, range) {
    if (context.$gtag == null) {
      return
    }
    context.$gtag('event', 'filter_applied', {
      event_category: 'specific_terms',
      event_label: `${entityID}-${filterKey}`,
      value: range,
    })
  },
  trackDatasetCustomFilterApplied(context, entityID, customFilter) {
    if (context.$gtag == null) {
      return
    }
    context.$gtag('event', 'filter_applied', {
      event_category: 'custom_filter',
      event_label: entityID,
      value: customFilter,
    })
  },
  trackDatasetSorted(context, entityID, sortProperties) {
    if (context.$gtag == null) {
      return
    }
    context.$gtag('event', 'dataset_sorted', {
      event_category: 'dataset_sorted',
      event_label: entityID,
      value: sortProperties.join(','),
    })
  },
  trackSearchFromAppBar(context, searchTerm) {
    if (context.$gtag == null) {
      return
    }
    context.$gtag('event', 'search_from_app_bar', {
      event_category: 'free_text_search',
      value: searchTerm,
    })
  },
  trackGoDirectlyToItemFromSuggestion(context, itemId) {
    if (context.$gtag == null) {
      return
    }
    context.$gtag('event', 'go_to_item_from_suggestion', {
      event_category: 'autocomplete',
      value: itemId,
    })
  },
}
