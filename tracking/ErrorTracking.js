export default {
  trackError(error, context) {
    if (context.$gtag == null) {
      return
    }
    context.$gtag('event', 'exception', {
      description: error,
      fatal: false, // set to true if the error is fatal
    })
  },
}
