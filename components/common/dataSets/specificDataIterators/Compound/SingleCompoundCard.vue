<template>
  <v-card outlined>
    <SelectionCheckbox
      class="selection-checkbox"
      :dataset-state="datasetState"
      :store-module-name="storeModuleName"
      :item="item"
    />
    <template v-if="reveal">
      <v-card-text>
        <component
          :is="fullDetailsComponent"
          v-if="reveal"
          :slot-description="slotDescription"
          :item="item"
        />
      </v-card-text>
      <v-divider />
      <v-card-actions class="pt-0">
        <v-btn text color="teal accent-4" @click="reveal = false">
          Close
        </v-btn>
      </v-card-actions>
    </template>

    <template v-else>
      <!-- This is just for clarity, the component is the same but the parameters are different-->
      <MoleculeImage
        v-if="enableSimilarityMaps"
        :molecule-chembl-id="item.id"
        enable-structure-search
        compact-structure-search
        enable-similarity-maps
        :similarity-maps-reference-structure="similarityMapsReferenceStructure"
        :show-similarity-maps="showSimilarityMaps"
        :show-options="false"
      />
      <MoleculeImage
        v-else-if="enableSubstructureHighlight"
        :molecule-chembl-id="item.id"
        enable-structure-search
        compact-structure-search
        enable-substructure-highlight
        :substructure-highlight-reference-structure="
          substructureHighlightReferenceStructure
        "
        :show-substructure-highlight="showSubstructureHighlight"
        :show-options="false"
      />
      <MoleculeImage
        v-else
        :molecule-chembl-id="item.id"
        enable-structure-search
        compact-structure-search
        :show-options="false"
      />

      <v-card-title>
        <a :href="linkToReportCard" class="text-truncate"> {{ itemLabel }}</a>
      </v-card-title>

      <v-card-text>
        <component
          :is="frontDetailsComponent"
          :slot-description="slotDescription"
          :item="item"
        />
      </v-card-text>
      <v-divider />
      <v-card-actions>
        <v-btn small text color="primary" @click="reveal = true">
          Details
        </v-btn>
      </v-card-actions>
    </template>
  </v-card>
</template>

<script>
import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'
import LinksToEntities from '~/web-components-submodule/standardisation/LinksToEntities.js'
import DefaultValues from '~/web-components-submodule/standardisation/DefaultValues.js'
import MoleculeImage from '~/web-components-submodule/components/common/ReportCards/Shared/MoleculeImage.vue'
import SelectionCheckbox from '~/web-components-submodule/components/common/dataSets/specificDataIterators/Compound/SelectionCheckbox.vue'
import CompoundFullDetails from '~/web-components-submodule/components/common/dataSets/specificDataIterators/Compound/CardDetails/CompoundFullDetails.vue'
import CompoundFrontDetails from '~/web-components-submodule/components/common/dataSets/specificDataIterators/Compound/CardDetails/CompoundFrontDetails.vue'
import DrugFrontDetails from '~/web-components-submodule/components/common/dataSets/specificDataIterators/Compound/CardDetails/DrugFrontDetails.vue'
import DrugFullDetails from '~/web-components-submodule/components/common/dataSets/specificDataIterators/Compound/CardDetails/DrugFullDetails.vue'

export default {
  components: {
    MoleculeImage,
    SelectionCheckbox,
  },
  props: {
    slotDescription: {
      type: Object,
      default: () => {},
    },
    item: {
      type: Object,
      default: () => {},
    },
    datasetState: {
      type: Object,
      default: () => {},
    },
    storeModuleName: {
      type: String,
      default: () => '',
    },
  },
  data: () => ({
    reveal: false,
  }),
  computed: {
    itemLabel() {
      return this.item.data.pref_name == null ||
        this.item.data.pref_name === '' ||
        this.item.data.pref_name === DefaultValues.DefaultNullTextValue
        ? this.item.id
        : this.item.data.pref_name
    },
    linkToReportCard() {
      const entityID = EntityNames.Compound.entityID
      const linkGenerator = LinksToEntities[entityID]
      const itemID = this.item.id
      return linkGenerator.getLinkToReportCard(itemID)
    },
    frontDetailsComponent() {
      return this.entityID === EntityNames.Compound.entityID
        ? CompoundFrontDetails
        : DrugFrontDetails
    },
    fullDetailsComponent() {
      return this.entityID === EntityNames.Compound.entityID
        ? CompoundFullDetails
        : DrugFullDetails
    },
    enableSimilarityMaps() {
      return this.datasetState.enableSimilarityMaps
    },
    enableSubstructureHighlight() {
      return this.datasetState.enableSubstructureHighlight
    },
    showSimilarityMaps() {
      return this.datasetState.similarityMaps.show
    },
    showSubstructureHighlight() {
      return this.datasetState.substructureHighlight.show
    },
    similarityMapsReferenceStructure() {
      return this.datasetState.similarityMaps.referenceStructure
    },
    substructureHighlightReferenceStructure() {
      return this.datasetState.substructureHighlight.referenceStructure
    },
    entityID() {
      return this.datasetState.entityID
    },
  },
}
</script>

<style scoped lang="scss">
.prop-label {
  margin-right: 5px;
}
.selection-checkbox {
  position: absolute;
  top: -17px;
  right: -5px;
  z-index: 1;
}
</style>
