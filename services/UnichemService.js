import { unichemApiClient } from '~/web-components-submodule/api_clients/UnichemAPIClient.js'

export default {
  getUnichemConnectivitySearchResults(inchiKey) {
    const payload = {
      type: 'inchikey',
      compound: inchiKey,
      searchComponents: true,
      componentConnLimit: 300,
    }
    return unichemApiClient.post('/connectivity', payload)
  },
}
