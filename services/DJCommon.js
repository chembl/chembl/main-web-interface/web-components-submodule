import { djApiClient } from '~/web-components-submodule/api_clients/DelajedJobsAPIClient.js'
import ErrorTracking from '~/web-components-submodule/tracking/ErrorTracking.js'

const methods = {
  getJobStatusUrl(jobID) {
    return `${djApiClient.defaults.baseURL}/status/${jobID}`
  },
  checkJobUntilDoneGenerator(storeParams, jobID, jobStatusCallback) {
    return function () {
      methods.checkJobUntilDone(storeParams, jobID, jobStatusCallback)
    }
  },
  checkJobUntilDone(storeParams, jobID, jobStatusCallback, jobErrorCallback) {
    methods
      .getJobStatus(jobID)
      .then((response) => {
        const jobData = response.data
        jobStatusCallback(storeParams, jobData)
        const keepChecking = !['ERROR', 'FINISHED'].includes(jobData.status)
        if (keepChecking) {
          setTimeout(
            methods.checkJobUntilDoneGenerator(
              storeParams,
              jobID,
              jobStatusCallback
            ),
            process.env.djJobStatusCheckIntervalMillis
          )
        }
      })
      .catch((error) => {
        ErrorTracking.trackError(error, this)
        if (jobErrorCallback != null) {
          jobErrorCallback(storeParams, error)
          return
        }
        console.error('Error checking job status', error)
      })
  },
  getJobStatus(jobID) {
    return djApiClient.get(`/status/${jobID}`)
  },
}

export default methods
