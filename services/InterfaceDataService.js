import { interfaceDataClient } from '~/web-components-submodule/api_clients/InterfaceDataAPIClient.js'

export default {
  getBlogEntries(pageToken) {
    return interfaceDataClient.get('/blog', {
      params: { pageToken },
    })
  },
  getBlastParams() {
    return interfaceDataClient.get('/blast_params')
  },
}
