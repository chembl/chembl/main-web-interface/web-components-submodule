import { djApiClient } from '~/web-components-submodule/api_clients/DelajedJobsAPIClient.js'

export default {
  submitDownloadJob(downloadParams) {
    const formData = new FormData()
    formData.append('index_name', downloadParams.indexName)
    formData.append('query', JSON.stringify(downloadParams.query))
    formData.append('format', downloadParams.format)
    formData.append(
      'download_columns_group',
      downloadParams.download_columns_group
    )

    if (
      downloadParams.context_obj != null &&
      typeof downloadParams.context_obj === 'object' &&
      Object.keys(downloadParams.context_obj).length > 0
    ) {
      formData.append('context_obj', JSON.stringify(downloadParams.context_obj))
    }

    formData.append('dl__ignore_cache', downloadParams.dl__ignore_cache)

    return djApiClient.post('/submit/download_job', formData)
  },
}
