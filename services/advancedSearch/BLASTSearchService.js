import { djApiClient } from '~/web-components-submodule/api_clients/DelajedJobsAPIClient.js'

export default {
  submitBLASTSearchJob(sequence) {
    const formData = new FormData()
    formData.append('sequence', sequence)
    // BLAST Search asks to ignore cache to prevent synchronization issues with the official EBI BLAST search
    formData.append('dl__ignore_cache', true)
    return djApiClient.post('/submit/biological_sequence_search_job', formData)
  },
  submitSubsustructureSearchJob(term) {
    const formData = new FormData()
    formData.append('search_type', 'SUBSTRUCTURE')
    formData.append('search_term', term)
    formData.append('dl__ignore_cache', process.env.delayedJobsIgnoreCache)
    return djApiClient.post('submit/structure_search_job', formData)
  },
}
