import { esProxyApiClient } from '~/web-components-submodule/api_clients/ESProxyAPIClient.js'
import ServerSideUtils from '~/web-components-submodule/utils/ServerSideUtils.js'
import URLUtils from '~/web-components-submodule/utils/URLUtils.js'

const methods = {
  parseHetmapDescription(heatmapDescription) {
    // if the heatmap description is already a string, assume that it's a heatmap ID
    if (typeof heatmapDescription === 'string') {
      return `HEATMAP_ID:${heatmapDescription}`
    }

    const b64Description = Buffer.from(JSON.stringify(heatmapDescription))
      .toString('base64')
      .replace(/\+/g, '-')
      .replace(/\//g, '_')

    return b64Description
  },
}

export default {
  getPopertyListConfiguration(indexName, properties) {
    return esProxyApiClient.get(
      `/properties_configuration/list/${indexName}/${encodeURIComponent(
        properties.join(',')
      )}`
    )
  },
  getPopertyGroupConfiguration(indexName, groupName) {
    return esProxyApiClient.get(
      `/properties_configuration/group/${indexName}/${groupName}`
    )
  },
  getFacetsGroupConfiguration(indexName, groupName) {
    return esProxyApiClient.get(
      `/properties_configuration/facets/${indexName}/${groupName}`
    )
  },
  getESData(indexName, query, contextObj, contextualSort) {
    const esProxyParams = {
      index_name: indexName,
      es_query: query,
      context_obj: contextObj,
      contextual_sort_data: contextualSort,
    }

    const b64Params = btoa(JSON.stringify(esProxyParams))
      .replace(/\+/g, '-')
      .replace(/\//g, '_')
    const fullGetURL = `${esProxyApiClient.defaults.baseURL}/es_data/get_es_data/${b64Params}`
    const tooLong = fullGetURL.length > process.env.esDataMaxURLLength

    if (tooLong) {
      // That's what she said
      const formData = new FormData()
      formData.append('index_name', indexName)
      formData.append('es_query', JSON.stringify(query))
      if (contextObj != null) {
        formData.append('context_obj', JSON.stringify(contextObj))
      }
      if (contextualSort != null) {
        formData.append('contextual_sort_data', JSON.stringify(contextualSort))
      }
      return esProxyApiClient.post('/es_data/get_es_data', formData)
    }

    const getURL = `/es_data/get_es_data/${b64Params}`
    return esProxyApiClient.get(getURL)
  },
  getESDocument(indexName, docID, docSource, customIDProperty) {
    let docURL = `/es_data/get_es_document/${indexName}/${docID}`
    const params = {}
    if (docSource != null) {
      params.source = docSource.join(',')
    }
    if (customIDProperty != null) {
      params.custom_id_property = customIDProperty
    }

    docURL = ServerSideUtils.addQueryParamsToURL(docURL, params)
    return esProxyApiClient.get(docURL)
  },
  getLookupData(chemblId) {
    const lookupURL = `/es_data/get_es_document/chembl_chembl_id_lookup/${chemblId}`
    return esProxyApiClient.get(lookupURL)
  },
  getHashForESJoin(joinParams) {
    const formData = new FormData()
    formData.append(
      'destination_entity_browser_state_template',
      joinParams.destinationEntityBrowserStateTemplate
    )
    formData.append('entity_from', joinParams.entityFrom)
    formData.append('entity_to', joinParams.entityTo)
    formData.append('es_query', JSON.stringify(joinParams.query))
    formData.append(
      'selection_description',
      JSON.stringify(joinParams.selectionDescription)
    )
    if (
      joinParams.context_obj != null &&
      typeof joinParams.context_obj === 'object' &&
      Object.keys(joinParams.context_obj).length > 0
    ) {
      formData.append('context_obj', JSON.stringify(joinParams.context_obj))
    }

    return esProxyApiClient.post(
      '/entities_join/get_link_to_related_items',
      formData
    )
  },
  getQueryForESJoin(joinParams) {
    const formData = new FormData()
    formData.append('entity_from', joinParams.entityFrom)
    formData.append('entity_to', joinParams.entityTo)
    formData.append('es_query', JSON.stringify(joinParams.query))
    formData.append(
      'selection_description',
      JSON.stringify(joinParams.selectionDescription)
    )
    if (
      joinParams.context_obj != null &&
      typeof joinParams.context_obj === 'object' &&
      Object.keys(joinParams.context_obj).length > 0
    ) {
      formData.append('context_obj', JSON.stringify(joinParams.context_obj))
    }

    return esProxyApiClient.post(
      '/entities_join/get_query_to_related_items',
      formData
    )
  },
  getHashForLongURL(longURLtoShorten) {
    const parsedURLForRequest = longURLtoShorten.replace(
      process.env.hastagUrlsBase,
      ''
    )
    const formData = new FormData()
    formData.append('long_url', parsedURLForRequest)

    return esProxyApiClient.post('/url_shortening/shorten_url', formData)
  },
  getLongURLForHash(hash) {
    return esProxyApiClient.get(`/url_shortening/expand_url/${hash}`)
  },
  getChemblSearchSuggestions(term) {
    const sanitisedText = URLUtils.sanitiseTextForEscaping(term)
    return esProxyApiClient.get(
      `/search/autocomplete/${encodeURIComponent(sanitisedText)}`
    )
  },
  getChemblSearchParams(term) {
    const sanitisedText = URLUtils.sanitiseTextForEscaping(term)
    return esProxyApiClient.get(
      `/search/free_text/${encodeURIComponent(sanitisedText)}`
    )
  },
  getQuickSearchPriorities(entityID) {
    return esProxyApiClient.get(`/search/properties_priorities/${entityID}`)
  },
  getEubopenSearchSuggestions(term) {
    const sanitisedText = URLUtils.sanitiseTextForEscaping(term)
    return esProxyApiClient.get(
      `/eubopen/search/autocomplete/${encodeURIComponent(sanitisedText)}`
    )
  },
  getEubopenSearchParams(term) {
    const sanitisedText = URLUtils.sanitiseTextForEscaping(term)
    return esProxyApiClient.get(
      `/eubopen/search/free_text/${encodeURIComponent(sanitisedText)}`
    )
  },
  getGenericData(dataPath) {
    return esProxyApiClient.get(dataPath)
  },
  Heatmap: {
    getHeatmapID(heatmapDescription) {
      const b64Description = btoa(JSON.stringify(heatmapDescription))
        .replace(/\+/g, '-')
        .replace(/\//g, '_')

      const fullGetURL = `${
        esProxyApiClient.defaults.baseURL
      }/visualisations/heatmap/get_id/${encodeURIComponent(b64Description)}`
      const tooLong = fullGetURL.length > process.env.esDataMaxURLLength
      if (tooLong) {
        const formData = new FormData()
        formData.append('descriptor', JSON.stringify(b64Description))
        return esProxyApiClient.post('/visualisations/heatmap/get_id', formData)
      }

      return esProxyApiClient.get(
        `/visualisations/heatmap/get_id/${encodeURIComponent(b64Description)}`
      )
    },
    getHeatmapDescription(heatmapID) {
      return esProxyApiClient.get(
        `/visualisations/heatmap/get_descriptor/${heatmapID}`
      )
    },
    getContextualData(heatmapDescription) {
      const b64Description = methods.parseHetmapDescription(heatmapDescription)

      return esProxyApiClient.get(
        `/visualisations/heatmap/${encodeURIComponent(
          b64Description
        )}/contextual_data`
      )
    },
    getAxisSummary(heatmapDescription, axis) {
      const b64Description = methods.parseHetmapDescription(heatmapDescription)

      return esProxyApiClient.get(
        `/visualisations/heatmap/${encodeURIComponent(
          b64Description
        )}/${axis}/summary`
      )
    },
    getAxisItems(heatmapDescription, axis, part, from, size) {
      const b64Description = methods.parseHetmapDescription(heatmapDescription)

      return esProxyApiClient.get(
        `/visualisations/heatmap/${encodeURIComponent(
          b64Description
        )}/${axis}/${part}/${from}/${size}`
      )
    },
    getCells(heatmapDescription, yStart, yEnd, xStart, xEnd) {
      const b64Description = methods.parseHetmapDescription(heatmapDescription)

      return esProxyApiClient.get(
        `/visualisations/heatmap/${encodeURIComponent(
          b64Description
        )}/cells/${xStart}/${xEnd - xStart}/${yStart}/${yEnd - yStart}`
      )
    },
    getAutocompleteSuggestions(heatmapDescription, axis, term) {
      const b64Description = methods.parseHetmapDescription(heatmapDescription)

      return esProxyApiClient.get(
        `/visualisations/heatmap/${encodeURIComponent(
          b64Description
        )}/autocomplete_in_axis/${axis}/${term}`
      )
    },
  },
}
