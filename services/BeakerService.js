import { beakerApiClient } from '~/web-components-submodule/api_clients/BeakerAPIClient.js'

export default {
  ctab2smiles(ctab) {
    const formData = new FormData()
    const molFileBlob = new Blob([ctab], { type: 'chemical/x-mdl-molfile' })
    formData.append('file', molFileBlob, 'molecule.mol')
    formData.append('sanitize', 0)

    return beakerApiClient.post('ctab2smiles', formData)
  },
  smiles2ctab(smiles) {
    const formData = new FormData()
    const smilesFileBlob = new Blob([smiles], {
      type: 'chemical/x-daylight-smiles',
    })

    formData.append('file', smilesFileBlob, 'molecule.smi')
    formData.append('sanitize', 0)
    return beakerApiClient.post('smiles2ctab', formData)
  },
  ctab2svg(ctab) {
    const formData = new FormData()
    const molFileBlob = new Blob([ctab], { type: 'chemical/x-mdl-molfile' })
    formData.append('file', molFileBlob, 'molecule.mol')
    formData.append('computeCoords', 0)
    formData.append('sanitize', 0)

    return beakerApiClient.post('ctab2svg', formData)
  },
  ctab2smarts(ctab) {
    const formData = new FormData()
    const molFileBlob = new Blob([ctab], { type: 'chemical/x-mdl-molfile' })
    formData.append('file', molFileBlob, 'molecule.mol')
    formData.append('sanitize', 0)

    return beakerApiClient.post('ctab2smarts', formData)
  },
  highlightCtabFragmentSvg(ctab, fragment) {
    const formData = new FormData()
    const molFileBlob = new Blob([ctab], { type: 'chemical/x-mdl-molfile' })
    formData.append('file', molFileBlob, 'aligned.mol')
    formData.append('smarts', fragment)
    formData.append('computeCoords', 0)
    formData.append('force', 'true')
    formData.append('sanitize', 0)

    return beakerApiClient.post('highlightCtabFragmentSvg', formData)
  },
  smiles2inchiKey(smiles) {
    return beakerApiClient.post('smiles2inchiKey', smiles)
  },
  smiles2SimilarityMapSvg(smiles, referenceSmiles) {
    const formData = new FormData()
    formData.append(
      'file',
      new Blob([referenceSmiles + '\n' + smiles], {
        type: 'chemical/x-daylight-smiles',
      }),
      'sim.smi'
    )
    formData.append('format', 'svg')
    formData.append('height', '500')
    formData.append('width', '500')
    formData.append('sanitize', 0)
    return beakerApiClient.post('smiles2SimilarityMapSvg', formData)
  },
}
