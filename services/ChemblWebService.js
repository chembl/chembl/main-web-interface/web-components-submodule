import { chemblWebServicesApiClient } from '~/web-components-submodule/api_clients/ChemblWebServicesAPIClient.js'

export default {
  getStatus() {
    return chemblWebServicesApiClient.get('/status.json')
  },
  getCompoundSDF(chemblID) {
    return chemblWebServicesApiClient.get(`/molecule/${chemblID}.sdf`)
  },
  getSimilarityPage(term, threshold, offset, limit, onlyProperties) {
    return chemblWebServicesApiClient.get(
      `/similarity/${encodeURI(term)}/${threshold}.json`,
      { params: { offset, limit, only: onlyProperties } }
    )
  },
  getStructuralAlertsPage(chemblID, offset, limit) {
    return chemblWebServicesApiClient.get(`/compound_structural_alert.json`, {
      params: {
        molecule_chembl_id: chemblID,
        offset,
        limit,
      },
    })
  },
  getTargetRelationsPage(chemblID, offset, limit) {
    return chemblWebServicesApiClient.get(`/target_relation.json`, {
      params: {
        related_target_chembl_id: chemblID,
        offset,
        limit,
        order_by: 'target_chembl_id',
      },
    })
  },
  getTargetsListPage(chemblIDs, offset, limit, orderBy) {
    return chemblWebServicesApiClient.get(`/target.json`, {
      params: {
        target_chembl_id__in: chemblIDs.join(','),
        offset,
        limit,
        order_by: orderBy,
      },
    })
  },
}
