import { interfaceDataClient } from '~/web-components-submodule/api_clients/InterfaceDataAPIClient.js'
import URLUtils from '~/web-components-submodule/utils/URLUtils.js'

export default {
  getPredictions(smiles) {
    return interfaceDataClient.get(
      `/target_predictions/${encodeURIComponent(
        URLUtils.sanitiseTextForEscaping(smiles)
      )}`
    )
  },
}
