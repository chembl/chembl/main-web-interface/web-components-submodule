import Vue from 'vue'
export default {
  getBaseMutations() {
    return {
      SET_HEATMAP_ID(state, heatmapID) {
        state.heatmapID = heatmapID
      },
      SET_HEATMAP_ID_EXPIRES(state, idExpires) {
        state.idExpires = idExpires
      },
      SET_DESCRIPTION(state, description) {
        state.description = description
      },
      SET_AXIS_ITEMS_COUNT(state, { axisID, totalItems }) {
        state[axisID].totalItems = totalItems
      },
      INCREASE_ZOOM(state) {
        state.zoom.zoomLevel += 1
      },
      DECREASE_ZOOM(state) {
        state.zoom.zoomLevel -= 1
      },
      RESET_ZOOM(state) {
        state.zoom.zoomLevel = state.zoom.initialZoomLevel
      },
      SET_ALL_READY(state, allReady) {
        state.initialLoad = allReady
      },
      APPEND_AXIS_ITEMS(state, { axisID, dataWindowID, newItems }) {
        for (const newItem of newItems) {
          state[axisID][dataWindowID].items.push({
            itemID: newItem._id,
            itemData: newItem._source,
          })
        }
      },
      BUMP_DATA_NEXT_FROM(state, { axisID, dataWindowID }) {
        state[axisID][dataWindowID].nextFrom +=
          state[axisID][dataWindowID].loadSize
      },
      REVERSE_DATA_NEXT_FROM(state, { axisID, dataWindowID }) {
        state[axisID][dataWindowID].nextFrom -=
          state[axisID][dataWindowID].loadSize
      },
      SET_AXIS_ITEMS_LOADING(state, { axisID, dataWindowID, loading }) {
        state[axisID][dataWindowID].loading = loading
      },
      SET_LEFT_SCROLL(state, leftScroll) {
        state.scroll.leftScroll = leftScroll
      },
      SET_TOP_SCROLL(state, topScroll) {
        confirm('SET_TOP_SCROLL', topScroll)
        state.scroll.topScroll = topScroll
      },
      SET_AXIS_VISUAL_WINDOW(state, { axisID, start, end }) {
        state.visualWindow[axisID].start = start
        state.visualWindow[axisID].end = end
      },
      SET_MAP_WINDOW(state, { xStart, xEnd, yStart, yEnd }) {
        state.mapWindow.xAxis.start = xStart
        state.mapWindow.xAxis.end = xEnd
        state.mapWindow.yAxis.start = yStart
        state.mapWindow.yAxis.end = yEnd
        state.mapWindow.sideLength = xEnd - xStart
      },
      MOVE_IN_VISUAL_WINDOW(state, { axisID, step }) {
        state.visualWindow[axisID].start += step
        state.visualWindow[axisID].end += step

        // make sure map window moves in the same way as the visual window
        // unless the final position is negative
        const mapWindowSize =
          state.mapWindow[axisID].end - state.mapWindow[axisID].start
        let mapWindowNewStart = state.mapWindow[axisID].start + step
        mapWindowNewStart = Math.max(0, mapWindowNewStart)
        state.mapWindow[axisID].start = mapWindowNewStart
        state.mapWindow[axisID].end = mapWindowNewStart + mapWindowSize
      },
      MOVE_TO_VISUAL_WINDOW_AXIS_START(state, axisID) {
        const currentSize =
          state.visualWindow[axisID].end - state.visualWindow[axisID].start

        const dataStart = state.allChunksLoaded
          ? 0
          : state.loadingWindow[axisID].start

        const visualStart = Math.max(dataStart - state.limitVisualOffset, 0)

        state.visualWindow[axisID].start = visualStart
        state.visualWindow[axisID].end = visualStart + currentSize

        // make sure map window moves in the same way as the visual window
        state.mapWindow[axisID].start = dataStart
        state.mapWindow[axisID].end = dataStart + state.mapWindow.sideLength
      },
      MOVE_TO_VISUAL_WINDOW_AXIS_END(state, axisID) {
        const currentSize =
          state.visualWindow[axisID].end - state.visualWindow[axisID].start

        const dataEnd = state.allChunksLoaded
          ? state[axisID].totalItems
          : state.loadingWindow[axisID].end

        const visualEnd = dataEnd + state.limitVisualOffset
        const visualStart = visualEnd - currentSize

        state.visualWindow[axisID].start = visualStart
        state.visualWindow[axisID].end = visualEnd

        // make sure map window moves in the same way as the visual window
        state.mapWindow[axisID].start = visualStart
        state.mapWindow[axisID].end = visualStart + state.mapWindow.sideLength
      },
      MOVE_TO_POINT_IN_VISUAL_WINDOW(state, { axisID, point }) {
        const currentSize =
          state.visualWindow[axisID].end - state.visualWindow[axisID].start

        state.visualWindow[axisID].start = point
        state.visualWindow[axisID].end = point + currentSize

        // make sure map window moves in the same way as the visual window
        state.mapWindow[axisID].start = point
        state.mapWindow[axisID].end = point + state.mapWindow.sideLength
      },
      MOVE_IN_VISUAL_WINDOW_FROM_MINIMAP(
        state,
        { xNewVisualWindowStart, yNewVisualWindowStart }
      ) {
        const currentXSize =
          state.visualWindow.xAxis.end - state.visualWindow.xAxis.start

        state.visualWindow.xAxis.start = xNewVisualWindowStart
        state.visualWindow.xAxis.end = xNewVisualWindowStart + currentXSize

        const currentYSize =
          state.visualWindow.yAxis.end - state.visualWindow.yAxis.start

        state.visualWindow.yAxis.start = yNewVisualWindowStart
        state.visualWindow.yAxis.end = yNewVisualWindowStart + currentYSize
      },
      ADD_NEW_AXIS_CHUNK(state, { axisID, windowID, start }) {
        const chunks = state[axisID][windowID].chunks

        Vue.set(chunks, start, {
          items: [],
          ready: false,
          pending: true,
          loading: false,
          error: false,
        })
      },
      SET_AXIS_CHUNK_LOADING(state, { axisID, windowID, start }) {
        state[axisID][windowID].chunks[start].ready = false
        state[axisID][windowID].chunks[start].pending = false
        state[axisID][windowID].chunks[start].loading = true
        state[axisID][windowID].chunks[start].error = false
      },
      SET_AXIS_CHUNK_ERROR(state, { axisID, windowID, start }) {
        state[axisID][windowID].chunks[start].ready = false
        state[axisID][windowID].chunks[start].pending = false
        state[axisID][windowID].chunks[start].loading = false
        state[axisID][windowID].chunks[start].error = true
      },
      SET_AXIS_CHUNK_LOADED(state, { axisID, windowID, start, items }) {
        state[axisID][windowID].chunks[start].pending = false
        state[axisID][windowID].chunks[start].loading = false
        state[axisID][windowID].chunks[start].error = false
        state[axisID][windowID].chunks[start].items = items
        state[axisID][windowID].chunks[start].ready = true // ready is set last, because the reactity system will trigger a re-render and can cause errors if set before the other properties
      },
      ADD_NEW_DATA_ROW(state, { quadrantYStart }) {
        Vue.set(state.cells.dataQuadrants, quadrantYStart, {})
      },
      ADD_NEW_DATA_QUADRANT(state, { quadrantYStart, quadrantXStart }) {
        const dataQuadrants = state.cells.dataQuadrants
        if (dataQuadrants[quadrantYStart] == null) {
          Vue.set(state.cells.dataQuadrants, quadrantYStart, {})
        }

        const dataQuadrant = dataQuadrants[quadrantYStart][quadrantXStart]
        if (dataQuadrant == null) {
          Vue.set(state.cells.dataQuadrants[quadrantYStart], quadrantXStart, {
            items: [],
            ready: false,
            pending: true,
            loading: false,
            error: false,
            coherent: undefined,
            numCatches: 0, // number of times this quadrant has caught an loading error. If it catches too many, it will be marked as error
          })
        }
      },
      SET_LOADING_WINDOW(state, { xStart, xEnd, yStart, yEnd }) {
        state.loadingWindow.xAxis.start = xStart
        state.loadingWindow.xAxis.end = xEnd
        state.loadingWindow.yAxis.start = yStart
        state.loadingWindow.yAxis.end = yEnd
      },
      SET_NUM_LOADING_DATA_CHUNKS(state, numLoadingChunks) {
        state.numLoadingChunks = numLoadingChunks
      },
      SET_ALL_CHUNKS_LOADED(state, allChunksLoaded) {
        state.allChunksLoaded = allChunksLoaded
      },
      SET_DATA_QUADRANT_LOADING(state, { yStart, xStart }) {
        state.cells.dataQuadrants[yStart][xStart].ready = false
        state.cells.dataQuadrants[yStart][xStart].pending = false
        state.cells.dataQuadrants[yStart][xStart].loading = true
        state.cells.dataQuadrants[yStart][xStart].error = false
      },
      SET_DATA_QUADRANT_ERROR(state, { yStart, xStart }) {
        state.cells.dataQuadrants[yStart][xStart].ready = false
        state.cells.dataQuadrants[yStart][xStart].pending = false
        state.cells.dataQuadrants[yStart][xStart].loading = false
        state.cells.dataQuadrants[yStart][xStart].error = true
      },
      SET_DATA_QUADRANT_LOADED(state, { yStart, xStart, cells }) {
        state.cells.dataQuadrants[yStart][xStart].ready = true
        state.cells.dataQuadrants[yStart][xStart].pending = false
        state.cells.dataQuadrants[yStart][xStart].loading = false
        state.cells.dataQuadrants[yStart][xStart].error = false
        state.cells.dataQuadrants[yStart][xStart].items = cells
      },
      SET_NUM_UNCOHERENT_DATA_CHUNKS(state, numUncoherentChunks) {
        state.numUncoherentChunks = numUncoherentChunks
      },
      SET_ALL_DATA_CHUNKS_COHERENT(state, allChunksCoherent) {
        state.allChunksCoherent = allChunksCoherent
      },
      SET_DATA_QUADRANT_COHERENT(state, { yStart, xStart, coherent }) {
        state.cells.dataQuadrants[yStart][xStart].coherent = coherent
      },
      SET_CURRENT_FOOTER_PROPERTY(state, { axisID, footerPropertyIndex }) {
        state[axisID].footerCurrentPropertyIndex = footerPropertyIndex
      },
      SET_CELLS_CONTEXTUAL_DATA(state, contextualData) {
        state.cells.contextualData = contextualData
      },
      SET_CONTEXTUAL_DATA_READY(state, contextualDataReady) {
        state.contextualDataReady = contextualDataReady
      },
      SET_CURRENT_HOVER(
        state,
        { x, y, cellData, xHeaderData, yHeaderData, hoverOverFooter }
      ) {
        state.currentHover.x = x
        state.currentHover.y = y
        state.currentHover.cellData = cellData
        state.currentHover.xHeaderData = xHeaderData
        state.currentHover.yHeaderData = yHeaderData
        state.currentHover.hoverOverFooter = hoverOverFooter
      },
      UNSET_CURRENT_HOVER(state) {
        state.currentHover.x = undefined
        state.currentHover.y = undefined
        state.currentHover.cellData = undefined
        state.currentHover.xHeaderData = undefined
        state.currentHover.yHeaderData = undefined
        state.currentHover.hoverOverFooter = undefined
      },
      FREEZE_HOVER(state) {
        state.freezeHover = true
      },
      UN_FREEZE_HOVER(state) {
        state.freezeHover = false
      },
      SET_CELL_PROPERTY(state, cellPropertyIndex) {
        state.cells.currentPropertyIndex = cellPropertyIndex
      },
      SHOW_CELL_DETAILS(state) {
        state.showCellDetails = true
      },
      HIDE_CELL_DETAILS(state) {
        state.showCellDetails = false
      },
      SET_CLICKED_CELL(state, cellData) {
        state.clickedCell = cellData
      },
    }
  },
}
