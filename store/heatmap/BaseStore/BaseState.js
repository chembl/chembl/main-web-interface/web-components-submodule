export default {
  getBaseState() {
    return {
      description: {},
      heatmapID: undefined,
      idExpires: undefined,
      initialLoad: false,
      contextualDataReady: false,
      numLoadingChunks: 0,
      allChunksLoaded: true,
      numUncoherentChunks: 0,
      allChunksCoherent: true,
      baseSideLength: 10,
      dataChunkSize: 20,
      limitVisualOffset: 5, // this is to make it easier to see the last items
      currentHover: {
        x: undefined,
        y: undefined,
        cellData: undefined,
        xHeaderData: undefined,
        yHeaderData: undefined,
        hoverOverFooter: false,
      },
      freezeHover: false,
      showCellDetails: false,
      clickedCell: undefined,
      // window used to show the main heatmap
      visualWindow: {
        fastStepSize: 20,
        slowStepSize: 10,
        xAxis: {
          start: 0,
          end: 0,
        },
        yAxis: {
          start: 0,
          end: 0,
        },
      },
      // window used to show the mini map
      mapWindow: {
        pixelSideLength: 10,
        sideLength: 0,
        xAxis: {
          start: 0,
          end: 0,
        },
        yAxis: {
          start: 0,
          end: 0,
        },
      },
      loadingWindow: {
        xAxis: {
          start: 0,
          end: 0,
        },
        yAxis: {
          start: 0,
          end: 0,
        },
      },
      scroll: {
        leftScroll: 0,
        topScroll: 0,
      },
      zoom: {
        initialZoomLevel: 3,
        zoomLevel: 3,
        maxZoom: 5,
        minZoom: 2,
      },
      xAxis: {
        totalItems: undefined,
        footerCurrentPropertyIndex: 0,
        headersDataWindow: {
          chunks: {
            // 0: {
            //   items: [],
            //   ready: false,
            //   pending: false,
            //   loading: true,
            //   error: false,
            // },
          },
        },
        footersDataWindow: {
          chunks: {},
        },
      },
      yAxis: {
        totalItems: undefined,
        footerCurrentPropertyIndex: 0,
        headersDataWindow: {
          chunks: {
            // 0: {
            //   items: [],
            //   pending: false,
            //   loading: true,
            //   error: false,
            // },
          },
        },
        footersDataWindow: {
          chunks: {},
        },
      },
      cells: {
        quadrantSideLength: 20,
        currentPropertyIndex: 0,
        contextualData: {
          ranges: {},
        },
        dataQuadrants: {
          // y
          // 0: {
          // X
          // 0: {
          //   items: [],
          //   ready: false,
          //   pending: false,
          //   loading: true,
          //   error: false,
          // },
          // },
        },
      },
    }
  },
}
