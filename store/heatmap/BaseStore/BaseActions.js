import RequestNotifications from '@/web-components-submodule/utils/RequestNotifications.js'
import HeatmapStd from '~/web-components-submodule/standardisation/HeatmapStd.js'
import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'

export default {
  getBaseActions() {
    return {
      setHeatmapID({ commit }, heatmapID) {
        commit('SET_HEATMAP_ID', heatmapID)
      },
      setHeatmapIDExpires({ commit }, idExpires) {
        commit('SET_HEATMAP_ID_EXPIRES', idExpires)
      },
      setDescription({ commit }, description) {
        commit('SET_DESCRIPTION', description)
      },
      fireItUp({ commit, state, dispatch }) {
        const heatmapDescriptor =
          state.heatmapID != null ? state.heatmapID : state.description

        ESProxyService.Heatmap.getContextualData(heatmapDescriptor)
          .then((response) => {
            const contextualData = response.data
            commit('SET_CELLS_CONTEXTUAL_DATA', contextualData)
            commit('SET_CONTEXTUAL_DATA_READY', true)
          })
          .catch((error) => {
            RequestNotifications.dispatchRequestErrorNotification(
              error,
              dispatch,
              `There was an error while loading the contextual data!`
            )
          })
      },
      loadAxisCount({ commit, state, dispatch }, axisID) {
        const axisString = HeatmapStd.axesNames[axisID].wsString

        const heatmapDescriptor =
          state.heatmapID != null ? state.heatmapID : state.description

        ESProxyService.Heatmap.getAxisSummary(heatmapDescriptor, axisString)
          .then((response) => {
            commit('SET_AXIS_ITEMS_COUNT', {
              axisID,
              totalItems: response.data.item_count,
            })
            dispatch('verifyInitialLoad')
          })
          .catch((error) => {
            RequestNotifications.dispatchRequestErrorNotification(
              error,
              dispatch,
              `There was an error while loading the heatmap axis ${axisString} summary!`
            )
          })
      },
      increaseZoom({ commit }) {
        commit('INCREASE_ZOOM')
      },
      decreaseZoom({ commit }) {
        commit('DECREASE_ZOOM')
      },
      resetZoom({ commit }) {
        commit('RESET_ZOOM')
      },
      verifyInitialLoad({ commit, state }) {
        const allReady = !(
          state.xAxis.totalItems == null ||
          isNaN(state.xAxis.totalItems) ||
          state.yAxis.totalItems == null ||
          isNaN(state.yAxis.totalItems)
        )
        commit('SET_ALL_READY', allReady)
      },
      setTopScroll({ commit }, topScroll) {
        commit('SET_TOP_SCROLL', topScroll)
      },
      setLeftScroll({ commit }, leftScroll) {
        commit('SET_LEFT_SCROLL', leftScroll)
      },
      setVisualWindowAxisSize({ commit, state, dispatch }, { axisID, size }) {
        const axisWindow = state.visualWindow[axisID]
        const start = axisWindow.start
        const end = axisWindow.start + size

        commit('SET_AXIS_VISUAL_WINDOW', { axisID, start, end })

        dispatch('loadMoreDataIfNecessary')
      },
      setMapWindowSize({ commit, state, dispatch }, size) {
        const mapWindow = state.mapWindow
        const xStart = mapWindow.xAxis.start
        const xEnd = xStart + size

        const yStart = mapWindow.yAxis.start
        const yEnd = yStart + size
        commit('SET_MAP_WINDOW', { xStart, xEnd, yStart, yEnd })

        dispatch('loadMoreDataIfNecessary')
      },
      moveInVisualWindow({ commit, state, dispatch }, { axisID, step }) {
        commit('MOVE_IN_VISUAL_WINDOW', { axisID, step })
        dispatch('loadMoreDataIfNecessary')
      },
      moveToVisualWindowAxisStart({ commit, state, dispatch }, { axisID }) {
        commit('MOVE_TO_VISUAL_WINDOW_AXIS_START', axisID)
        dispatch('loadMoreDataIfNecessary')
      },
      moveToVisualWindowAxisEnd({ commit, state, dispatch }, { axisID }) {
        commit('MOVE_TO_VISUAL_WINDOW_AXIS_END', axisID)
        dispatch('loadMoreDataIfNecessary')
      },
      moveToPointInVisualWindow(
        { commit, state, dispatch },
        { axisID, point }
      ) {
        commit('MOVE_TO_POINT_IN_VISUAL_WINDOW', { axisID, point })
      },
      moveToCoordinates({ commit, state, dispatch }, { x, y }) {
        commit('MOVE_TO_POINT_IN_VISUAL_WINDOW', {
          axisID: HeatmapStd.axesNames.xAxis.axisID,
          point: x,
        })
        commit('MOVE_TO_POINT_IN_VISUAL_WINDOW', {
          axisID: HeatmapStd.axesNames.yAxis.axisID,
          point: y,
        })
      },
      moveVisualWindowFromMinimap(
        { commit, state, dispatch },
        { xNewVisualWindowStart, yNewVisualWindowStart }
      ) {
        commit('MOVE_IN_VISUAL_WINDOW_FROM_MINIMAP', {
          xNewVisualWindowStart,
          yNewVisualWindowStart,
        })
      },
      showCellDetails({ commit, state, dispatch }) {
        if (state.freezeHover) {
          return
        }
        commit('SHOW_CELL_DETAILS')
      },
      hideCellDetails({ commit, state, dispatch }) {
        commit('HIDE_CELL_DETAILS')
      },
      setClickedCell({ commit, state, dispatch }, cellData) {
        commit('SET_CLICKED_CELL', cellData)
      },
      loadMoreDataIfNecessary({ commit, state, dispatch }) {
        const quadrantSideLength =
          state[HeatmapStd.axesNames.cells.axisID].quadrantSideLength
        const yExplorationWindow =
          state.mapWindow[HeatmapStd.axesNames.yAxis.axisID]
        const xExplorationWindow =
          state.mapWindow[HeatmapStd.axesNames.xAxis.axisID]

        // get the y coordinate of the quadrant where the visual window starts
        const explorationStartY =
          Math.floor(yExplorationWindow.start / quadrantSideLength) *
          quadrantSideLength

        // get the x coordinate of the quadrant where the visual window starts
        const explorationStartX =
          Math.floor(xExplorationWindow.start / quadrantSideLength) *
          quadrantSideLength

        const explorationEndY = yExplorationWindow.end
        const explorationEndX = xExplorationWindow.end

        // trigger the load of the quadrants involved, further actions don't trigger load again if the quadrant has already been loaded
        for (
          let quadrantYStart = explorationStartY;
          quadrantYStart < explorationEndY;
          quadrantYStart += quadrantSideLength
        ) {
          for (
            let quadrantXStart = explorationStartX;
            quadrantXStart < explorationEndX;
            quadrantXStart += quadrantSideLength
          ) {
            dispatch('loadQuadrantAndAxes', {
              quadrantYStart,
              quadrantXStart,
              quadrantSideLength,
            })
          }
        }

        dispatch('calculateLoadingWindow')
        dispatch('calculateNumLoadingChunks')
      },
      calculateLoadingWindow({ commit, state, dispatch }) {
        const dataQuadrants =
          state[HeatmapStd.axesNames.cells.axisID].dataQuadrants
        const quadrantSideLength =
          state[HeatmapStd.axesNames.cells.axisID].quadrantSideLength
        let xStart = Number.MAX_VALUE
        let xEnd = 0
        let yStart = Number.MAX_VALUE
        let yEnd = 0

        for (const y in dataQuadrants) {
          const row = dataQuadrants[y]
          for (const x in row) {
            const quadrant = row[x]
            if (quadrant.loading) {
              xStart = Math.min(xStart, parseInt(x))
              xEnd = Math.max(xEnd, parseInt(x) + quadrantSideLength)
              yStart = Math.min(yStart, parseInt(y))
              yEnd = Math.max(yEnd, parseInt(y) + quadrantSideLength)
            }
          }
        }

        commit('SET_LOADING_WINDOW', { xStart, xEnd, yStart, yEnd })
      },
      verifyChunksCoherence({ commit, state, dispatch }) {
        const dataQuadrants =
          state[HeatmapStd.axesNames.cells.axisID].dataQuadrants

        let numUncoherentChunks = 0

        for (const yStart in dataQuadrants) {
          const row = dataQuadrants[yStart]
          for (const xStart in row) {
            const quadrant = row[xStart]
            let quadrantIsCoherent = true
            if (quadrant.ready && quadrant.coherent == null) {
              for (let index = 0; index < quadrant.items.length; index++) {
                const currentItem = quadrant.items[index]
                const xIndexMustBe = index % state.dataChunkSize
                const xIndexGot = currentItem.x_index
                const yIndexMustBe = Math.floor(index / state.dataChunkSize)
                const yIndexGot = currentItem.y_index
                if (xIndexMustBe !== xIndexGot || yIndexMustBe !== yIndexGot) {
                  numUncoherentChunks++
                  quadrantIsCoherent = false
                  break
                }
              }
            }

            commit('SET_DATA_QUADRANT_COHERENT', {
              yStart,
              xStart,
              coherent: quadrantIsCoherent,
            })
          }
        }
        commit('SET_NUM_UNCOHERENT_DATA_CHUNKS', numUncoherentChunks)
        commit('SET_ALL_DATA_CHUNKS_COHERENT', numUncoherentChunks === 0)
      },
      calculateNumLoadingChunks({ commit, state, dispatch }) {
        let numLoadingChunks = 0

        const dataQuadrants =
          state[HeatmapStd.axesNames.cells.axisID].dataQuadrants

        // Data quadrants
        for (const y in dataQuadrants) {
          const row = dataQuadrants[y]
          for (const x in row) {
            const quadrant = row[x]
            if (quadrant.loading) {
              numLoadingChunks++
            }
          }
        }

        // Axis chunks
        for (const axisID of [
          HeatmapStd.axesNames.xAxis.axisID,
          HeatmapStd.axesNames.yAxis.axisID,
        ]) {
          for (const dataWindowID of [
            HeatmapStd.dataWindowsNames.headersDataWindow.windowID,
            HeatmapStd.dataWindowsNames.footersDataWindow.windowID,
          ]) {
            const chunks = state[axisID][dataWindowID].chunks

            for (const chunkStart in chunks) {
              const chunk = chunks[chunkStart]
              if (chunk.loading) {
                numLoadingChunks++
              }
            }
          }
        }

        commit('SET_NUM_LOADING_DATA_CHUNKS', numLoadingChunks)
        commit('SET_ALL_CHUNKS_LOADED', numLoadingChunks === 0)
        dispatch('verifyChunksCoherence')
      },
      loadQuadrantAndAxes(
        { commit, state, dispatch },
        { quadrantYStart, quadrantXStart, quadrantSideLength }
      ) {
        dispatch('addAxisChunkIfNotExistent', {
          axisID: HeatmapStd.axesNames.xAxis.axisID,
          windowID: HeatmapStd.dataWindowsNames.headersDataWindow.windowID,
          start: quadrantXStart,
        })

        dispatch('loadAxisChunk', {
          axisID: HeatmapStd.axesNames.xAxis.axisID,
          windowID: HeatmapStd.dataWindowsNames.headersDataWindow.windowID,
          start: quadrantXStart,
        })

        dispatch('addAxisChunkIfNotExistent', {
          axisID: HeatmapStd.axesNames.xAxis.axisID,
          windowID: HeatmapStd.dataWindowsNames.footersDataWindow.windowID,
          start: quadrantXStart,
        })

        dispatch('loadAxisChunk', {
          axisID: HeatmapStd.axesNames.xAxis.axisID,
          windowID: HeatmapStd.dataWindowsNames.footersDataWindow.windowID,
          start: quadrantXStart,
        })

        dispatch('addAxisChunkIfNotExistent', {
          axisID: HeatmapStd.axesNames.yAxis.axisID,
          windowID: HeatmapStd.dataWindowsNames.headersDataWindow.windowID,
          start: quadrantYStart,
        })

        dispatch('loadAxisChunk', {
          axisID: HeatmapStd.axesNames.yAxis.axisID,
          windowID: HeatmapStd.dataWindowsNames.headersDataWindow.windowID,
          start: quadrantYStart,
        })

        dispatch('addAxisChunkIfNotExistent', {
          axisID: HeatmapStd.axesNames.yAxis.axisID,
          windowID: HeatmapStd.dataWindowsNames.footersDataWindow.windowID,
          start: quadrantYStart,
        })

        dispatch('loadAxisChunk', {
          axisID: HeatmapStd.axesNames.yAxis.axisID,
          windowID: HeatmapStd.dataWindowsNames.footersDataWindow.windowID,
          start: quadrantYStart,
        })

        dispatch('addDataQuadrantIfNotExistent', {
          quadrantYStart,
          quadrantXStart,
        })

        dispatch('loadDataQuadrant', {
          yStart: quadrantYStart,
          yEnd: quadrantYStart + quadrantSideLength,
          xStart: quadrantXStart,
          xEnd: quadrantXStart + quadrantSideLength,
        })
      },
      addAxisChunkIfNotExistent(
        { commit, state, dispatch },
        { axisID, windowID, start }
      ) {
        const chunks = state[axisID][windowID].chunks
        if (chunks[start] == null) {
          commit('ADD_NEW_AXIS_CHUNK', {
            axisID,
            windowID,
            start,
          })
        }
      },
      addDataQuadrantIfNotExistent(
        { commit, state, dispatch },
        { quadrantYStart, quadrantXStart }
      ) {
        const dataQuadrants =
          state[HeatmapStd.axesNames.cells.axisID].dataQuadrants

        if (dataQuadrants[quadrantYStart] == null) {
          commit('ADD_NEW_DATA_ROW', { quadrantYStart })
        }

        if (dataQuadrants[quadrantYStart][quadrantXStart] == null) {
          commit('ADD_NEW_DATA_QUADRANT', {
            quadrantYStart,
            quadrantXStart,
          })
        }
      },
      loadDataQuadrant(
        { commit, state, dispatch },
        { yStart, yEnd, xStart, xEnd }
      ) {
        const dataQuadrants =
          state[HeatmapStd.axesNames.cells.axisID].dataQuadrants

        const dataQuadrant = dataQuadrants[yStart][xStart]
        if (dataQuadrant.loading || dataQuadrant.ready) {
          return
        }
        commit('SET_DATA_QUADRANT_LOADING', { yStart, xStart })

        const heatmapDescriptor =
          state.heatmapID != null ? state.heatmapID : state.description

        ESProxyService.Heatmap.getCells(
          heatmapDescriptor,
          yStart,
          yEnd,
          xStart,
          xEnd
        )
          .then((response) => {
            commit('SET_DATA_QUADRANT_LOADED', {
              yStart,
              xStart,
              cells: response.data.cells,
            })
            dispatch('calculateNumLoadingChunks')
          })
          .catch(() => {
            commit('SET_DATA_QUADRANT_ERROR', { yStart, xStart })
            dispatch('calculateNumLoadingChunks')
          })
      },
      loadAxisChunk({ commit, state, dispatch }, { axisID, windowID, start }) {
        const chunks = state[axisID][windowID].chunks
        const chunk = chunks[start]

        if (chunk.loading || chunk.ready) {
          return
        }

        commit('SET_AXIS_CHUNK_LOADING', { axisID, windowID, start })

        const axisString = HeatmapStd.axesNames[axisID].wsString
        const partString = HeatmapStd.dataWindowsNames[windowID].wsString
        const from = start
        const size = state.dataChunkSize

        const heatmapDescriptor =
          state.heatmapID != null ? state.heatmapID : state.description

        ESProxyService.Heatmap.getAxisItems(
          heatmapDescriptor,
          axisString,
          partString,
          from,
          size
        )
          .then((response) => {
            const items = response.data.items
            commit('SET_AXIS_CHUNK_LOADED', { axisID, windowID, start, items })
            dispatch('calculateNumLoadingChunks')
          })
          .catch(() => {
            commit('SET_AXIS_CHUNK_ERROR', { axisID, windowID, start })
            dispatch('calculateNumLoadingChunks')
          })
      },
      setCurrentFooterProperty(
        { commit, state, dispatch },
        { axisID, footerPropertyIndex }
      ) {
        commit('SET_CURRENT_FOOTER_PROPERTY', { axisID, footerPropertyIndex })
      },
      setCurrentHover(
        { commit, state, dispatch },
        { y, x, cellData, xHeaderData, yHeaderData, hoverOverFooter }
      ) {
        if (state.freezeHover) {
          return
        }
        commit('SET_CURRENT_HOVER', {
          x,
          y,
          cellData,
          xHeaderData,
          yHeaderData,
          hoverOverFooter,
        })
      },
      unSetCurrentHover({ commit, state, dispatch }) {
        if (state.freezeHover) {
          return
        }
        commit('UNSET_CURRENT_HOVER')
      },
      freezeHover({ commit, state, dispatch }) {
        commit('FREEZE_HOVER')
      },
      unFreezeHover({ commit, state, dispatch }) {
        commit('UN_FREEZE_HOVER')
        commit('UNSET_CURRENT_HOVER')
      },
      setCellProperty({ commit, state, dispatch }, cellPropertyIndex) {
        commit('SET_CELL_PROPERTY', cellPropertyIndex)
      },
    }
  },
}
