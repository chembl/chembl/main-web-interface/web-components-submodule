import BaseState from '~/web-components-submodule/store/heatmap/BaseStore/BaseState.js'
import BaseMutations from '~/web-components-submodule/store/heatmap/BaseStore/BaseMutations.js'
import BaseActions from '~/web-components-submodule/store/heatmap/BaseStore/BaseActions.js'
import BaseMethods from '~/web-components-submodule/store/heatmap/BaseStore/BaseMethods.js'

const _ = require('lodash')

const baseStore = {
  state: () => BaseState.getBaseState(),
  mutations: BaseMutations.getBaseMutations(),
  actions: BaseActions.getBaseActions(),
  methods: BaseMethods.getBaseMethods(),
  namespaced: true,
}

const methods = {
  generateDatasetStoreModule() {
    const generatedStore = _.cloneDeep(baseStore)
    return generatedStore
  },
}
export default methods
