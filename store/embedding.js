export default {
  state: () => ({
    isEmbedded: false,
  }),

  mutations: {
    SET_IS_EMBEDDED(state, isEmbedded) {
      state.isEmbedded = isEmbedded
    },
  },

  actions: {
    setIsEmbedded({ commit }, isEmbedded = true) {
      commit('SET_IS_EMBEDDED', isEmbedded)
    },
  },
}
