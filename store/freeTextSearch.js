import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'
import LinksToSearches from '~/web-components-submodule/standardisation/LinksToSearches.js'
import SearchNames from '~/web-components-submodule/standardisation/SearchNames.js'

const helperMethods = {
  getB64State(searchState) {
    const strState = JSON.stringify(searchState)
    const b64State = btoa(strState)
    return b64State
  },
}
export default {
  state: () => ({
    searchTerm: undefined,
    currentlyInASearchPage: false,
    searchState: {
      entities: {},
    },
    loadingStateHash: false,
    stateHash: undefined,
    stateExpires: undefined,
  }),
  mutations: {
    SET_SEARCH_TERM(state, searchTerm) {
      state.searchTerm = searchTerm
    },
    SET_CURRENTLY_IN_A_SEARCH_PAGE(state, currentlyInASearchPage) {
      state.currentlyInASearchPage = currentlyInASearchPage
    },
    SET_SEARCH_STATE(state, searchState) {
      state.searchState = searchState
    },
    APPEND_TO_SEARCH_STATE(state, { datasetState, entityID }) {
      state.searchState.entities[entityID] = datasetState
    },
    SET_LOADING_STATE_HASH(state, loadingStateHash) {
      state.loadingStateHash = loadingStateHash
    },
    SET_STATE_HASH(state, stateHash) {
      state.stateHash = stateHash
    },
    SET_STATE_EXPIRES(state, stateExpires) {
      state.stateExpires = stateExpires
    },
  },
  actions: {
    setSearchState({ commit }, searchState) {
      commit('SET_SEARCH_STATE', searchState)
    },
    setSearchTerm({ commit }, searchTerm) {
      commit('SET_SEARCH_TERM', searchTerm)
    },
    setCurrentlyInASearchPage({ commit }, currentlyInASearchPage) {
      commit('SET_CURRENTLY_IN_A_SEARCH_PAGE', currentlyInASearchPage)
    },
    appendToSearchState({ commit, state }, { datasetState, entityID, router }) {
      commit('APPEND_TO_SEARCH_STATE', { datasetState, entityID })

      const stateToExport = state.searchState

      commit('SET_LOADING_STATE_HASH', true)
      const b64State = helperMethods.getB64State(stateToExport)

      ESProxyService.getHashForLongURL(b64State).then((response) => {
        const hash = response.data.hash

        const expires = response.data.expires
        const stateID = `STATE_ID:${hash}`

        const searchURL = LinksToSearches[
          SearchNames.ChemblTextSearch.searchID
        ].getLinkToSearchWithState(state.searchTerm, stateID, true)

        // push this url into history!
        router.replace({ path: searchURL, query: router.query })
        commit('SET_LOADING_STATE_HASH', false)
        commit('SET_STATE_HASH', hash)
        commit('SET_STATE_EXPIRES', expires)
      })
    },
  },
}
