export default {
  state: () => ({
    notifications: [],
  }),
  mutations: {
    PUSH(state, notification) {
      state.notifications.push(notification)
    },
    POP(state) {
      state.notifications.pop()
    },
    POP_ALL(state) {
      let isEmpty = state.notifications.pop() == null
      while (!isEmpty) {
        isEmpty = state.notifications.pop() == null
      }
    },
  },
  actions: {
    add({ commit }, notification) {
      commit('PUSH', notification)
    },
    removeLast({ commit }) {
      commit('POP')
    },
    removeAll({ commit }) {
      commit('POP_ALL')
    },
  },
}
