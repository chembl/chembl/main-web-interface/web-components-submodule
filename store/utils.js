export default {
  state: () => ({
    showMasthead: true,
  }),
  mutations: {
    SET_SHOW_MASTHEAD(state, show) {
      state.showMasthead = show
    },
  },
  actions: {
    setShowMasthead({ commit }, show) {
      commit('SET_SHOW_MASTHEAD', show)
    },
  },
}
