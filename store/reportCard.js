export default {
  state: () => ({
    structureReady: false,
    reportCardStructure: {},
    activeSection: undefined,
  }),
  mutations: {
    SET_REPORT_CARD_STRUCTURE(state, reportCardStructure) {
      state.reportCardStructure = reportCardStructure
    },
    SET_ACTIVE_SECTION(state, activeSection) {
      state.activeSection = activeSection
    },
    SET_STRUCTURE_READY(state, structureReady) {
      state.structureReady = structureReady
    },
  },
  actions: {
    setReportCardStructure({ commit }, reportCardStructure) {
      commit('SET_REPORT_CARD_STRUCTURE', reportCardStructure)
    },
    setActiveSection({ commit }, activeSection) {
      commit('SET_ACTIVE_SECTION', activeSection)
    },
    setStructureReady({ commit }, structureReady) {
      commit('SET_STRUCTURE_READY', structureReady)
    },
  },
}
