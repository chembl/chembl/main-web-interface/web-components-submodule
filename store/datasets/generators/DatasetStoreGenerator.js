import BaseState from '@/web-components-submodule/store/datasets/BaseStore/BaseState.js'
import BaseMutations from '@/web-components-submodule/store/datasets/BaseStore/BaseMutations.js'
import BaseActions from '@/web-components-submodule/store/datasets/BaseStore/BaseActions.js'
import BaseMethods from '@/web-components-submodule/store/datasets/BaseStore/BaseMethods.js'

const _ = require('lodash')

const baseStore = {
  state: () => BaseState.getBaseState(),
  mutations: BaseMutations.getBaseMutations(),
  actions: BaseActions.getBaseActions(),
  methods: BaseMethods,
  namespaced: true,
}

const methods = {
  generateDatasetStoreModule() {
    const generatedStore = _.cloneDeep(baseStore)
    return generatedStore
  },
}
export default methods
