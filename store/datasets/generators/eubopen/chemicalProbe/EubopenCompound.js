import IndexNames from '@/web-components-submodule/standardisation/IndexNames.js'
import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'

const methods = {
  getControlStructuresAndUseQuery(itemID) {
    return {
      query: {
        bool: {
          must: [
            {
              terms: {
                '_metadata.eubopen.has_probe': [itemID],
              },
            },
          ],
        },
      },
    }
  },
  getProbesForThisCompoundQuery(itemID) {
    return {
      query: {
        bool: {
          must: [
            {
              terms: {
                '_metadata.eubopen.is_probe_for': [itemID],
              },
            },
          ],
        },
      },
    }
  },
  getControlStructuresAndUseParams(itemID, propertiesGroups) {
    const initialQuery = methods.getControlStructuresAndUseQuery(itemID)
    const entityID = EntityNames.EubopenCompound.entityID
    const starterParams = {
      propertiesGroups,
      initialQuery,
      indexName: IndexNames.getIndexNameFromEntityID(entityID),
    }
    return starterParams
  },
  getProbesForThisCompoundParams(itemID, propertiesGroups) {
    const initialQuery = methods.getProbesForThisCompoundQuery(itemID)
    const entityID = EntityNames.EubopenCompound.entityID
    const starterParams = {
      propertiesGroups,
      initialQuery,
      indexName: IndexNames.getIndexNameFromEntityID(entityID),
    }
    return starterParams
  },
}

export default methods
