import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'
import StarterParams from '~/web-components-submodule/store/datasets/generators/shared/StarterParams.js'

const methods = {
  getInitialQueryBrowser() {
    return {}
  },
  getBroswerStarterParams(propertiesGroups, dataSetParamsFromState) {
    return StarterParams.getBrowserStarterParams(
      dataSetParamsFromState,
      methods.getInitialQueryBrowser(),
      EntityNames.EubopenTarget.entityID,
      propertiesGroups,
      'eubopen_browser_facets'
    )
  },
}

export default methods
