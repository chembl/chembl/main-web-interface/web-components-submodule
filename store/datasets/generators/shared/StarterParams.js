import IndexNames from '@/web-components-submodule/standardisation/IndexNames.js'

const methods = {
  // this should be deprecated, look at the computed property in the datasetMixin called starterParams
  getBrowserStarterParams(
    dataSetParamsFromState,
    defaultQuery,
    entityID,
    propertiesGroups,
    facetsGroups,
    contextObj
  ) {
    let initialQuery
    let subsetHumanDescription
    let initialFacetsState
    let customFiltering
    let quickSearchTerm
    let exactTextFilters

    if (dataSetParamsFromState != null) {
      initialQuery = dataSetParamsFromState.initialQuery
      subsetHumanDescription = dataSetParamsFromState.subsetHumanDescription
      initialFacetsState = dataSetParamsFromState.facetsState
      customFiltering = dataSetParamsFromState.customFiltering
      quickSearchTerm = dataSetParamsFromState.quickSearchTerm
      exactTextFilters = dataSetParamsFromState.exactTextFilters
    }

    initialQuery = initialQuery == null ? defaultQuery : initialQuery

    const starterParams = {
      propertiesGroups,
      initialQuery,
      indexName: IndexNames.getIndexNameFromEntityID(entityID),
      facetsGroups,
      subsetHumanDescription,
      initialFacetsState,
      customFiltering,
      quickSearchTerm,
      exactTextFilters,
      contextObj,
    }
    return starterParams
  },
}

export default methods
