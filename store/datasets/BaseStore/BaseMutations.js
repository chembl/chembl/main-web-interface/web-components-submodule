import Vue from 'vue'
import HeaderMethods from '~/web-components-submodule/store/datasets/BaseStore/HeaderMethods.js'
import FiltersViewNames from '~/web-components-submodule/standardisation/datasets/FiltersViewNames.js'

const _ = require('lodash')

const HelperMethods = {
  checkIfAllCurrentFiltersDataAreLoaded(state) {
    const currentFacets = state.currentFacets

    for (let i = 0; i < currentFacets.length; i++) {
      const currentFacet = currentFacets[i]
      if (!currentFacet.show) {
        continue
      }

      const currentView = currentFacet.currentView
      if (currentView === FiltersViewNames.histogram) {
        const loadingHistogram = currentFacet.loadingHistogram
        if (loadingHistogram) {
          return false
        }
      }

      if (currentView === FiltersViewNames.range) {
        const loadingRange = currentFacet.loadingRange
        if (loadingRange) {
          return false
        }
      }
    }
    return true
  },
}

export default {
  getBaseMutations() {
    return {
      SET_INITIAL_DATA_LOADED(state) {
        state.initialLoad = true
      },
      SET_INITIAL_DATA_NOT_LOADED(state) {
        state.initialLoad = false
      },
      SET_PAGE_LOADING(state, isLoading) {
        state.pageLoading = isLoading
      },
      SET_INDEX_NAME(state, indexName) {
        state.indexName = indexName
      },
      SET_ENTITY_ID(state, entityID) {
        state.entityID = entityID
      },
      SET_SEARCH_TERM(state, searchTerm) {
        state.searchTerm = searchTerm
      },
      SET_CURRENT_VIEW(state, currentView) {
        state.currentView = currentView
      },
      SET_SUBSET_HUMAN_DESCRIPTION(state, subsetHumanDescription) {
        state.subsetHumanDescription = subsetHumanDescription
      },
      SET_DOWNLOAD_PROPERTIES_GROUP(state, downloadPropertiesGroup) {
        state.downloadPropertiesGroup = downloadPropertiesGroup
      },
      SET_POSSIBLE_JOIN_DESTINATIONS(state, possibleJoinDestinations) {
        state.possibleJoinDestinations = possibleJoinDestinations
      },
      SET_QUERY(state, query) {
        state.query = Object.assign({}, query)
      },
      SET_INITIAL_QUERY(state, initialQuery) {
        state.initialQuery = Object.assign({}, initialQuery)
      },
      SET_SLOTS(state, slots) {
        state.slots = slots
      },
      SET_SEARCH_RESULTS_ITEM(state, searchResultsItemSlot) {
        state.searchResultsItemSlot = searchResultsItemSlot
      },
      SET_TOTAL_ITEMS(state, totalItems) {
        state.totalItems = totalItems
      },
      SET_PAGE(state, page) {
        state.page = page
      },
      SET_ITEMS_PER_PAGE(state, itemsPerPage) {
        state.itemsPerPage = itemsPerPage
      },
      SET_CURRENT_PAGE_START(state, currentPageStart) {
        state.currentPageStart = currentPageStart
      },
      SET_CURRENT_PAGE_END(state, currentPageEnd) {
        state.currentPageEnd = currentPageEnd
      },
      SET_PAGE_COUNT(state, pageCount) {
        state.pageCount = pageCount
      },
      SET_PROPERTIES_GROUPS(state, propertiesGroups) {
        state.propertiesGroups = propertiesGroups
      },
      SET_PROPERTIES_INDEX(state, propertiesIndex) {
        state.propertiesIndex = propertiesIndex
      },
      SET_CONTEXT_OBJ(state, contextObj) {
        state.contextObj = contextObj
      },
      SET_FACETS_GROUPS(state, facetsGroups) {
        state.facetsGroups = facetsGroups
      },
      SET_FACETS_LOADED(state, facetsLoaded) {
        state.facetsLoaded = facetsLoaded
      },
      SET_HEADERS_FOR_GROUP(state, { groupKey, allHeaders }) {
        state.propertiesGroups[groupKey].allHeaders = allHeaders
      },
      SET_CURRENT_HEADERS(state, currentHeaders) {
        state.currentHeaders = currentHeaders
      },
      SET_CURRENT_PROPERTY_GROUP(state, groupKey) {
        for (const [theGroupKey, currentGroup] of Object.entries(
          state.propertiesGroups
        )) {
          if (groupKey === theGroupKey) {
            Vue.set(currentGroup, 'selected', true)
          } else {
            Vue.set(currentGroup, 'selected', false)
          }
        }
      },
      SET_CURRENT_FACETS(state, currentFacets) {
        state.currentFacets = currentFacets
      },
      SET_CURRENT_ITEMS(state, currentItems) {
        state.currentItems = currentItems
      },
      SET_SORT_BY(state, sortBy) {
        state.sortBy = sortBy
      },
      SET_SORT_DESC(state, sortDesc) {
        state.sortDesc = sortDesc
      },
      SET_SORT_OBJECT(state, sortObject) {
        state.sortObject = sortObject
      },
      SET_POSSIBLE_PAGE_SIZES(state, possiblePageSizes) {
        state.possiblePageSizes = possiblePageSizes
      },
      SET_PLURAL_ENTITY_NAME(state, pluralEntityName) {
        state.pluralEntityName = pluralEntityName
      },
      SET_QUERYSTRING_EXAMPLES(state, querystringExamples) {
        state.querystringExamples = querystringExamples
      },
      SET_ENABLE_SELECTION(state, enableSelection) {
        state.enableSelection = enableSelection
      },
      SET_ENABLE_MULTISORT(state, enableMultiSort) {
        state.enableMultiSort = enableMultiSort
      },
      SET_ENABLE_SIMILARITY_MAPS(state, enableSimilarityMaps) {
        state.enableSimilarityMaps = enableSimilarityMaps
      },
      SET_SHOW_SIMILARITY_MAPS(state, showSimilarityMaps) {
        state.similarityMaps.show = showSimilarityMaps
      },
      SET_ENABLE_SUBSTRUCTURE_HIGHLIGHT(state, enableSubstructureHighlight) {
        state.enableSubstructureHighlight = enableSubstructureHighlight
      },
      SET_SHOW_SUBSTRUCTURE_HIGHLIGHT(state, showSubstructureHighlight) {
        state.substructureHighlight.show = showSubstructureHighlight
      },
      SET_SIMILARITY_MAPS_REFERENCE_STRUCTURE(
        state,
        similarityMapsReferenceStructure
      ) {
        state.similarityMaps.referenceStructure = similarityMapsReferenceStructure
      },
      SET_SUBSTRUCTURE_HIGHLIGHT_REFERENCE_STRUCTURE(
        state,
        substructureHighlightReferenceStructure
      ) {
        state.substructureHighlight.referenceStructure = substructureHighlightReferenceStructure
      },
      SET_ENABLE_STATE_SAVING(state, enableStateSaving) {
        state.enableStateSaving = enableStateSaving
      },
      SET_ENABLE_QUICK_TEXT_SEARCH(state, enableQuickTextSearch) {
        state.enableQuickTextSearch = enableQuickTextSearch
      },
      SET_ENABLE_HEATMAP_GENERATION(state, enableHeatmapGeneration) {
        state.enableHeatmapGeneration = enableHeatmapGeneration
      },
      SET_GENERATING_HEATMAP_URL(state, generatingURL) {
        state.heatmapGeneration.generatingURL = generatingURL
      },
      SET_QUERYSTRING(state, querystring) {
        state.querystring = querystring
      },
      SET_QUERYSTRING_TO_APPLY(state, querystringToApply) {
        state.querystringToApply = querystringToApply
      },
      SET_INITIAL_FACETS_STATE(state, initialFacetsState) {
        state.initialFacetsState = initialFacetsState
      },
      SET_INITIAL_FACETS_STATE_WAS_CONSUMED(state, { filterKey, wasConsumed }) {
        const initialFacetsState = state.initialFacetsState
        const modifiedFacet = initialFacetsState.find(
          (filter) => filter.prop_id === filterKey
        )
        Vue.set(modifiedFacet, 'consumed', wasConsumed)
      },
      SET_INITIAL_FACETS_STATE_WAS_CONSUMED_FOR_INITIAL_PAGE_LOAD(
        state,
        { filterKey, wasConsumed }
      ) {
        const initialFacetsState = state.initialFacetsState
        const modifiedFacet = initialFacetsState.find(
          (filter) => filter.prop_id === filterKey
        )
        Vue.set(modifiedFacet, 'consumedForFirstPageLoad', wasConsumed)
      },
      REPLACE_FACET_STATE(state, { filterKey, facetState }) {
        const currentFacets = state.currentFacets
        const mofifiedFacets = []

        for (let i = 0; i < currentFacets.length; i++) {
          const currFacet = currentFacets[i]
          if (currFacet.prop_id !== filterKey) {
            mofifiedFacets.push(currFacet)
          } else {
            mofifiedFacets.push(facetState)
          }
        }
        Vue.set(state, 'currentFacets', mofifiedFacets)
      },
      SET_QUERYSTRING_WAS_APPLIED(state, querystringWasApplied) {
        state.querystringWasApplied = querystringWasApplied
      },
      SET_MAPPINGS_FOR_QUERYSTRING(state, mappingsForQuerystring) {
        state.mappingsForQuerystring = mappingsForQuerystring
      },
      SET_LOADED_MAPPINGS_FOR_QUERYSTRING(state, loadedMappingsForQuerystring) {
        state.loadedMappingsForQuerystring = loadedMappingsForQuerystring
      },
      SET_DOWNLOAD_FORMAT_PROCESSING(state, { formatID, isProcessing }) {
        state.downloadFormats[formatID].processing = isProcessing
      },
      UPDATE_DATASET_DOWNLOAD_PROPS(state, downloadProps) {
        const jobID = downloadProps.id
        const downloadIndex = state.datasetJobs.downloads.findIndex(
          (elem) => elem.id === jobID
        )

        if (downloadIndex === -1) {
          state.datasetJobs.downloads.push(downloadProps)
        } else {
          const existingDownload = state.datasetJobs.downloads[downloadIndex]
          const propertiesToCheck = ['status', 'progress', 'title', 'fileUrl']
          for (let i = 0; i < propertiesToCheck.length; i++) {
            const propertyName = propertiesToCheck[i]
            const oldValue = existingDownload[propertyName]
            const newValue = downloadProps[propertyName]

            if (oldValue !== newValue) {
              existingDownload[propertyName] = downloadProps[propertyName]
            }
          }

          Vue.set(state.datasetJobs.downloads, downloadIndex, existingDownload)
        }
      },
      SET_ENTITY_JOIN_PROCESSING(state, { entityID, isProcessing }) {
        const currentEntityJoinState = state.possibleJoinDestinations
        const newEntityJoinState = currentEntityJoinState.map((item) => {
          if (item.entityID === entityID) {
            item.processing = isProcessing
          }
          return item
        })
        Vue.set(state, 'possibleJoinDestinations', newEntityJoinState)
      },
      SET_JOIN_MODE(state, joinMode) {
        state.joinMode = joinMode
      },
      UPDATE_FILTER_HISTOGRAM_PARAMS(state, { filterKey, histogramParams }) {
        const currentFacets = state.currentFacets
        const modifiedFacet = _.find(currentFacets, { key: filterKey })
        Vue.set(modifiedFacet, 'histogramParams', histogramParams)
      },
      UPDATE_FILTER_CURRENT_VIEW(state, { filterKey, currentView }) {
        const currentFacets = state.currentFacets
        const modifiedFacet = _.find(currentFacets, { key: filterKey })
        Vue.set(modifiedFacet, 'currentView', currentView)
      },
      UPDATE_FILTER_NUMERIC_VALUES_PARAMS(
        state,
        { filterKey, numericValuesData }
      ) {
        const currentFacets = state.currentFacets
        const modifiedFacet = _.find(currentFacets, { key: filterKey })
        Vue.set(modifiedFacet, 'numericValuesData', numericValuesData)
      },
      UPDATE_SELECTED_RANGE_DATA(state, { filterKey, selectedRange }) {
        const currentFacets = state.currentFacets
        const modifiedFacet = _.find(currentFacets, { key: filterKey })
        Vue.set(modifiedFacet, 'selectedRange', selectedRange)
      },
      UPDATE_FILTER_HISTOGRAM_DATA(state, { filterKey, histogramData }) {
        const currentFacets = state.currentFacets
        const modifiedFacet = _.find(currentFacets, { key: filterKey })
        Vue.set(modifiedFacet, 'histogramData', histogramData)
      },
      UPDATE_FILTER_HISTOGRAM_QUERY(state, { filterKey, histogramQuery }) {
        const currentFacets = state.currentFacets
        const modifiedFacet = _.find(currentFacets, { key: filterKey })
        Vue.set(modifiedFacet, 'histogramQuery', histogramQuery)
      },
      SET_LOADING_FILTER_HISTOGRAM_DATA(
        state,
        { filterKey, loadingHistogram }
      ) {
        const currentFacets = state.currentFacets
        const modifiedFacet = _.find(currentFacets, { key: filterKey })
        Vue.set(modifiedFacet, 'loadingHistogram', loadingHistogram)
        state.allCurrentFiltersDataLoaded = HelperMethods.checkIfAllCurrentFiltersDataAreLoaded(
          state
        )
      },
      SET_LOADING_FILTER_RANGE_DATA(state, { filterKey, loadingRange }) {
        const currentFacets = state.currentFacets
        const modifiedFacet = _.find(currentFacets, { key: filterKey })
        Vue.set(modifiedFacet, 'loadingRange', loadingRange)
        state.allCurrentFiltersDataLoaded = HelperMethods.checkIfAllCurrentFiltersDataAreLoaded(
          state
        )
      },
      TOGGLE_FILTER_TERM(state, { filterKey, term }) {
        const currentFacets = state.currentFacets
        const modifiedFacet = _.find(currentFacets, { key: filterKey })
        const histogramData = modifiedFacet.histogramData

        const selectedBucket = _.find(
          histogramData.buckets,
          (bucket) => String(bucket.key) === String(term)
        )
        selectedBucket.selected = !selectedBucket.selected
        const atLeastOneBucketIsSelected =
          _.find(histogramData.buckets, (bucket) => bucket.selected) != null
        Vue.set(modifiedFacet, 'locked', atLeastOneBucketIsSelected)
        Vue.set(modifiedFacet, 'histogramData', modifiedFacet.histogramData)
      },
      TOGGLE_FILTER_RANGE(state, { filterKey, range }) {
        const currentFacets = state.currentFacets
        const modifiedFacet = _.find(currentFacets, { key: filterKey })
        const selectedRange = modifiedFacet.selectedRange
        selectedRange.minSelection = range[0]
        selectedRange.maxSelection = range[1]
        const lockFilter =
          selectedRange.minSelection >
            modifiedFacet.numericValuesData.minValue ||
          selectedRange.maxSelection < modifiedFacet.numericValuesData.maxValue
        Vue.set(modifiedFacet, 'locked', lockFilter)
        Vue.set(modifiedFacet, 'selectedRange', selectedRange)
      },
      TOGGLE_FILTER(state, filterKey) {
        const currentFacets = state.currentFacets
        const modifiedFacet = _.find(currentFacets, { key: filterKey })
        Vue.set(modifiedFacet, 'show', !modifiedFacet.show)
      },
      SET_SHOW_FILTER(state, { filterKey, show }) {
        const currentFacets = state.currentFacets
        const modifiedFacet = _.find(currentFacets, { key: filterKey })
        Vue.set(modifiedFacet, 'show', show)
      },
      MOVE_FILTERS(state, { originKey, destinationKey }) {
        const currentFacets = state.currentFacets
        const originFacet = _.find(
          currentFacets,
          (facet) => facet.key === originKey
        )

        const destinationFacet = _.find(
          currentFacets,
          (facet) => facet.key === destinationKey
        )

        const originPosition = originFacet.server_initial_config.position
        const destinationPosition =
          destinationFacet.server_initial_config.position

        if (originPosition + 1 === destinationPosition) {
          return
        }

        const direction = destinationPosition > originPosition ? 1 : -1

        let rangeStart = Math.min(originPosition, destinationPosition) + 1
        const rangeEnd = Math.max(originPosition, destinationPosition) - 1

        if (direction === -1) {
          rangeStart -= 1
        }

        const modifiedFacets = state.currentFacets.filter(
          (filter) =>
            filter.server_initial_config.position >= rangeStart &&
            filter.server_initial_config.position <= rangeEnd
        )

        for (const modifiedFacet of modifiedFacets) {
          Vue.set(
            modifiedFacet.server_initial_config,
            'position',
            modifiedFacet.server_initial_config.position - direction
          )
        }

        Vue.set(
          originFacet.server_initial_config,
          'position',
          destinationPosition
        )
      },
      TOGGLE_COLUMN(state, columnKey) {
        const selectedGroupKey = HeaderMethods.getSelectedPropertyGroupKey(
          state.propertiesGroups
        )
        const allHeaders = state.propertiesGroups[selectedGroupKey].allHeaders
        const modifiedColumn = allHeaders[columnKey]
        Vue.set(modifiedColumn, 'show', !modifiedColumn.show)
      },
      MOVE_COLUMNS(state, { originKey, destinationKey }) {
        const selectedGroupKey = HeaderMethods.getSelectedPropertyGroupKey(
          state.propertiesGroups
        )
        const allHeaders = state.propertiesGroups[selectedGroupKey].allHeaders
        const originColumn = allHeaders[originKey]
        const destinationColumn = allHeaders[destinationKey]

        const originPosition = originColumn.position
        const destinationPosition = destinationColumn.position

        if (originPosition + 1 === destinationPosition) {
          return
        }

        const direction = destinationPosition > originPosition ? 1 : -1

        let rangeStart = Math.min(originPosition, destinationPosition) + 1
        const rangeEnd = Math.max(originPosition, destinationPosition) - 1

        if (direction === -1) {
          rangeStart -= 1
        }

        const modifiedColumns = Object.entries(allHeaders).filter(
          (column) =>
            column.position >= rangeStart && column.position <= rangeEnd
        )

        for (const modifiedCol of modifiedColumns) {
          Vue.set(modifiedCol, 'position', modifiedCol.position - direction)
        }

        Vue.set(originColumn, 'position', destinationPosition)
      },
      SELECT_SPECIFIC_TERMS(state, { filterKey, terms }) {
        const currentFacets = state.currentFacets
        const modifiedFacet = _.find(currentFacets, { key: filterKey })
        const lockFilter = terms.length > 0

        Vue.set(modifiedFacet, 'locked', lockFilter)
        Vue.set(modifiedFacet, 'specificTermsSelected', terms)
      },
      CLEAR_FILTERS_SELECTION(state, filterKey) {
        const currentFacets = state.currentFacets
        const modifiedFacet = _.find(currentFacets, { key: filterKey })
        const histogramData = modifiedFacet.histogramData

        if (histogramData.buckets == null) {
          return
        }
        histogramData.buckets.forEach((bucket) => (bucket.selected = false))
        Vue.set(modifiedFacet, 'specificTermsSelected', [])
        Vue.set(modifiedFacet, 'selectedRange', {})
        Vue.set(modifiedFacet, 'locked', false)
      },
      UNLOCK_FILTER(state, filterKey) {
        const currentFacets = state.currentFacets
        const modifiedFacet = _.find(currentFacets, { key: filterKey })
        Vue.set(modifiedFacet, 'locked', false)
      },
      SET_QUICK_SEARCH_TERM(state, quickSearchTerm) {
        state.quickSearchTerm = quickSearchTerm
      },
      SET_EXACT_TEXT_FILTERS_FROM_STATE(state, exactTextFilters) {
        state.exactTextFilters = exactTextFilters
      },
      SET_EXACT_TEXT_FILTER(state, { valueAccessor, filterValue }) {
        Vue.set(state.exactTextFilters, valueAccessor, filterValue)
      },
      UNSET_EXACT_TEXT_FILTER(state, valueAccessor) {
        Vue.set(state.exactTextFilters, valueAccessor, null)
      },
      UNSET_ALL_EXACT_TEXT_FILTERS(state) {
        state.exactTextFilters = {}
      },
      ADD_ITEM_TO_SELECTION_EXCEPTIONS(state, id) {
        const selectionExceptions = state.selection.exceptions
        const selectionExceptionsSet = new Set(selectionExceptions)
        selectionExceptionsSet.add(id)
        Vue.set(state.selection, 'exceptions', [...selectionExceptionsSet])
      },
      REMOVE_ITEM_FROM_SELECTION_EXCEPTIONS(state, id) {
        const selectionExceptions = state.selection.exceptions
        const selectionExceptionsSet = new Set(selectionExceptions)
        delete selectionExceptionsSet.delete(id)
        Vue.set(state.selection, 'exceptions', [...selectionExceptionsSet])
      },
      SET_SELECTION_MODE(state, selectionMode) {
        state.selection.selectionMode = selectionMode
      },
      RESET_SELECTION_EXCEPTIONS(state) {
        Vue.set(state.selection, 'exceptions', [])
      },
      SET_NUM_SELECTED_ITEMS(state, numItemsSelected) {
        state.selection.numItemsSelected = numItemsSelected
      },
      SET_SOME_ITEMS_SELECTED(state, someItemsSelected) {
        state.selection.someItemsSelected = someItemsSelected
      },
      SET_ALL_ITEMS_SELECTED(state, allItemsSelected) {
        state.selection.allItemsSelected = allItemsSelected
      },
      SET_SELECTED_IN_PAGE(state, selectedInPage) {
        Vue.set(state.selection, 'selectedInPage', selectedInPage)
      },
      SET_PROCESSING_BROWSE_ALL_LINK(state, isProcessing) {
        state.browseAll.processingLink = isProcessing
      },
      SET_EMBEDDING_ID_FOR_PARENT(state, idForParent) {
        state.embedding.idForParent = idForParent
      },
      SET_STATE_SAVING(state, stateSaving) {
        Vue.set(state, 'stateSaving', stateSaving)
      },
      SET_USE_HASH_IN_URL(state, useHashInURL) {
        state.stateSaving.useHashInURL = useHashInURL
      },
      SET_LOADING_STATE_HASH(state, isLoading) {
        state.stateSaving.loadingHash = isLoading
      },
      SET_STATE_HASH(state, sateHash) {
        state.stateSaving.sateHash = sateHash
      },
      SET_STATE_EXPIRES(state, expires) {
        state.stateSaving.expires = expires
      },
      SET_B64_STATE(state, b64State) {
        state.stateSaving.b64State = b64State
      },
      ADD_DOWNLOAD_FORMAT(state, formatID) {
        state.downloadFormats[formatID] = {
          id: formatID,
          processing: false,
        }
      },
      TOGGLE_DEBUG_MODE(state) {
        state.debug = !state.debug
      },
      SET_QUICK_SEARCH_PRIORITIES(state, quickSearchPriorities) {
        state.quickSearchPriorities.priorities = quickSearchPriorities
      },
      SET_QUICK_SEARCH_PRIORITIES_LOADED(state, loaded) {
        state.quickSearchPriorities.prioritiesLoaded = loaded
      },
    }
  },
}
