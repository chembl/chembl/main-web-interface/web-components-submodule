import HeaderMethods from '@/web-components-submodule/store/datasets/BaseStore/HeaderMethods.js'
import ItemsMethods from '@/web-components-submodule/store/datasets/BaseStore/ItemsMethods.js'
import DownloadMethods from '@/web-components-submodule/store/datasets/BaseStore/DownloadMethods.js'
import MappingsMethods from '@/web-components-submodule/store/datasets/BaseStore/MappingsMethods.js'

const methods = {
  ...HeaderMethods,
  ...ItemsMethods,
  ...DownloadMethods,
  ...MappingsMethods,
}

export default methods
