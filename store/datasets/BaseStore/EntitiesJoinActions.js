import JoinModes from '@/web-components-submodule/standardisation/datasets/JoinModes.js'
import RequestNotifications from '@/web-components-submodule/utils/RequestNotifications.js'
import LinksToBrowsers from '~/web-components-submodule/standardisation/LinksToBrowsers.js'
import EntitiesJoin from '~/web-components-submodule/standardisation/datasets/EntitiesJoin.js'
import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'
import ErrorTracking from '~/web-components-submodule/tracking/ErrorTracking.js'

const actions = {
  triggerEntityJoin({ commit, state, dispatch, rootState }, entityID) {
    commit('SET_ENTITY_JOIN_PROCESSING', { entityID, isProcessing: true })

    let selectionDescription
    if (state.joinMode === JoinModes.allItems) {
      selectionDescription = {
        selectionMode: 'allItemsExcept',
        exceptions: [],
      }
    } else {
      selectionDescription = {
        selectionMode: state.selection.selectionMode,
        exceptions: state.selection.exceptions,
      }
    }

    const originEntityID = state.entityID
    const destinationEntityID = entityID
    const joinParams = {
      destinationEntityBrowserStateTemplate:
        LinksToBrowsers.entityJoinDestinationTemplate,
      entityFrom: EntitiesJoin.getOriginParamName(originEntityID),
      entityTo: EntitiesJoin.getDestinationParamName(
        originEntityID,
        destinationEntityID
      ),
      query: state.query,
      selectionDescription,
      context_obj: state.contextObj,
    }

    ESProxyService.getHashForESJoin(joinParams)
      .then((response) => {
        commit('SET_ENTITY_JOIN_PROCESSING', { entityID, isProcessing: false })

        RequestNotifications.dispatchRequestInfoNotification(
          dispatch,
          'Query created, redirecting...'
        )
        const stateHash = response.data.tiny_hash

        LinksToBrowsers.buildURLForEntityBrowserStateIDAndRedirect(
          destinationEntityID,
          stateHash
        )
      })
      .catch((error) => {
        commit('SET_ENTITY_JOIN_PROCESSING', { entityID, isProcessing: false })
        ErrorTracking.trackError(error, this)

        RequestNotifications.dispatchRequestErrorNotification(
          error,
          dispatch,
          'There was a problem while performing the join: '
        )
      })
  },
  setJoinMode({ commit, state, dispatch }, joinMode) {
    commit('SET_JOIN_MODE', joinMode)
  },
}

export default actions
