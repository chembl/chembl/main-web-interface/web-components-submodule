import ESProxyService from '@/web-components-submodule/services/ESProxyService.js'
import RequestNotifications from '@/web-components-submodule/utils/RequestNotifications.js'
import BaseMethods from '@/web-components-submodule/store/datasets/BaseStore/BaseMethods.js'
import Querystrings from '@/web-components-submodule/standardisation/Querystrings.js'
import FiltersActions from '@/web-components-submodule/store/datasets/BaseStore/FiltersActions.js'
import SelectionActions from '@/web-components-submodule/store/datasets/BaseStore/SelectionActions.js'
import EntitiesJoinActions from '@/web-components-submodule/store/datasets/BaseStore/EntitiesJoinActions.js'
import HeatmapActions from '@/web-components-submodule/store/datasets/BaseStore/HeatmapActions.js'
import FiltersViewNames from '~/web-components-submodule/standardisation/datasets/FiltersViewNames.js'
import ExportingStateActions from '~/web-components-submodule/store/datasets/BaseStore/ExportingStateActions.js'
import ErrorTracking from '~/web-components-submodule/tracking/ErrorTracking.js'
import GenericEventsTracking from '~/web-components-submodule/tracking/CustomEventsTracking.js'

const _ = require('lodash')

export default {
  getBaseActions() {
    return {
      async loadPropertiesConfiguration({ commit, state, dispatch }) {
        const indexName = state.indexName
        const propertiesGroups = state.propertiesGroups

        for (const [groupKey, currentGroup] of Object.entries(
          propertiesGroups
        )) {
          try {
            const { data } = await ESProxyService.getPopertyGroupConfiguration(
              indexName,
              currentGroup.id
            )

            const allHeaders = BaseMethods.parsePropertiesConfiguration(data)
            commit('SET_HEADERS_FOR_GROUP', { groupKey, allHeaders })
          } catch (error) {
            ErrorTracking.trackError(error, this)

            RequestNotifications.dispatchRequestErrorNotification(
              error,
              dispatch,
              `There was a problem loading the properties configuration for ${currentGroup.id} for index ${indexName}`
            )
          }
        }

        dispatch('createPropertiesIndex')
        BaseMethods.setCurrentTableHeaders({ commit, state, dispatch })
        commit('SET_INITIAL_DATA_LOADED')
        return true
      },
      createPropertiesIndex({ commit, state, dispatch }) {
        // create a properties index to be able to find the properties by their id
        const propertiesGroups = state.propertiesGroups

        const propertiesIndex = {}
        for (const groupKey in propertiesGroups) {
          const currentGroup = propertiesGroups[groupKey]
          const allHeaders = currentGroup.allHeaders
          for (const [headerKey, currentHeader] of Object.entries(allHeaders)) {
            propertiesIndex[headerKey] = currentHeader
          }
        }

        commit('SET_PROPERTIES_INDEX', propertiesIndex)
      },
      setInitialDataLoaded({ commit, state, dispatch }) {
        commit('SET_INITIAL_DATA_LOADED')
      },
      setInitialDataNotLoaded({ commit, state, dispatch }) {
        commit('SET_INITIAL_DATA_NOT_LOADED')
      },
      loadfacetsGroups({ commit, state, dispatch }) {
        const indexName = state.indexName
        const facetsGroups = state.facetsGroups

        ESProxyService.getFacetsGroupConfiguration(indexName, facetsGroups)
          .then((response) => {
            const facetsConfig = response.data.properties
            const allFacets = []
            const baseFacetConfig = {
              histogramParams: {},
              histogramData: {},
              histogramQuery: {},
              loadingHistogram: true,
              loadingRange: true,
              selectedRange: {},
              numericValuesData: {},
              specificTermsSelected: [],
              currentView: FiltersViewNames.histogram,
              locked: false,
              open: true,
            }
            for (const fConfigKey in facetsConfig.default) {
              const receivedConfig = facetsConfig.default[fConfigKey]
              const fConfig = {
                ...receivedConfig,
                show: true,
                key: receivedConfig.prop_id,
                ...baseFacetConfig,
              }

              allFacets.push(fConfig)
            }

            const initialFacetsState = state.initialFacetsState
            for (const fConfigKey in facetsConfig.optional) {
              const receivedConfig = facetsConfig.optional[fConfigKey]

              const initialState = _.find(
                initialFacetsState,
                (facet) => facet.key === receivedConfig.prop_id
              )

              const fConfig = {
                ...facetsConfig.optional[fConfigKey],
                show: initialState != null, // if the initial state is not null make sure it is shown regardless of the config
                key: receivedConfig.prop_id,
                ...baseFacetConfig,
              }
              allFacets.push(fConfig)
            }

            // Make sure to sort facets as set by config
            allFacets.sort(function (a, b) {
              return a.position - b.position
            })

            commit('SET_CURRENT_FACETS', allFacets)
            commit('SET_FACETS_LOADED', true)
          })
          .catch((error) => {
            ErrorTracking.trackError(error, this)

            RequestNotifications.dispatchRequestErrorNotification(
              error,
              dispatch,
              `There was an error while loading the items! `
            )
          })
      },
      async loadQuickSearchPriorities({ commit, state, dispatch }) {
        try {
          const entityID = state.entityID
          const searchPrioritiesResponse = await ESProxyService.getQuickSearchPriorities(
            entityID
          )
          const searchPriorities = searchPrioritiesResponse.data
          commit('SET_QUICK_SEARCH_PRIORITIES', searchPriorities)
          commit('SET_QUICK_SEARCH_PRIORITIES_LOADED', true)
          return true
        } catch (error) {
          ErrorTracking.trackError(error, this)

          RequestNotifications.dispatchRequestErrorNotification(
            error,
            dispatch,
            `There was a problem loading the properties priorities for the quick search.`
          )
          return false
        }
      },
      fireItUp({ commit, state, dispatch }, starterParams) {
        BaseMethods.fireItUp({ commit, state, dispatch }, starterParams)
      },
      loadCurrentPage({ commit, state, dispatch }) {
        BaseMethods.loadCurrentPage({ commit, state, dispatch })
      },
      loadNextPage({ commit, state, dispatch }) {
        BaseMethods.loadNextPage({ commit, state, dispatch })
      },
      loadPreviousPage({ commit, state, dispatch }) {
        BaseMethods.loadPreviousPage({ commit, state, dispatch })
      },
      setPage({ commit, state, dispatch }, pageNumber) {
        BaseMethods.setPage({ commit, state, dispatch }, pageNumber)
      },
      setPageSize({ commit, state, dispatch }, pageSize) {
        BaseMethods.setPageSize({ commit, state, dispatch }, pageSize)
      },
      setSortBy({ commit, state, dispatch }, sortParams) {
        commit('SET_SORT_BY', sortParams)
        dispatch('buildSortObject')

        GenericEventsTracking.trackDatasetSorted(
          this,
          state.entityID,
          sortParams
        )
      },
      setSortDesc({ commit, state, dispatch }, sortParams) {
        commit('SET_SORT_DESC', sortParams)
        dispatch('buildSortObject')
        BaseMethods.loadCurrentPage({ commit, state, dispatch })
      },
      buildSortObject({ commit, state, dispatch }) {
        const propertiesIndex = state.propertiesIndex
        const sortBy = state.sortBy
        const sortDesc = state.sortDesc
        const sort = []
        for (let i = 0; i < sortBy.length; i++) {
          const currentPropertyToSort = sortBy[i]
          const currentPropConfig = propertiesIndex[currentPropertyToSort]
          const valueAccessor =
            currentPropConfig == null
              ? currentPropertyToSort
              : currentPropConfig.is_virtual
              ? currentPropConfig.based_on
              : currentPropertyToSort
          const currentSortDirection = sortDesc[i] ? 'desc' : 'asc'
          sort.push({
            [valueAccessor]: currentSortDirection,
          })
        }

        const containsContextualProperties = sortBy.some((prop) => {
          // this happens when the sort property is not in the properties index, for example
          // the mechanisms of action in a compound report card are sorted by max phase, but this is not
          // dispaled in the table, so it is not in the properties index
          if (propertiesIndex[prop] == null) {
            return false
          }
          return propertiesIndex[prop].is_contextual
        })

        const sortObject = {
          sort,
          containsContextualProperties,
        }

        commit('SET_SORT_OBJECT', sortObject)
      },
      setCustomRepresentationParams(
        { commit, state, dispatch },
        customRepresentationParams
      ) {
        commit(
          'SET_POSSIBLE_PAGE_SIZES',
          customRepresentationParams.possiblePageSizes
        )
        commit('SET_ITEMS_PER_PAGE', customRepresentationParams.itemsPerPage)
      },
      setPluralEntityName({ commit, state, dispatch }, pluralEntityName) {
        commit('SET_PLURAL_ENTITY_NAME', pluralEntityName)
      },
      setEmbeddingIDForParent({ commit, state, dispatch }, idForParent) {
        commit('SET_EMBEDDING_ID_FOR_PARENT', idForParent)
      },
      addDownloadFormat({ commit, state, dispatch }, formatID) {
        commit('ADD_DOWNLOAD_FORMAT', formatID)
      },
      setEntityID({ commit, state, dispatch }, entityID) {
        commit('SET_ENTITY_ID', entityID)
      },
      setSearchTerm({ commit, state, dispatch }, searchTerm) {
        commit('SET_SEARCH_TERM', searchTerm)
      },
      setCurrentView({ commit, state, dispatch }, currentView) {
        commit('SET_CURRENT_VIEW', currentView)
      },
      setSubsetHumanDescription(
        { commit, state, dispatch },
        subsetHumanDescription
      ) {
        commit('SET_SUBSET_HUMAN_DESCRIPTION', subsetHumanDescription)
      },
      setDownloadPropertiesGroup(
        { commit, state, dispatch },
        downloadPropertiesGroup
      ) {
        commit('SET_DOWNLOAD_PROPERTIES_GROUP', downloadPropertiesGroup)
      },
      setPossibleJoinDestinations(
        { commit, state, dispatch },
        possibleJoinDestinations
      ) {
        commit('SET_POSSIBLE_JOIN_DESTINATIONS', possibleJoinDestinations)
      },
      setQuerystringExamples({ commit, state, dispatch }, querystringExamples) {
        commit('SET_QUERYSTRING_EXAMPLES', querystringExamples)
      },
      setEnableSelection({ commit, state, dispatch }, enableSelection) {
        commit('SET_ENABLE_SELECTION', enableSelection)
      },
      setEnableMultiSort({ commit, state, dispatch }, enableMultiSort) {
        commit('SET_ENABLE_MULTISORT', enableMultiSort)
      },
      setEnableSimilarityMaps(
        { commit, state, dispatch },
        enableSimilarityMaps
      ) {
        commit('SET_ENABLE_SIMILARITY_MAPS', enableSimilarityMaps)
      },
      setShowSimilarityMaps({ commit, state, dispatch }, showSimilarityMaps) {
        commit('SET_SHOW_SIMILARITY_MAPS', showSimilarityMaps)
      },
      setSimilarityMapsReferenceStructure(
        { commit, state, dispatch },
        similarityMapsReferenceStructure
      ) {
        commit(
          'SET_SIMILARITY_MAPS_REFERENCE_STRUCTURE',
          similarityMapsReferenceStructure
        )
      },
      setSubstructureHighlightReferenceStructure(
        { commit, state, dispatch },
        substructureHighlightReferenceStructure
      ) {
        commit(
          'SET_SUBSTRUCTURE_HIGHLIGHT_REFERENCE_STRUCTURE',
          substructureHighlightReferenceStructure
        )
      },
      setEnableSubstructureHighlight(
        { commit, state, dispatch },
        enableSubstructureHighlight
      ) {
        commit('SET_ENABLE_SUBSTRUCTURE_HIGHLIGHT', enableSubstructureHighlight)
      },

      setShowSubstructureHighlight(
        { commit, state, dispatch },
        showSubstructureHighlight
      ) {
        commit('SET_SHOW_SUBSTRUCTURE_HIGHLIGHT', showSubstructureHighlight)
      },
      setEnableStateSaving({ commit, state, dispatch }, enableStateSaving) {
        commit('SET_ENABLE_STATE_SAVING', enableStateSaving)
      },
      setEnableHeatmapGeneration(
        { commit, state, dispatch },
        enableHeatmapGeneration
      ) {
        commit('SET_ENABLE_HEATMAP_GENERATION', enableHeatmapGeneration)
      },
      setEnableQuickTextSearch(
        { commit, state, dispatch },
        enableQuickTextSearch
      ) {
        commit('SET_ENABLE_QUICK_TEXT_SEARCH', enableQuickTextSearch)
      },
      submitDownload({ commit, state, dispatch }, formatID) {
        BaseMethods.startDownload({ commit, state, dispatch }, formatID)
      },
      setQuerystring({ commit, state, dispatch }, querystring) {
        commit('SET_QUERYSTRING', querystring)
      },
      setQuerystringToApply({ commit, state, dispatch }, querystringToApply) {
        commit('SET_QUERYSTRING_TO_APPLY', querystringToApply)
      },
      loadMappingsForQuerystring({ commit, state, dispatch }) {
        BaseMethods.loadMappingsForQuerystring({ commit, state, dispatch })
      },
      applyQuerystring({ commit, state, dispatch }) {
        const querystringToApply = state.querystringToApply
        commit('SET_QUERYSTRING', querystringToApply)
        dispatch('clearAllselections')
        BaseMethods.setPage({ commit, state, dispatch }, 1)
        dispatch('reloadAllFilters')
        dispatch('getHashForCurrentState')
        GenericEventsTracking.trackDatasetCustomFilterApplied(
          this,
          state.entityID,
          querystringToApply
        )
      },
      resetQuerystring({ commit, state, dispatch }) {
        commit('SET_QUERYSTRING', Querystrings.DEFAULT_QUERYSTRING)
        commit('SET_QUERYSTRING_TO_APPLY', Querystrings.DEFAULT_QUERYSTRING)
        BaseMethods.setPage({ commit, state, dispatch }, 1)
        dispatch('unlockAllFilters')
        dispatch('clearAllFilters')
        dispatch('reloadAllFilters')
      },
      toggleColumn({ commit, state, dispatch }, columnKey) {
        commit('TOGGLE_COLUMN', columnKey)
        BaseMethods.setCurrentTableHeaders({ commit, state, dispatch })
        BaseMethods.loadCurrentPage({ commit, state, dispatch })
      },
      moveColumns({ commit, state, dispatch }, { originKey, destinationKey }) {
        commit('MOVE_COLUMNS', { originKey, destinationKey })
        BaseMethods.setCurrentTableHeaders({ commit, state, dispatch })
        BaseMethods.loadCurrentPage({ commit, state, dispatch })
      },
      changeCurrentPropertyGroup({ commit, state, dispatch }, groupKey) {
        BaseMethods.changeCurrentPropertyGroup(
          { commit, state, dispatch },
          groupKey
        )
      },
      toggleDebugModeAction({ commit, state, dispatch }) {
        commit('TOGGLE_DEBUG_MODE')
      },
      ...FiltersActions,
      ...SelectionActions,
      ...EntitiesJoinActions,
      ...ExportingStateActions,
      ...HeatmapActions,
    }
  },
}
