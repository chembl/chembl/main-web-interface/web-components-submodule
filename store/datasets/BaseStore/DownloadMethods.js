import RequestNotifications from '@/web-components-submodule/utils/RequestNotifications.js'
import SubmissionService from '~/web-components-submodule/services/downloads/SubmissionService.js'
import DJCommon from '~/web-components-submodule/services/DJCommon.js'
import ErrorTracking from '~/web-components-submodule/tracking/ErrorTracking.js'

const methods = {
  startDownload({ commit, state, dispatch }, formatID) {
    commit('SET_DOWNLOAD_FORMAT_PROCESSING', { formatID, isProcessing: true })

    const downloadParams = {
      indexName: state.indexName,
      query: state.query.query,
      format: formatID,
      download_columns_group: state.downloadPropertiesGroup,
      dl__ignore_cache: process.env.delayedJobsIgnoreCache,
      context_obj: state.contextObj,
    }
    SubmissionService.submitDownloadJob(downloadParams)
      .then((response) => {
        const jobID = response.data.job_id

        const newDownload = {
          id: jobID,
          fileType: formatID,
          linkToRawDetails: DJCommon.getJobStatusUrl(jobID),
          title: `${formatID} File Generation Job Submitted...`,
          status: 'SUBMITTED',
          progress: '0',
        }

        commit('UPDATE_DATASET_DOWNLOAD_PROPS', newDownload)

        DJCommon.checkJobUntilDone(
          { commit, state, dispatch },
          jobID,
          methods.jobStatusCallback,
          methods.jobErrorCallback
        )
      })
      .catch((error) => {
        commit('SET_DOWNLOAD_FORMAT_PROCESSING', {
          formatID,
          isProcessing: false,
        })
        ErrorTracking.trackError(error, this)

        RequestNotifications.dispatchRequestErrorNotification(
          error,
          dispatch,
          'There was a problem submitting the download: '
        )
      })
  },
  jobStatusCallback(storeParams, jobData) {
    const status = jobData.status
    const fileFormat = JSON.parse(jobData.raw_params).format
    let title = ''
    let fileUrl

    const lastTimeChecked = new Date()

    const hours = String(lastTimeChecked.getHours()).padStart(2, '0')
    const minutes = String(lastTimeChecked.getMinutes()).padStart(2, '0')
    const seconds = String(lastTimeChecked.getSeconds()).padStart(2, '0')

    const timeString = `Last updated at ${hours}:${minutes}:${seconds}`

    if (status !== 'FINISHED') {
      title = `Generating ${fileFormat} file... ${timeString}`
    } else {
      title = `${fileFormat} Generated!`
      fileUrl = `https://${
        jobData.output_files_urls[Object.keys(jobData.output_files_urls)[0]]
      }`
    }

    const newDownloadParams = {
      id: jobData.id,
      title,
      status,
      fileUrl,
      progress: jobData.progress,
    }

    storeParams.commit('UPDATE_DATASET_DOWNLOAD_PROPS', newDownloadParams)

    if (status === 'FINISHED') {
      storeParams.commit('SET_DOWNLOAD_FORMAT_PROCESSING', {
        formatID: fileFormat,
        isProcessing: false,
      })
    }
  },
  jobErrorCallback(storeParams, error) {
    storeParams.commit('SET_DOWNLOAD_FORMAT_PROCESSING', {
      formatID: storeParams.formatID,
      isProcessing: false,
    })
  },
}

export default methods
