import SelectionModes from '@/web-components-submodule/standardisation/datasets/SelectionModes.js'
import EntitiesJoin from '~/web-components-submodule/standardisation/datasets/EntitiesJoin.js'

const actions = {
  setAllSelected({ commit, state, dispatch }, value) {
    const newSelectionMode = value
      ? SelectionModes.allItemsExcept
      : SelectionModes.noItemsExcept
    commit('SET_SELECTION_MODE', newSelectionMode)
    commit('RESET_SELECTION_EXCEPTIONS')
    dispatch('calculateNumSelected')
    dispatch('calculateSelectedInPage')
    dispatch('setJoinMode', 'all_items')
  },
  setItemSelected({ commit, state, dispatch }, { id, value }) {
    const selectionMode = state.selection.selectionMode
    const selectingAllExcept = selectionMode === SelectionModes.allItemsExcept
    const mustBeInExceptions = value !== selectingAllExcept
    // const itemInExceptions = state.selection.exceptions[id] !== null
    // const mustAddToExceptions = value !==
    if (mustBeInExceptions) {
      commit('ADD_ITEM_TO_SELECTION_EXCEPTIONS', id)
    } else {
      commit('REMOVE_ITEM_FROM_SELECTION_EXCEPTIONS', id)
    }
    dispatch('calculateNumSelected')
    dispatch('calculateSelectedInPage')
    const newJoinMode = state.selection.someItemsSelected
      ? EntitiesJoin.JoinModes.selection
      : EntitiesJoin.JoinModes.allItems
    dispatch('setJoinMode', newJoinMode)
  },
  calculateNumSelected({ commit, state, dispatch }) {
    const selectionMode = state.selection.selectionMode
    const totalItems = state.totalItems
    const selectionExceptions = state.selection.exceptions

    const numItemsSelected =
      selectionMode === SelectionModes.allItemsExcept
        ? totalItems - selectionExceptions.length
        : selectionExceptions.length

    commit('SET_NUM_SELECTED_ITEMS', numItemsSelected)
    const allItemsSelected = numItemsSelected === totalItems
    commit('SET_ALL_ITEMS_SELECTED', allItemsSelected)
    const someItemsSelected =
      numItemsSelected > 0 && numItemsSelected < totalItems
    commit('SET_SOME_ITEMS_SELECTED', someItemsSelected)
  },
  calculateSelectedInPage({ commit, state, dispatch }) {
    const itemsInPage = state.currentItems
    const selectionExceptions = state.selection.exceptions
    const selectionMode = state.selection.selectionMode
    let selectedInPage
    if (selectionMode === SelectionModes.allItemsExcept) {
      selectedInPage = itemsInPage.filter(
        (item) => !selectionExceptions.includes(item.id)
      )
    } else {
      selectedInPage = itemsInPage.filter((item) =>
        selectionExceptions.includes(item.id)
      )
    }
    commit('SET_SELECTED_IN_PAGE', selectedInPage)
  },
  clearAllselections({ commit, state, dispatch }) {
    commit('SET_SELECTION_MODE', SelectionModes.noItemsExcept)
    commit('RESET_SELECTION_EXCEPTIONS')
    dispatch('calculateNumSelected')
    dispatch('calculateSelectedInPage')
    dispatch('setJoinMode', 'all_items')
  },
}

export default actions
