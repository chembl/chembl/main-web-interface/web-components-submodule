import SearchResultsItemsComponents from '~/web-components-submodule/standardisation/SearchResultsItemsComponents.js'

const methods = {
  async fireItUp({ commit, state, dispatch }, initialParams) {
    commit('SET_INDEX_NAME', initialParams.indexName)
    commit('SET_INITIAL_QUERY', initialParams.initialQuery)
    commit('SET_QUERY', initialParams.initialQuery)
    // This comes initialised if included in the initialParams from the state
    commit('SET_PROPERTIES_GROUPS', initialParams.propertiesGroups)
    commit('SET_CONTEXT_OBJ', initialParams.contextObj)

    const searchResultsItemsComponentName =
      SearchResultsItemsComponents[state.entityID]

    commit('SET_SEARCH_RESULTS_ITEM', searchResultsItemsComponentName)

    if (initialParams.initialSort != null) {
      commit('SET_SORT_BY', initialParams.initialSort)

      if (initialParams.initialSortDesc != null) {
        commit('SET_SORT_DESC', initialParams.initialSortDesc)
      }
    }

    if (initialParams.customFiltering != null) {
      commit('SET_QUERYSTRING', initialParams.customFiltering)
      commit('SET_QUERYSTRING_TO_APPLY', initialParams.customFiltering)
    }

    if (initialParams.subsetHumanDescription != null) {
      commit(
        'SET_SUBSET_HUMAN_DESCRIPTION',
        initialParams.subsetHumanDescription
      )
    }

    // if the initial facets state is provided set it so it can be used for the first load
    if (initialParams.initialFacetsState != null) {
      const initialFacetsState = initialParams.initialFacetsState.map(
        (filter) => {
          return {
            ...filter,
            consumed: false,
            consumedForFirstPageLoad: false,
          }
        }
      )
      commit('SET_INITIAL_FACETS_STATE', initialFacetsState)
    } else {
      commit('SET_INITIAL_FACETS_STATE', [])
    }

    if (initialParams.quickSearchTerm != null) {
      commit('SET_QUICK_SEARCH_TERM', initialParams.quickSearchTerm)
    }

    if (initialParams.exactTextFilters != null) {
      commit(
        'SET_EXACT_TEXT_FILTERS_FROM_STATE',
        initialParams.exactTextFilters
      )
    }

    const propertiesConfigurationLoaded = await dispatch(
      'loadPropertiesConfiguration'
    )
    const facetsGroups = initialParams.facetsGroups
    if (facetsGroups != null) {
      commit('SET_FACETS_GROUPS', facetsGroups)
      dispatch('loadfacetsGroups')
    }
    const enableQuickTextSearch = state.enableQuickTextSearch
    let allReadyForFirstPageLoad =
      propertiesConfigurationLoaded && !enableQuickTextSearch
    if (allReadyForFirstPageLoad) {
      dispatch('loadCurrentPage')
      return true
    }

    const quickSearchPrioritiesLoaded = await dispatch(
      'loadQuickSearchPriorities'
    )
    allReadyForFirstPageLoad = quickSearchPrioritiesLoaded

    if (allReadyForFirstPageLoad) {
      dispatch('loadCurrentPage')
      return true
    }
  },
  parsePropertiesConfiguration(receivedConfiguration) {
    const allHeaders = {}
    const defaultRawProperties = receivedConfiguration.properties.default || []
    methods.addRawHeadersToAllHeaders(
      allHeaders,
      defaultRawProperties,
      'default'
    )
    const optionalRawProperties =
      receivedConfiguration.properties.optional || []
    methods.addRawHeadersToAllHeaders(
      allHeaders,
      optionalRawProperties,
      'optional'
    )

    const optionalStickyProperties =
      receivedConfiguration.properties.sticky || []
    methods.addRawHeadersToAllHeaders(
      allHeaders,
      optionalStickyProperties,
      'sticky'
    )

    return allHeaders
  },
  addRawHeadersToAllHeaders(allHeaders, rawProperties, propsType) {
    const show = propsType === 'default'

    let currentPosition = Object.keys(allHeaders).length + 1
    for (let i = 0; i < rawProperties.length; i++) {
      const currentRawProperty = rawProperties[i]
      const id = currentRawProperty.prop_id
      const alreadyAdded = allHeaders[id] != null
      if (alreadyAdded) {
        continue
      }

      const parsedConfig = {
        id,
        text: currentRawProperty.label,
        sortable: currentRawProperty.sortable,
        value: currentRawProperty.prop_id,
        position: currentPosition,
        show,
        compulsoriness: propsType,
        ...currentRawProperty,
      }

      allHeaders[parsedConfig.id] = parsedConfig
      currentPosition += 1
    }
  },
  setCurrentTableHeaders({ commit, state, dispatch }) {
    const propertiesGroups = state.propertiesGroups
    const selectedGroupKey = methods.getSelectedPropertyGroupKey(
      propertiesGroups
    )
    const selectedHeaders = state.propertiesGroups[selectedGroupKey].allHeaders
    const visibleHeaders = Object.values(selectedHeaders).filter(
      (header) => header.show
    )
    visibleHeaders.sort((headerA, headerB) => {
      if (headerA.position > headerB.position) {
        return 1
      }
      if (headerA.position < headerB.position) {
        return -1
      }
      return 0
    })
    const sanitisedHeaders = methods.sanitiseTableHeaders(visibleHeaders)
    commit('SET_CURRENT_HEADERS', sanitisedHeaders)
  },
  changeCurrentPropertyGroup({ commit, state, dispatch }, groupKey) {
    commit('SET_CURRENT_PROPERTY_GROUP', groupKey)
    methods.setCurrentTableHeaders({ commit, state, dispatch })
    dispatch('loadCurrentPage')
  },
  getSelectedPropertyGroupKey(propertiesGroups) {
    for (const [groupKey, currentProperty] of Object.entries(
      propertiesGroups
    )) {
      if (currentProperty.selected) {
        return groupKey
      }
    }
  },
  sanitiseTableHeaders(tableHeaders) {
    const sanitisedHeaders = []
    for (let i = 0; i < tableHeaders.length; i++) {
      const currentHeader = tableHeaders[i]
      const sanitisedHeader = {
        text: currentHeader.text,
        sortable: currentHeader.sortable,
        value: currentHeader.value,
      }
      sanitisedHeaders.push(sanitisedHeader)
    }
    return sanitisedHeaders
  },
}

export default methods
