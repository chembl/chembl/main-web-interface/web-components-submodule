import RequestNotifications from '@/web-components-submodule/utils/RequestNotifications.js'
import JoinModes from '@/web-components-submodule/standardisation/datasets/JoinModes.js'
import SelectionModes from '@/web-components-submodule/standardisation/datasets/SelectionModes.js'
import LinksToHeatmap from '~/web-components-submodule/standardisation/LinksToHeatmap.js'
import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'
import EntitiesJoin from '~/web-components-submodule/standardisation/datasets/EntitiesJoin.js'
import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'

const methods = {
  getYAxisQuery({ commit, state, dispatch }) {
    const yAxisBaseQuery = JSON.parse(JSON.stringify(state.query))
    const yAxisSelection = state.selection

    const numItemsSelected = yAxisSelection.numItemsSelected
    if (numItemsSelected > 0) {
      const selectionMode = yAxisSelection.selectionMode
      if (selectionMode === SelectionModes.allItemsExcept) {
        yAxisBaseQuery.query.bool.must_not = [
          {
            ids: {
              values: yAxisSelection.exceptions,
            },
          },
        ]
      } else {
        yAxisBaseQuery.query.bool.filter = [
          {
            ids: {
              values: yAxisSelection.exceptions,
            },
          },
        ]
      }
    }

    return yAxisBaseQuery
  },
  getXAxisQuery({ commit, state, dispatch }) {
    let selectionDescription
    if (state.joinMode === JoinModes.allItems) {
      selectionDescription = {
        selectionMode: 'allItemsExcept',
        exceptions: [],
      }
    } else {
      selectionDescription = {
        selectionMode: state.selection.selectionMode,
        exceptions: state.selection.exceptions,
      }
    }

    const yAxisEntityID = state.entityID
    const xAxisEntityID =
      yAxisEntityID === EntityNames.Target.entityID
        ? EntityNames.Compound.entityID
        : EntityNames.Target.entityID

    const yAxisQuery = methods.getYAxisQuery({ commit, state, dispatch })

    const joinParams = {
      entityFrom: EntitiesJoin.getOriginParamName(yAxisEntityID),
      entityTo: EntitiesJoin.getDestinationParamName(
        yAxisEntityID,
        xAxisEntityID
      ),
      query: yAxisQuery,
      selectionDescription,
      context_obj: state.contextObj,
    }

    return new Promise((resolve, reject) => {
      ESProxyService.getQueryForESJoin(joinParams)
        .then((response) => {
          const query = response.data.query
          resolve(query)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
}

const actions = {
  triggerHeatmapGeneration({ commit, state, dispatch, rootState }) {
    commit('SET_GENERATING_HEATMAP_URL', true)

    // if yAxisEntityID is drugs, the yAxisEntityID is compound.
    const yAxisEntityID =
      state.entityID === EntityNames.Drug.entityID
        ? EntityNames.Compound.entityID
        : state.entityID
    // This is intended to generate heatmaps between compounds and targets, joined by activities.
    // If other heatmaps are needed, this should be refactored to be more flexible.
    const xAxisEntityID =
      yAxisEntityID === EntityNames.Target.entityID
        ? EntityNames.Compound.entityID
        : EntityNames.Target.entityID
    const yAxisQuery = methods.getYAxisQuery({ commit, state, dispatch })

    methods
      .getXAxisQuery({ commit, state, dispatch })
      .then((xAxisQuery) => {
        const generationParams = {
          xAxis: {
            entityID: xAxisEntityID,
            query: xAxisQuery,
          },
          yAxis: {
            entityID: yAxisEntityID,
            query: yAxisQuery,
          },
        }

        LinksToHeatmap.buildURLForHeatmap(generationParams)
          .then((url) => {
            commit('SET_GENERATING_HEATMAP_URL', false)
            window.location.href = url
          })
          .catch((error) => {
            RequestNotifications.dispatchRequestErrorNotification(
              error,
              dispatch,
              `There was an error while getting the heatmap ID. ${error}`
            )
          })
      })
      .catch((error) => {
        RequestNotifications.dispatchRequestErrorNotification(
          error,
          dispatch,
          `There was an error while joining the items of ${yAxisEntityID} with ${xAxisEntityID} . ${error}`
        )
      })
  },
}

export default actions
