import RequestNotifications from '@/web-components-submodule/utils/RequestNotifications.js'
import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'
import FiltersViewNames from '~/web-components-submodule/standardisation/datasets/FiltersViewNames.js'
import ErrorTracking from '~/web-components-submodule/tracking/ErrorTracking.js'
import GenericEventsTracking from '~/web-components-submodule/tracking/CustomEventsTracking.js'
import ObjectPropertyAccess from '~/web-components-submodule/utils/ObjectPropertyAccess.js'
import FiltersInitialParameters from '~/web-components-submodule/standardisation/FiltersInitialParameters.js'

const _ = require('lodash')

const defaults = {
  termsHistogramsDefaults: {
    minBars: 2,
    defaultNumBars: 10,
    numBars: 10,
    maxBars: 30,
    defaultSort: 'desc',
    sort: 'desc',
    defaultSortBy: '_count',
    sortBy: '_count',
    defaultNullValuesLabel: 'N/A',
    nullValuesLabel: 'N/A',
  },
  numericHistogramsDefaults: {
    defaultNullValuesLabel: 'N/A',
    nullValuesLabel: 'N/A',
    mergeEmptyRanges: true,
  },
}

const helperMethods = {
  calculateIntervalRange(minValue, maxValue, propType) {
    const minNumBarsDesired = defaults.termsHistogramsDefaults.minBars
    const maxNumBarsDesired = defaults.termsHistogramsDefaults.maxBars
    const defaultBarsDesired = defaults.termsHistogramsDefaults.defaultNumBars
    const rangeSize = maxValue - minValue
    if (['integer', 'double'].includes(propType)) {
      const maxInterval = Math.ceil(rangeSize / minNumBarsDesired)
      const minInterval = Math.ceil(rangeSize / maxNumBarsDesired)
      const defaultInterval = Math.ceil(rangeSize / defaultBarsDesired)

      return {
        minInterval,
        maxInterval,
        defaultInterval,
        interval: defaultInterval > 0 ? defaultInterval : 1,
      }
    }
  },
  getParsedNumericBuckets(rawBuckets, maxValue, propType) {
    const parsedBuckets = []
    for (let i = 0; i < rawBuckets.length; i++) {
      const rawBucket = rawBuckets[i]

      let keyForHumans
      let minRangeKey
      let maxRangeKey

      // if not the last one
      if (i < rawBuckets.length - 1) {
        minRangeKey = rawBucket.key
        const nextRawBucket = rawBuckets[i + 1]
        maxRangeKey = nextRawBucket.key

        // this is the case for properties such as max phase, it is an integer and the difference between the
        // range bounds is 1. Max phase [0,1) becomes 0, [1,2) becomes 1
        if (propType === 'integer' && maxRangeKey - minRangeKey === 1) {
          keyForHumans = `${minRangeKey}`
        } else {
          keyForHumans = `[${minRangeKey},${maxRangeKey})`
        }
      } else {
        minRangeKey = rawBucket.key
        maxRangeKey = maxValue

        if (minRangeKey === maxRangeKey) {
          keyForHumans = `${minRangeKey}`
        } else {
          keyForHumans = `[${minRangeKey},${maxRangeKey}]`
        }
      }

      const parsedBucket = {
        key: `${rawBucket.key}`,
        key_for_humans: keyForHumans,
        doc_count: rawBucket.doc_count,
        selected: false,
        minRangeKey,
        maxRangeKey,
      }

      parsedBuckets.push(parsedBucket)
    }
    return parsedBuckets
  },
  getFilterInitialState(filterKey, state) {
    return state.initialFacetsState
      .filter((filter) => !filter.consumed)
      .find((filter) => filter.prop_id === filterKey)
  },
  loadFilterFromInitialState({ filterKey, initialState }, { state, commit }) {
    commit('REPLACE_FACET_STATE', { filterKey, facetState: initialState })
    commit('SET_INITIAL_FACETS_STATE_WAS_CONSUMED', {
      filterKey,
      wasConsumed: true,
    })
  },
  processCustomBucketSorting(rawBuckets, histogramParams) {
    const customBucketSortFunction = histogramParams.customBucketSortFunction
    if (customBucketSortFunction == null) {
      return
    }

    rawBuckets.sort(customBucketSortFunction)
  },
  processOtherBucket(rawBuckets, otherDocCount) {
    if (otherDocCount > 0) {
      rawBuckets.push({
        key: 'Other',
        doc_count: otherDocCount,
      })
    }
  },
  processBucketsKeys(rawBuckets, propType) {
    rawBuckets.forEach((bucket) => {
      bucket.selected = false
      bucket.key_for_humans =
        propType === 'boolean' ? (bucket.key === 1 ? 'Yes' : 'No') : bucket.key
    })
  },
  processBooleanBuckets(rawBuckets) {
    // For boolean properties, put always Yes before No in the buckets
    rawBuckets.sort((a, b) => {
      if (a.key_for_humans === 'Yes' && b.key_for_humans === 'No') {
        return -1 // a should come before b
      } else if (a.key_for_humans === 'No' && b.key_for_humans === 'Yes') {
        return 1 // b should come before a
      }
      return 0 // no need to change the order
    })
  },
}

const actions = {
  setFilterConfigAndLoad(
    { commit, state, dispatch },
    { filterKey, customConfig, currentView }
  ) {
    commit('UPDATE_FILTER_CURRENT_VIEW', { filterKey, currentView })
    const currentFilter = _.find(state.currentFacets, { key: filterKey })
    const propType = currentFilter.property_config.type
    if (['string', 'boolean'].includes(propType)) {
      const superDefaultHistogramParams = defaults.termsHistogramsDefaults

      const defaultHistogramParams = FiltersInitialParameters.methods.getInitialHistogramParamsForFilter(
        state.entityID,
        currentFilter.prop_id
      )

      const customHistogramParams = customConfig == null ? {} : customConfig

      // this assignment sets the precedence of the final value of the hisogram params
      const histogramParams = {
        ...superDefaultHistogramParams,
        ...defaultHistogramParams,
        ...customHistogramParams,
      }

      commit('UPDATE_FILTER_HISTOGRAM_PARAMS', { filterKey, histogramParams })
      dispatch('loadFilter', { filterKey })
    }
    if (['integer', 'double'].includes(propType)) {
      dispatch('loadFilter', { filterKey, customConfig })
    }
  },
  loadFilter({ commit, state, dispatch }, { filterKey, customConfig }) {
    const initialState = helperMethods.getFilterInitialState(filterKey, state)
    if (initialState != null) {
      helperMethods.loadFilterFromInitialState(
        { filterKey, initialState },
        { state, commit }
      )
    } else {
      const currentFilter = _.find(state.currentFacets, { key: filterKey })
      const propType = currentFilter.property_config.type
      if (['string', 'boolean'].includes(propType)) {
        if (currentFilter.currentView === FiltersViewNames.histogram) {
          dispatch('loadTermsFilter', { filterKey, customConfig, propType })
        }
        // The find term filter does not require to be loaded
      } else if (['integer', 'double'].includes(propType)) {
        if (currentFilter.currentView === FiltersViewNames.histogram) {
          dispatch('loadNumericFilter', { filterKey, customConfig })
        } else if (currentFilter.currentView === FiltersViewNames.range) {
          dispatch('loadRangeFilter', { filterKey, customConfig })
        }
      }
    }
  },
  loadNumericFilter({ commit, state, dispatch }, { filterKey, customConfig }) {
    const currentFilter = _.find(state.currentFacets, { key: filterKey })
    const currentDatasetQuery = state.query
    const field = currentFilter.property_config.prop_id
    const minAggName = `min..${field}`
    const maxAggName = `max..${field}`
    const minMaxQuery = {
      size: 0,
      query: currentDatasetQuery.query,
      aggs: {
        [minAggName]: { min: { field } },
        [maxAggName]: { max: { field } },
      },
    }
    const indexName = state.indexName
    const contextObj = state.contextObj
    ESProxyService.getESData(indexName, minMaxQuery, contextObj)
      .then((response) => {
        const minValue =
          response.data.es_response.aggregations[minAggName].value
        const maxValue =
          response.data.es_response.aggregations[maxAggName].value

        const numericValuesData = {
          minValue,
          maxValue,
        }

        commit('UPDATE_FILTER_NUMERIC_VALUES_PARAMS', {
          filterKey,
          numericValuesData,
        })

        const propType = currentFilter.property_config.type
        const intervalRange = helperMethods.calculateIntervalRange(
          minValue,
          maxValue,
          propType
        )

        const oldHistogramParams = currentFilter.histogramParams

        const histogramParams = {
          ...oldHistogramParams,
          ...defaults.numericHistogramsDefaults,
          ...intervalRange,
          ...customConfig,
        }

        const mergeEmptyRangesServerConfig = ObjectPropertyAccess.getPropertyPalue(
          currentFilter,
          'server_initial_config.agg_params.merge_empty_ranges',
          undefined,
          true
        )

        if (mergeEmptyRangesServerConfig != null) {
          histogramParams.mergeEmptyRanges = mergeEmptyRangesServerConfig
        }
        commit('UPDATE_FILTER_HISTOGRAM_PARAMS', { filterKey, histogramParams })
        dispatch('loadNumericFilterData', filterKey)
      })
      .catch((error) => {
        ErrorTracking.trackError(error, this)

        RequestNotifications.dispatchRequestErrorNotification(
          error,
          dispatch,
          `There was a problem loading minimun and maximum values for ${field}`
        )
      })
  },
  loadNumericFilterData({ commit, state, dispatch }, filterKey) {
    const currentFilter = _.find(state.currentFacets, { key: filterKey })
    const aggName = currentFilter.property_config.prop_id
    const currentDatasetQuery = state.query
    const field = currentFilter.property_config.prop_id
    const interval = currentFilter.histogramParams.interval
    const minDocCount = currentFilter.histogramParams.mergeEmptyRanges ? 1 : 0

    const aggQuery = {
      size: 0,
      query: currentDatasetQuery.query,
      aggs: {
        [aggName]: {
          histogram: {
            field,
            interval,
            min_doc_count: minDocCount,
          },
        },
      },
    }

    commit('SET_LOADING_FILTER_HISTOGRAM_DATA', {
      filterKey,
      loadingHistogram: true,
    })

    commit('UPDATE_FILTER_HISTOGRAM_QUERY', {
      filterKey,
      histogramQuery: aggQuery,
    })

    const indexName = state.indexName
    const contextObj = state.contextObj
    ESProxyService.getESData(indexName, aggQuery, contextObj)
      .then((response) => {
        const histogramData = response.data.es_response.aggregations[aggName]

        const propType = currentFilter.property_config.type

        const parsedBuckets = helperMethods.getParsedNumericBuckets(
          histogramData.buckets,
          currentFilter.numericValuesData.maxValue,
          propType
        )

        const parsedHistogramData = {
          buckets: parsedBuckets,
        }

        commit('UPDATE_FILTER_HISTOGRAM_DATA', {
          filterKey,
          histogramData: parsedHistogramData,
        })

        dispatch('loadNumericFilterNullValues', filterKey)
      })
      .catch((error) => {
        const field = currentFilter.property_config.prop_id
        ErrorTracking.trackError(error, this)

        RequestNotifications.dispatchRequestErrorNotification(
          error,
          dispatch,
          `There was an error while loading the histogram data for ${field}`
        )
      })
  },
  loadNumericFilterNullValues({ commit, state, dispatch }, filterKey) {
    const currentFilter = _.find(state.currentFacets, { key: filterKey })
    const field = currentFilter.property_config.prop_id

    const currentDatasetQuery = _.cloneDeep(state.query)

    const nullValuesQuery = {
      size: 0,
      track_total_hits: true,
      query: {
        bool: {
          must_not: [],
        },
        ...currentDatasetQuery.query,
      },
    }
    if (nullValuesQuery.query.bool.must_not == null) {
      nullValuesQuery.query.bool.must_not = []
    }

    nullValuesQuery.query.bool.must_not.push({
      exists: {
        field,
      },
    })

    commit('SET_LOADING_FILTER_HISTOGRAM_DATA', {
      filterKey,
      loadingHistogram: true,
    })

    const indexName = state.indexName
    const contextObj = state.contextObj
    ESProxyService.getESData(indexName, nullValuesQuery, contextObj)
      .then((response) => {
        const nullValuesCount = response.data.es_response.hits.total.value
        const nullValuesLabel = currentFilter.histogramParams.nullValuesLabel
        const histogramData = _.cloneDeep(currentFilter.histogramData)

        histogramData.buckets.splice(0, 0, {
          key: nullValuesLabel,
          key_for_humans: nullValuesLabel,
          doc_count: nullValuesCount,
          selected: false,
        })

        commit('UPDATE_FILTER_HISTOGRAM_DATA', {
          filterKey,
          histogramData,
        })

        commit('SET_LOADING_FILTER_HISTOGRAM_DATA', {
          filterKey,
          loadingHistogram: false,
        })
      })
      .catch((error) => {
        const field = currentFilter.property_config.prop_id
        ErrorTracking.trackError(error, this)

        RequestNotifications.dispatchRequestErrorNotification(
          error,
          dispatch,
          `There was an error while loading the null values for ${field}`
        )
      })
  },
  loadTermsFilter({ commit, state, dispatch }, { filterKey, propType }) {
    const currentFilter = _.find(state.currentFacets, { key: filterKey })
    const aggName = currentFilter.property_config.prop_id
    const field = currentFilter.property_config.prop_id
    const size = currentFilter.histogramParams.numBars
    const missing = currentFilter.histogramParams.nullValuesLabel
    const sortBy = currentFilter.histogramParams.sortBy

    const currentDatasetQuery = state.query
    const aggQuery = {
      size: 0,
      query: currentDatasetQuery.query,
      aggs: {
        [aggName]: {
          terms: {
            field,
            size,
          },
        },
      },
    }
    if (propType !== 'boolean') {
      const sort = currentFilter.histogramParams.sort
      aggQuery.aggs[aggName].terms.missing = missing
      aggQuery.aggs[aggName].terms.order = { [sortBy]: sort }
    }

    commit('SET_LOADING_FILTER_HISTOGRAM_DATA', {
      filterKey,
      loadingHistogram: true,
    })

    commit('UPDATE_FILTER_HISTOGRAM_QUERY', {
      filterKey,
      histogramQuery: aggQuery,
    })

    const indexName = state.indexName
    const contextObj = state.contextObj
    ESProxyService.getESData(indexName, aggQuery, contextObj)
      .then((response) => {
        const histogramData = response.data.es_response.aggregations[aggName]

        if (propType !== 'boolean') {
          const otherDocCount =
            response.data.es_response.aggregations[aggName].sum_other_doc_count
          helperMethods.processOtherBucket(histogramData.buckets, otherDocCount)
          const histogramParams = currentFilter.histogramParams
          helperMethods.processCustomBucketSorting(
            histogramData.buckets,
            histogramParams
          )
        }

        helperMethods.processBucketsKeys(histogramData.buckets, propType)

        if (propType === 'boolean') {
          helperMethods.processBooleanBuckets(histogramData.buckets)
        }

        commit('UPDATE_FILTER_HISTOGRAM_DATA', { filterKey, histogramData })
        commit('SET_LOADING_FILTER_HISTOGRAM_DATA', {
          filterKey,
          loadingHistogram: false,
        })
      })
      .catch((error) => {
        const field = currentFilter.property_config.prop_id
        ErrorTracking.trackError(error, this)

        RequestNotifications.dispatchRequestErrorNotification(
          error,
          dispatch,
          `There was an error while loading the histogram data for ${field}`
        )
      })
  },
  loadRangeFilter({ commit, state, dispatch }, { filterKey }) {
    const currentFilter = _.find(state.currentFacets, { key: filterKey })
    const currentDatasetQuery = state.query
    const field = currentFilter.property_config.prop_id
    const minAggName = `min..${field}`
    const maxAggName = `max..${field}`
    const minMaxQuery = {
      size: 0,
      query: currentDatasetQuery.query,
      aggs: {
        [minAggName]: { min: { field } },
        [maxAggName]: { max: { field } },
      },
    }
    const indexName = state.indexName

    commit('SET_LOADING_FILTER_RANGE_DATA', {
      filterKey,
      loadingRange: true,
    })

    const contextObj = state.contextObj
    ESProxyService.getESData(indexName, minMaxQuery, contextObj)
      .then((response) => {
        const minValue =
          response.data.es_response.aggregations[minAggName].value
        const maxValue =
          response.data.es_response.aggregations[maxAggName].value

        const numericValuesData = {
          minValue,
          maxValue,
        }

        commit('UPDATE_FILTER_NUMERIC_VALUES_PARAMS', {
          filterKey,
          numericValuesData,
        })

        const selectedRange = {
          minSelection: minValue,
          maxSelection: maxValue,
        }

        commit('UPDATE_SELECTED_RANGE_DATA', {
          filterKey,
          selectedRange,
        })

        commit('SET_LOADING_FILTER_RANGE_DATA', {
          filterKey,
          loadingRange: false,
        })
      })
      .catch((error) => {
        ErrorTracking.trackError(error, this)

        RequestNotifications.dispatchRequestErrorNotification(
          error,
          dispatch,
          `There was a problem loading range for ${field}`
        )
      })
  },
  toggleTerm({ commit, state, dispatch }, { filterKey, term }) {
    commit('TOGGLE_FILTER_TERM', { filterKey, term })
    commit('SET_PAGE', 1)
    dispatch('loadCurrentPage')
    dispatch('clearAllselections')
    dispatch('reloadAllFilters')
    dispatch('getHashForCurrentState')
    GenericEventsTracking.trackDatasetTermFiltered(
      this,
      state.entityID,
      filterKey,
      term
    )
  },
  toggleFilter({ commit, state, dispatch }, filterKey) {
    commit('TOGGLE_FILTER', filterKey)
  },
  moveFilters({ commit, state, dispatch }, { originKey, destinationKey }) {
    commit('MOVE_FILTERS', { originKey, destinationKey })
  },
  selectRange({ commit, state, dispatch }, { filterKey, range }) {
    commit('TOGGLE_FILTER_RANGE', { filterKey, range })
    commit('SET_PAGE', 1)
    dispatch('loadCurrentPage')
    dispatch('clearAllselections')
    dispatch('reloadAllFilters')
    dispatch('getHashForCurrentState')
    GenericEventsTracking.trackDatasetSpecificTermFiltered(
      this,
      state.entityID,
      filterKey,
      range
    )
  },
  selectSpecificTerms({ commit, state, dispatch }, { filterKey, terms }) {
    commit('SELECT_SPECIFIC_TERMS', { filterKey, terms })
    commit('SET_PAGE', 1)
    dispatch('loadCurrentPage')
    dispatch('clearAllselections')
    dispatch('reloadAllFilters')
    dispatch('getHashForCurrentState')
    GenericEventsTracking.trackDatasetSpecificTermFiltered(
      this,
      state.entityID,
      filterKey,
      terms
    )
  },
  clearFiltersSelection({ commit, state, dispatch }, filterKey) {
    commit('CLEAR_FILTERS_SELECTION', filterKey)
    commit('SET_PAGE', 1)
    dispatch('loadCurrentPage')
    dispatch('clearAllselections')
    dispatch('reloadAllFilters')
    dispatch('getHashForCurrentState')
  },
  reloadAllFilters({ commit, state, dispatch }) {
    const freeShownFilters = state.currentFacets
      .filter((filter) => !filter.locked)
      .filter((filter) => filter.show)
    for (let i = 0; i < freeShownFilters.length; i++) {
      const currentFilter = freeShownFilters[i]
      const filterKey = currentFilter.key
      dispatch('loadFilter', { filterKey })
    }
  },
  clearAllFilters({ commit, state, dispatch }) {
    for (let i = 0; i < state.currentFacets.length; i++) {
      const currentFilter = state.currentFacets[i]
      const filterKey = currentFilter.key
      commit('CLEAR_FILTERS_SELECTION', filterKey)
    }
    commit('SET_PAGE', 1)
    dispatch('loadCurrentPage')
    dispatch('clearAllselections')
    dispatch('reloadAllFilters')
    dispatch('getHashForCurrentState')
  },
  unlockAllFilters({ commit, state, dispatch }) {
    const freeFilters = state.currentFacets.filter((filter) => !filter.locked)
    for (let i = 0; i < freeFilters.length; i++) {
      const currentFilter = freeFilters[i]
      const filterKey = currentFilter.key
      commit('UNLOCK_FILTER', filterKey)
    }
  },
  setQuickSearchTerm({ commit, state, dispatch }, quickSearchTerm) {
    commit('SET_QUICK_SEARCH_TERM', quickSearchTerm)
    commit('SET_PAGE', 1)
    dispatch('loadCurrentPage')
    dispatch('clearAllselections')
    dispatch('reloadAllFilters')
    dispatch('getHashForCurrentState')
  },
  setAllExactTextFiltersFromState(
    { commit, state, dispatch },
    exactTextFilters
  ) {
    commit('SET_EXACT_TEXT_FILTERS_FROM_STATE', exactTextFilters)
  },
  setExactTextFilter(
    { commit, state, dispatch },
    { valueAccessor, filterValue }
  ) {
    commit('SET_EXACT_TEXT_FILTER', { valueAccessor, filterValue })
    commit('SET_PAGE', 1)
    dispatch('loadCurrentPage')
    dispatch('clearAllselections')
    dispatch('reloadAllFilters')
    dispatch('getHashForCurrentState')
  },
  unsetExactTextFilter({ commit, state, dispatch }, valueAccessor) {
    commit('UNSET_EXACT_TEXT_FILTER', valueAccessor)
    commit('SET_PAGE', 1)
    dispatch('loadCurrentPage')
    dispatch('clearAllselections')
    dispatch('reloadAllFilters')
    dispatch('getHashForCurrentState')
  },
  unsetAllExactTextFilters({ commit, state, dispatch }) {
    commit('UNSET_ALL_EXACT_TEXT_FILTERS')
    commit('SET_PAGE', 1)
    dispatch('loadCurrentPage')
    dispatch('clearAllselections')
    dispatch('reloadAllFilters')
    dispatch('getHashForCurrentState')
  },
}

export default actions
