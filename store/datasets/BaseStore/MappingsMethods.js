import ESProxyService from '@/web-components-submodule/services/ESProxyService.js'
import RequestNotifications from '@/web-components-submodule/utils/RequestNotifications.js'
import ErrorTracking from '~/web-components-submodule/tracking/ErrorTracking.js'

const methods = {
  loadMappingsForQuerystring({ commit, state, dispatch }) {
    const indexName = state.indexName
    const propertiesGroup = 'querystring'

    ESProxyService.getPopertyGroupConfiguration(indexName, propertiesGroup)
      .then((response) => {
        const mappings = response.data.properties.default
        commit('SET_MAPPINGS_FOR_QUERYSTRING', mappings)
        commit('SET_LOADED_MAPPINGS_FOR_QUERYSTRING', true)
      })
      .catch((error) => {
        ErrorTracking.trackError(error, this)

        RequestNotifications.dispatchRequestErrorNotification(
          error,
          dispatch,
          'There was an error while fetching the index mapping: '
        )
      })
  },
}

export default methods
