import SelectionModes from '@/web-components-submodule/standardisation/datasets/SelectionModes.js'
import JoinModes from '@/web-components-submodule/standardisation/datasets/JoinModes.js'

const defaultPageSizes = [5, 10, 20, 50, 100]

export default {
  getBaseState() {
    return {
      initialLoad: false,
      propertiesLoaded: false,
      facetsLoaded: false,
      allCurrentFiltersDataLoaded: false,
      contextualDataReady: false,
      contextObj: undefined,
      indexName: undefined,
      entityID: undefined,
      possibleJoinDestinations: [],
      joinMode: JoinModes.allItems, // 'all_items' or 'selection'
      searchTerm: undefined,
      query: {},
      initialQuery: {},
      initialFacetsState: [],
      propertiesGroups: {},
      propertiesIndex: {},
      currentView: 0,
      stateSaving: {
        useHashInURL: false,
        sateHash: undefined,
        loadingHash: true,
        b64State: undefined,
        expires: undefined,
      },
      facetsGroups: '',
      currentHeaders: [],
      currentFacets: [],
      pageLoading: false,
      page: 1,
      itemsPerPage: defaultPageSizes[2],
      pageCount: 0,
      currentItems: [],
      slots: [],
      searchResultsItemSlot: undefined,
      totalItems: 0,
      currentPageStart: 0,
      currentPageEnd: 0,
      possiblePageSizes: defaultPageSizes,
      sortBy: [],
      sortDesc: [],
      enableMultiSort: true,
      enableSimilarityMaps: false,
      similarityMaps: {
        show: false,
        referenceStructure: undefined,
      },
      enableSubstructureHighlight: false,
      substructureHighlight: {
        show: true,
        referenceStructure: undefined,
      },
      sortObject: {},
      pluralEntityName: '',
      querystringWasApplied: false,
      querystring: undefined,
      querystringToApply: '*',
      querystringExamples: [],
      loadedMappingsForQuerystring: false,
      mappingsForQuerystring: [],
      exactTextFilters: {},
      datasetJobs: {
        downloads: [
          // {
          //   id: 'DOWNLOAD-ZUN30QVgHRqQiPz5OTSK3hUu2mI5vU6JwewlAMGgyic=',
          //   linkToRawDetails:
          //     'https://www.ebi.ac.uk/chembl/interface_api/delayed_jobs/status/DOWNLOAD-ZUN30QVgHRqQiPz5OTSK3hUu2mI5vU6JwewlAMGgyic=',
          //   status: 'RUNNING',
          //   progress: '43',
          //   fileType: 'CSV',
          //   title: 'Generating CSV File...',
          // },
          // {
          //   id: 'DOWNLOAD-ZUN30QVgHRqQiPz5OTSK3hUu2mI5vU6JwewlAMGgyic=',
          //   linkToRawDetails:
          //     'https://www.ebi.ac.uk/chembl/interface_api/delayed_jobs/status/DOWNLOAD-ZUN30QVgHRqQiPz5OTSK3hUu2mI5vU6JwewlAMGgyic=',
          //   status: 'FINISHED',
          //   progress: '100',
          //   fileType: 'TSV',
          //   title: 'Generating TSV File...',
          //   fileUrl:
          //     'www.ebi.ac.uk/chembl/interface_api/delayed_jobs/outputs/DOWNLOAD-ZUN30QVgHRqQiPz5OTSK3hUu2mI5vU6JwewlAMGgyic=/DOWNLOAD-ZUN30QVgHRqQiPz5OTSK3hUu2mI5vU6JwewlAMGgyic=.zip',
          // },
          // {
          //   id: 'DOWNLOAD-ZUN30QVgHRqQiPz5OTSK3hUu2mI5vU6JwewlAMGgyic=',
          //   linkToRawDetails:
          //     'https://www.ebi.ac.uk/chembl/interface_api/delayed_jobs/status/DOWNLOAD-ZUN30QVgHRqQiPz5OTSK3hUu2mI5vU6JwewlAMGgyic=',
          //   status: 'ERROR',
          //   progress: '43',
          //   fileType: 'SDF',
          //   title: 'Generating SDF File...',
          // },
        ],
        linksToOtherEntities: [],
      },
      downloadFormats: {
        CSV: { id: 'CSV', processing: false },
        TSV: { id: 'TSV', processing: false },
      },
      downloadPropertiesGroup: 'download',
      enableSelection: false,
      enableStateSaving: true,
      enableHeatmapGeneration: false,
      enableQuickTextSearch: false,
      quickSearchTerm: undefined,
      quickSearchPriorities: {
        priorities: {},
        prioritiesLoaded: false,
      },
      heatmapGeneration: {
        generatingURL: false,
        URL: undefined,
      },
      selection: {
        allItemsSelected: false,
        someItemsSelected: false,
        selectionMode: SelectionModes.noItemsExcept,
        exceptions: [],
        numItemsSelected: 0,
        selectedInPage: [],
      },
      browseAll: {
        processingLink: false,
      },
      embedding: {
        idForParent: '',
      },
      debug: false,
    }
  },
}
