import RequestNotifications from '@/web-components-submodule/utils/RequestNotifications.js'
import LinksToBrowsers from '~/web-components-submodule/standardisation/LinksToBrowsers.js'
import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'
import DatasetStateMessages from '~/web-components-submodule/utils/DatasetStateMessages.js'
import ObjectPropertyAccess from '~/web-components-submodule/utils/ObjectPropertyAccess.js'
import ErrorTracking from '~/web-components-submodule/tracking/ErrorTracking.js'

const helperMethods = {
  getExportableState(state) {
    const stateToExport = {
      dataset: {
        entityID: state.entityID,
        initialQuery: state.initialQuery,
        facetsState: helperMethods.getFacetsState(state),
        customFiltering: helperMethods.getDatasetCustomFiltersState(state),
        subsetHumanDescription: state.subsetHumanDescription,
        searchTerm: state.searchTerm,
        quickSearchTerm: state.quickSearchTerm,
        exactTextFilters: state.exactTextFilters,
      },
    }
    return stateToExport
  },
  getB64ExportableState(state) {
    const stateToExport = helperMethods.getExportableState(state)
    const strState = JSON.stringify(stateToExport)
    const b64State = btoa(strState)
    return b64State
  },
  getFacetsState(state) {
    const atLeastOneFilterIsLocked =
      state.currentFacets.filter((filter) => filter.locked).length > 0

    if (!atLeastOneFilterIsLocked) {
      return null
    }

    return state.currentFacets.filter((filter) => filter.locked)
  },
  getFacetSelectedHistogramBuckets(currentFacet) {
    if (currentFacet.histogramData == null) {
      return null
    }
    const buckets = currentFacet.histogramData.buckets
    if (buckets == null) {
      return null
    }
    if (buckets.length === 0) {
      return null
    }
    const selectedBucketKeys = buckets
      .filter((bucket) => bucket.selected)
      .map((bucket) => bucket.key)

    return selectedBucketKeys
  },
  getFacetSpecificSelectedTerms(currentFacet) {
    if (currentFacet.specificTermsSelected == null) {
      return null
    }
    if (currentFacet.specificTermsSelected.length === 0) {
      return null
    }
    return currentFacet.specificTermsSelected
  },
  getFacetSelectedRange(currentFacet) {
    if (currentFacet.selectedRange.minSelection == null) {
      return null
    }
    if (currentFacet.selectedRange.maxSelection == null) {
      return null
    }
    return currentFacet.selectedRange
  },
  getDatasetCustomFiltersState(state) {
    if (state.querystring == null) {
      return null
    }
    return state.querystring
  },
  stateIsExportable(state) {
    const initialQuery = state.initialQuery
    let initialQueryEmpty = true
    if (initialQuery != null) {
      initialQueryEmpty = Object.keys(initialQuery).length === 0
    }

    const atLeastOneFilterIsLocked =
      state.currentFacets.filter((filter) => filter.locked).length > 0

    const facetsSelectionEmpty = !atLeastOneFilterIsLocked

    const customFilterIsEmpty =
      state.querystring == null || state.querystring === '*'

    const quickSearchTermIsEmpty =
      state.quickSearchTerm == null || state.quickSearchTerm === ''

    const extactTextFiltersAreEmpty = !Object.values(
      state.exactTextFilters
    ).some((value) => value != null)

    return (
      !initialQueryEmpty ||
      !facetsSelectionEmpty ||
      !customFilterIsEmpty ||
      !quickSearchTermIsEmpty ||
      !extactTextFiltersAreEmpty
    )
  },
}

const actions = {
  getHashForCurrentState({ commit, state, dispatch, rootState }) {
    const stateSavigIsDisabled = !state.enableStateSaving
    if (stateSavigIsDisabled) {
      return
    }

    const stateIsExportable = helperMethods.stateIsExportable(state)
    if (!stateIsExportable) {
      commit('SET_USE_HASH_IN_URL', false)
      const browserURL = LinksToBrowsers.buildURLForEntityBrowser(
        state.entityID,
        ''
      )
      commit('SET_B64_STATE', '')
      window.history.pushState({}, document.title, browserURL)
      return
    }
    const oldB64State = state.stateSaving.b64State
    const b64State = helperMethods.getB64ExportableState(state)
    const stateChanged = oldB64State !== b64State

    const currentlyInASearchPage =
      rootState.freeTextSearch == null
        ? false
        : rootState.freeTextSearch.currentlyInASearchPage

    if (currentlyInASearchPage) {
      const stateToExport = helperMethods.getExportableState(state)
      const entityID = state.entityID
      dispatch(
        'freeTextSearch/appendToSearchState',
        { datasetState: stateToExport, entityID, router: this.$router },
        { root: true }
      )
      return
    }
    // if state didn't change, don't attempt to get the hash. This is to reduce a unnecessary exceess of requests to the server.
    if (!stateChanged) {
      commit('SET_LOADING_STATE_HASH', false)
      commit('SET_USE_HASH_IN_URL', true)
      return
    }
    commit('SET_B64_STATE', b64State)
    commit('SET_LOADING_STATE_HASH', true)
    commit('SET_USE_HASH_IN_URL', true)
    ESProxyService.getHashForLongURL(b64State)
      .then((response) => {
        const hash = response.data.hash
        const expires = response.data.expires
        const browserURL = LinksToBrowsers.buildURLForEntityBrowser(
          state.entityID,
          hash
        )
        commit('SET_STATE_HASH', hash)
        commit('SET_STATE_EXPIRES', expires)
        window.history.pushState({}, document.title, browserURL)
        commit('SET_LOADING_STATE_HASH', false)
      })
      .catch((error) => {
        ErrorTracking.trackError(error, this)

        RequestNotifications.dispatchRequestErrorNotification(
          error,
          dispatch,
          'There was an error while processing the dataset state'
        )
      })
  },
  setStateSavingParams({ commit, state, dispatch }, stateSavingParams) {
    commit('SET_STATE_SAVING', stateSavingParams)
  },
  goToFullBrowser({ commit, state, dispatch, rootState }) {
    const b64State = helperMethods.getB64ExportableState(state)

    // Remember that now, instead of the full url, I get the hash for the b64State only
    commit('SET_PROCESSING_BROWSE_ALL_LINK', true)

    ESProxyService.getHashForLongURL(b64State)
      .then((response) => {
        const hash = response.data.hash
        commit('SET_PROCESSING_BROWSE_ALL_LINK', false)
        RequestNotifications.dispatchRequestInfoNotification(
          dispatch,
          'Query created, redirecting...'
        )

        const browserURL = LinksToBrowsers.buildURLForEntityBrowser(
          state.entityID,
          hash
        )

        const isEmbedded = ObjectPropertyAccess.getPropertyPalue(
          rootState,
          'embedding.isEmbedded',
          false
        )

        if (isEmbedded) {
          window.parent.location.href = browserURL
        } else {
          window.location.href = browserURL
        }
      })
      .catch((error) => {
        commit('SET_PROCESSING_BROWSE_ALL_LINK', false)
        ErrorTracking.trackError(error, this)

        RequestNotifications.dispatchRequestErrorNotification(
          error,
          dispatch,
          'There was a problem with building the query: '
        )
      })
  },
  exportUpdatedState({ commit, state, dispatch, rootState }) {
    const b64State = helperMethods.getB64ExportableState(state)
    if (rootState.embedding.isEmbedded) {
      DatasetStateMessages.sendStateUpdateMessage(
        state.embedding.idForParent,
        b64State
      )
    }
  },
}

export default actions
