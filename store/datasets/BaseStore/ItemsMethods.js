import RequestNotifications from '@/web-components-submodule/utils/RequestNotifications.js'
import ObjectPropertyAccess from '@/web-components-submodule/utils/ObjectPropertyAccess.js'
import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'
import PropertiesComponents from '~/web-components-submodule/standardisation/PropertiesComponents.js'
import ComponentSlotDescriptors from '~/web-components-submodule/standardisation/ComponentSlotDescriptors.js'
import FiltersViewNames from '~/web-components-submodule/standardisation/datasets/FiltersViewNames.js'
import ErrorTracking from '~/web-components-submodule/tracking/ErrorTracking.js'
import DefaultValues from '~/web-components-submodule/standardisation/DefaultValues.js'

const methods = {
  loadCurrentPage({ commit, state, dispatch }) {
    commit('SET_PAGE_LOADING', true)
    commit('SET_CURRENT_ITEMS', [])

    dispatch('buildSortObject')
    const query = methods.prepareQuery(state, commit)
    commit('SET_QUERY', query)

    const sortObject = state.sortObject

    const indexName = state.indexName
    const contextObj = state.contextObj
    const contextualSort = methods.getContextualSortData(sortObject)

    ESProxyService.getESData(indexName, query, contextObj, contextualSort)
      .then((response) => {
        const propertiesToLoad = methods.getAllPropertiesToLoadList(state)
        const propertiesGroups = state.propertiesGroups
        const searchTerm = state.searchTerm
        const page = state.page
        const itemsPerPage = state.itemsPerPage
        const parsedItems = methods.parseReceivedItems(
          response.data,
          propertiesToLoad,
          propertiesGroups,
          searchTerm,
          page,
          itemsPerPage
        )
        const slots = methods.getSlotsDescription(state, parsedItems)
        commit('SET_SLOTS', slots)
        commit('SET_CURRENT_ITEMS', parsedItems)
        methods.setItemsCounts({ commit, state, dispatch }, response.data)
        dispatch('calculateSelectedInPage')
        commit('SET_PAGE_LOADING', false)
      })
      .catch((error) => {
        ErrorTracking.trackError(error, this)

        RequestNotifications.dispatchRequestErrorNotification(
          error,
          dispatch,
          `There was an error while loading the items! `
        )
      })
  },
  getContextualSortData(sortObject) {
    if (!sortObject.containsContextualProperties) {
      return null
    }

    const firstSort = sortObject.sort[0]
    const key = Object.keys(firstSort)[0] // Get the key dynamically
    const value = firstSort[key]
    const contextualSort = { [`_context.${key}`]: value }
    return contextualSort
  },
  prepareQuery(state, commit) {
    // Make sure to regenerate the must and not carry rubbish from previous fetches
    let query = {
      query: {
        bool: {
          filter: [],
          should: [],
          must_not: [],
          must: [],
        },
      },
    }

    const initialQuery = JSON.parse(JSON.stringify(state.initialQuery))

    query = {
      ...query,
      ...initialQuery,
    }

    methods.sanitizeQuery(query)
    methods.addTotalHitsTrackingToQuery(query)
    methods.addPaginationToQuery(query, state)
    methods.addSourceToQuery(query, state)
    methods.addSortingToQuery(query, state)
    methods.addQuerystringToQuery(query, state, commit)
    methods.addFiltersToQuery(query, state, commit)
    methods.addQuickTextSearchToQuery(query, state, commit)
    methods.addExactTextFiltersToQuery(query, state, commit)
    return query
  },
  sanitizeQuery(query) {
    if (query.query.bool == null) {
      query.query.bool = {}
    }
    if (query.query.bool.filter == null) {
      query.query.bool.filter = []
    }
    if (query.query.bool.should == null) {
      query.query.bool.should = []
    }
    if (query.query.bool.must_not == null) {
      query.query.bool.must_not = []
    }
    if (query.query.bool.must == null) {
      query.query.bool.must = []
    }
  },
  addTotalHitsTrackingToQuery(query) {
    query.track_total_hits = true
  },
  addPaginationToQuery(query, state) {
    let size = state.itemsPerPage
    if (size < 0) {
      size = 0
    }

    let from = (state.page - 1) * state.itemsPerPage
    if (from < 0) {
      from = 0
    }

    query.size = size
    query.from = from
  },
  getAllPropertiesToLoadList(state) {
    const propertiesGroups = state.propertiesGroups
    const uniqueProps = new Set()

    for (const viewKey in propertiesGroups) {
      const viewConfig = propertiesGroups[viewKey]

      for (const propKey in viewConfig.allHeaders) {
        const propConfig = viewConfig.allHeaders[propKey]
        const isVirtual = propConfig.is_virtual
        let keyToAdd = isVirtual ? propConfig.based_on : propKey
        const isContextual = propConfig.is_contextual
        keyToAdd = isContextual ? `_context.${propKey}` : keyToAdd
        uniqueProps.add(keyToAdd)
      }
    }

    const propertiesToLoad = Array.from(uniqueProps)
    return propertiesToLoad
  },
  addSourceToQuery(query, state) {
    const propertiesToLoad = methods.getAllPropertiesToLoadList(state)
    query._source = propertiesToLoad
  },
  addSortingToQuery(query, state) {
    const sortObject = state.sortObject
    // only if there are no contextual properties they are added to the ES query, if there are contextual
    // properties, the sorting is handled in a different way by the backend.
    // When there are contextual properties, only single sorting should be allowed
    if (!sortObject.containsContextualProperties) {
      query.sort = sortObject.sort
    }
  },
  addQuerystringToQuery(query, state, commit) {
    const querystring = state.querystring
    if ([null, undefined, '', '*'].includes(querystring)) {
      commit('SET_QUERYSTRING_WAS_APPLIED', false)
      return
    }
    commit('SET_QUERYSTRING_WAS_APPLIED', true)
    query.query.bool.filter.push({
      query_string: {
        analyze_wildcard: true,
        query: querystring,
      },
    })
  },
  addQuickTextSearchToQuery(query, state, commit) {
    const quickSearchTerm = state.quickSearchTerm

    if ([null, undefined, '', '*'].includes(quickSearchTerm)) {
      return
    }

    const propertiesGroups = state.propertiesGroups
    const propsForQuickSearch = new Set()
    const priorities = state.quickSearchPriorities.priorities

    // Find the maximum priority
    const maxPriority = Math.max(...Object.values(priorities))

    // calculate boosts exponentially based on the priority
    const boosts = {}
    Object.entries(priorities).forEach(([key, priority]) => {
      boosts[key] = Math.pow(2, maxPriority - priority + 1)
    })

    Object.entries(boosts).forEach(([key, boost]) => {
      propsForQuickSearch.add(`${key}^${boost}`)
    })

    for (const viewKey in propertiesGroups) {
      const viewConfig = propertiesGroups[viewKey]

      for (const propKey in viewConfig.allHeaders) {
        if (propKey in boosts) {
          continue
        }

        const propConfig = viewConfig.allHeaders[propKey]
        const propType = propConfig.type

        const isVirtual = propConfig.is_virtual
        if (isVirtual) {
          propsForQuickSearch.add(
            `${propConfig.based_on}.*.alphanumeric_lowercase_keyword`
          )
          propsForQuickSearch.add(`${propConfig.based_on}.*.eng_analyzed`)
          propsForQuickSearch.add(`${propConfig.based_on}.*.std_analyzed`)
          propsForQuickSearch.add(`${propConfig.based_on}.*.ws_analyzed`)
          continue
        }

        const isObject = propType === 'object'
        if (isObject) {
          propsForQuickSearch.add(`${propKey}.*.alphanumeric_lowercase_keyword`)
          propsForQuickSearch.add(`${propKey}.*.eng_analyzed`)
          propsForQuickSearch.add(`${propKey}.*.std_analyzed`)
          propsForQuickSearch.add(`${propKey}.*.ws_analyzed`)
          continue
        }

        const isText = propType === 'string'
        if (isText) {
          propsForQuickSearch.add(`${propKey}`)
          continue
        }
      }
    }
    const parsedTerm = quickSearchTerm.replace(/^\*+/, '').replace(/\*+$/, '') // remove starting asterisks

    query.query.bool.filter.push({
      query_string: {
        fields: Array.from(propsForQuickSearch),
        query: `*${parsedTerm}*`,
      },
    })
  },
  addExactTextFiltersToQuery(query, state, commit) {
    const exactTextFilters = state.exactTextFilters
    for (const [propKey, value] of Object.entries(exactTextFilters)) {
      if (value === null) {
        continue
      } else if (value === DefaultValues.DefaultNullTextValue) {
        query.query.bool.filter.push({
          bool: {
            should: [
              {
                bool: {
                  must_not: {
                    exists: {
                      field: propKey,
                    },
                  },
                },
              },
            ],
          },
        })
      } else {
        query.query.bool.filter.push({
          term: {
            [propKey]: value,
          },
        })
      }
    }
  },
  addFiltersToQuery(query, state, commit) {
    const currentFacets = methods.getCurrentFacetsWithInitialState(
      state,
      commit
    )
    // filters are ANDed
    for (let i = 0; i < currentFacets.length; i++) {
      const currentFacet = currentFacets[i]
      const propId = currentFacet.prop_id

      const propType = currentFacet.property_config.type
      const currentView = currentFacet.currentView
      let currentFacetQuery
      if (['string', 'boolean'].includes(propType)) {
        if (currentView === FiltersViewNames.histogram) {
          currentFacetQuery = methods.getTermsFacetQuery(currentFacet, propId)
        } else if (currentView === FiltersViewNames.findTerm) {
          currentFacetQuery = methods.getSpecificTermsFacetQuery(
            currentFacet,
            propId
          )
        }
      }
      if (['integer', 'double'].includes(propType)) {
        if (currentView === FiltersViewNames.histogram) {
          currentFacetQuery = methods.getNumericHistogramFacetQuery(
            currentFacet,
            propId
          )
        } else if (currentView === FiltersViewNames.range) {
          currentFacetQuery = methods.getNumericRangeFacetQuery(
            currentFacet,
            propId
          )
        }
      }
      if (currentFacetQuery != null) {
        query.query.bool.filter.push(currentFacetQuery)
      }
    }
  },
  getCurrentFacetsWithInitialState(state, commit) {
    const currentFacets = state.currentFacets
    const initialFacetsState = state.initialFacetsState
    const unusedFacetsInitialState = initialFacetsState.filter(
      (filter) => !filter.consumedForFirstPageLoad
    )

    // facets config may not have been loaded yet, so I will just make a list of the facets that I know about
    const allFacetsPropIDs = unusedFacetsInitialState
      .map((filter) => filter.prop_id)
      .concat(currentFacets.map((filter) => filter.prop_id))

    const facetsForQuery = []
    for (const currentPropID of allFacetsPropIDs) {
      const facetFromInitialState = unusedFacetsInitialState.find(
        (aFilter) => aFilter.prop_id === currentPropID
      )

      if (facetFromInitialState != null) {
        facetsForQuery.push(facetFromInitialState)
        commit('SET_INITIAL_FACETS_STATE_WAS_CONSUMED_FOR_INITIAL_PAGE_LOAD', {
          filterKey: currentPropID,
          wasConsumed: true,
        })
      } else {
        const currentFacet = currentFacets.find(
          (aFilter) => aFilter.prop_id === currentPropID
        )
        facetsForQuery.push(currentFacet)
      }
    }

    return facetsForQuery
  },
  getTermsFacetQuery(currentFacet, propId) {
    const currentBuckets = currentFacet.histogramData.buckets
    if (currentBuckets == null) {
      return undefined
    }

    // Terms within a filter are ORed
    const facetBaseQuery = {
      bool: {
        should: [],
      },
    }
    let filtersApplied = false

    const bucketsNoOtherNoNA = currentBuckets.filter(
      (bucket) =>
        bucket.key !== 'Other' &&
        bucket.key !== currentFacet.histogramParams.nullValuesLabel
    )

    const nABucket = currentBuckets.find(
      (bucket) => bucket.key === currentFacet.histogramParams.nullValuesLabel
    )
    if (nABucket != null) {
      if (nABucket.selected) {
        facetBaseQuery.bool.should.push({
          bool: {
            must_not: {
              exists: {
                field: propId,
              },
            },
          },
        })
        filtersApplied = true
      }
    }

    const otherBucket =
      currentBuckets.find((bucket) => bucket.key === 'Other') || {}
    if (otherBucket.selected) {
      const nonSelectedBuckets = bucketsNoOtherNoNA.filter(
        (bucket) => !bucket.selected
      )
      const nonSelectedTerms = nonSelectedBuckets.map((bucket) => bucket.key)
      const othersQuery = {
        bool: {
          must_not: {
            terms: {
              [propId]: nonSelectedTerms,
            },
          },
        },
      }

      if (nABucket != null) {
        if (!nABucket.selected) {
          othersQuery.bool.must = []
          othersQuery.bool.must.push({
            exists: {
              field: propId,
            },
          })
        }
      }

      facetBaseQuery.bool.should.push(othersQuery)
      filtersApplied = true
    }

    const selectedBuckets = bucketsNoOtherNoNA.filter(
      (bucket) => bucket.selected
    )

    if (selectedBuckets.length > 0 && !otherBucket.selected) {
      let selectedTerms = selectedBuckets.map((bucket) => bucket.key)
      const propType = currentFacet.property_config.type
      if (propType === 'boolean') {
        selectedTerms = selectedTerms.map((term) => String(term) === '1')
      }

      facetBaseQuery.bool.should.push({
        terms: {
          [propId]: selectedTerms,
        },
      })
      filtersApplied = true
    }

    if (!filtersApplied) {
      return undefined
    }
    return facetBaseQuery
  },
  getSpecificTermsFacetQuery(currentFacet, propId) {
    const selectedTerms = currentFacet.specificTermsSelected
    if (selectedTerms == null) {
      return undefined
    }
    if (selectedTerms.length === 0) {
      return undefined
    }

    const termsQuery = {
      terms: {
        [propId]: selectedTerms,
      },
    }
    return termsQuery
  },
  getNumericRangeFacetQuery(currentFacet, propID) {
    const selectedRange = currentFacet.selectedRange
    const numericValuesData = currentFacet.numericValuesData

    const mustApplyFilter =
      selectedRange.minSelection > numericValuesData.minValue ||
      selectedRange.maxSelection < numericValuesData.maxValue

    if (!mustApplyFilter) {
      return undefined
    }
    const propType = currentFacet.property_config.type

    const rangeQuery = methods.getRangeQuery(
      selectedRange.minSelection,
      selectedRange.maxSelection,
      propID,
      propType
    )

    return rangeQuery
  },
  getNumericHistogramFacetQuery(currentFacet, propID) {
    const currentBuckets = currentFacet.histogramData.buckets
    if (currentBuckets == null) {
      return undefined
    }

    // Terms within a filter are ORed
    const facetBaseQuery = {
      bool: {
        should: [],
      },
    }

    const nullValuesLabel = currentFacet.histogramParams.nullValuesLabel
    const selectedBuckets = currentBuckets.filter((bucket) => bucket.selected)

    if (selectedBuckets.length === 0) {
      return undefined
    }
    for (let i = 0; i < selectedBuckets.length; i++) {
      const currentBucket = selectedBuckets[i]
      const propId = currentFacet.prop_id

      let bucketQuery
      if (currentBucket.key === nullValuesLabel) {
        bucketQuery = {
          bool: {
            must_not: {
              exists: {
                field: propId,
              },
            },
          },
        }
      } else {
        const propType = currentFacet.property_config.type

        bucketQuery = methods.getRangeQuery(
          currentBucket.minRangeKey,
          currentBucket.maxRangeKey,
          propId,
          propType
        )
      }

      facetBaseQuery.bool.should.push(bucketQuery)
    }

    return facetBaseQuery
  },
  getRangeQuery(min, max, propID, propType) {
    // This is to avoid a situation where it creates a query for x <=1 and x <1, which no document will match ever
    const exactValue = propType === 'integer' && max - min === 0

    if (exactValue) {
      return {
        term: {
          [propID]: {
            value: max,
          },
        },
      }
    }

    return {
      range: {
        [propID]: {
          gte: min,
          lt: max,
        },
      },
    }
  },
  parseReceivedItems(
    esResponse,
    propertiesToLoad,
    propertiesGroups,
    searchTerm,
    page,
    itemsPerPage
  ) {
    const rawItems = esResponse.es_response.hits.hits
    const parsedItems = []
    for (let i = 0; i < rawItems.length; i++) {
      const currentRawItem = rawItems[i]
      let rawHighlight = currentRawItem.highlight
      rawHighlight = rawHighlight == null ? {} : rawHighlight
      const highlight = methods.parseHighlight(
        rawHighlight,
        propertiesGroups,
        searchTerm
      )
      const parsedItem = {
        id: currentRawItem._id,
        data: {},
        highlight,
        itemNumber: `${itemsPerPage * (page - 1) + (i + 1)}.`,
      }
      for (let j = 0; j < propertiesToLoad.length; j++) {
        const currentProperty = propertiesToLoad[j]
        const currentValue = ObjectPropertyAccess.getPropertyPalue(
          currentRawItem._source,
          currentProperty
        )

        parsedItem.data[currentProperty] = currentValue
      }
      parsedItems.push(parsedItem)
    }
    return parsedItems
  },
  parseHighlight(rawHighlight, propertiesGroups, searchTerm) {
    const highlight = {}
    for (const [highlightedPropKey, highlightedPropValue] of Object.entries(
      rawHighlight
    )) {
      const propLabel = methods.findPropertyLabel(
        propertiesGroups,
        highlightedPropKey
      )
      const highlightDesc = {
        highlights: highlightedPropValue,
        propLabel,
        searchTerm,
      }
      highlight[highlightedPropKey] = highlightDesc
    }

    return highlight
  },
  findPropertyLabel(propertiesGroups, propID) {
    for (const viewKey in propertiesGroups) {
      const viewConfig = propertiesGroups[viewKey]
      const propConfig = viewConfig.allHeaders[propID]

      if (propConfig != null) {
        return propConfig.text
      }
    }

    return propID
  },
  getSlotsDescription(state, parsedItems) {
    const currentHeaders = state.currentHeaders
    const indexName = state.indexName
    const entityID = state.entityID
    const slots = []

    for (let i = 0; i < currentHeaders.length; i++) {
      const currentHeader = currentHeaders[i]
      const valueAccessor = currentHeader.value
      const propConfig = state.propertiesGroups[1].allHeaders[valueAccessor]
      const propertyComponent = PropertiesComponents.methods.getSlotForProperty(
        entityID,
        valueAccessor
      )

      const descriptorGeneratorFunction = ComponentSlotDescriptors.methods.getGeneratorFunctionForPoperty(
        entityID,
        valueAccessor
      )

      const slotDescription = {
        componentName: propertyComponent,
        slotID: `item.${valueAccessor}`,
        slotParams: descriptorGeneratorFunction({
          indexName,
          valueAccessor,
          highlightText: state.searchTerm || state.quickSearchTerm,
          propConfig,
        }),
      }

      slots.push(slotDescription)
    }
    return slots
  },
  setItemsCounts({ commit, state, dispatch }, responseData) {
    const totalItems = responseData.es_response.hits.total.value
    commit('SET_TOTAL_ITEMS', totalItems)

    const currentPageStart = (state.page - 1) * state.itemsPerPage + 1
    commit('SET_CURRENT_PAGE_START', currentPageStart)

    const numItemsReceived = responseData.es_response.hits.hits.length
    const currentPageEnd = currentPageStart + numItemsReceived - 1
    commit('SET_CURRENT_PAGE_END', currentPageEnd)

    const pageCount = Math.ceil(totalItems / state.itemsPerPage)
    commit('SET_PAGE_COUNT', pageCount)
  },
  loadNextPage({ commit, state, dispatch }) {
    const newPageNum = state.page
    commit('SET_PAGE', newPageNum)
    commit('SET_CURRENT_ITEMS', [])
    methods.loadCurrentPage({ commit, state, dispatch })
  },
  loadPreviousPage({ commit, state, dispatch }) {
    const newPageNum = state.page
    commit('SET_PAGE', newPageNum)
    methods.loadCurrentPage({ commit, state, dispatch })
  },
  setPage({ commit, state, dispatch }, pageNumber) {
    commit('SET_PAGE', pageNumber)
    methods.loadCurrentPage({ commit, state, dispatch })
  },
  setPageSize({ commit, state, dispatch }, pageSize) {
    commit('SET_ITEMS_PER_PAGE', pageSize)
    commit('SET_PAGE', 1)
    methods.loadCurrentPage({ commit, state, dispatch })
  },
}

export default methods
