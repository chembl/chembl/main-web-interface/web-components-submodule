import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'
import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'
import LinksToEntities from '~/web-components-submodule/standardisation/LinksToEntities.js'

const methods = {
  getEntityIDFromReportCardPath(pageRoute) {
    // removes trailing slash first
    const cleanPageRoute = pageRoute.replace(/\/$/, '')

    const pageRouteParts = cleanPageRoute.split('/')
    const entityText = pageRouteParts[pageRouteParts.length - 2]

    switch (entityText) {
      case 'tissue':
        return EntityNames.Tissue.entityID
      case 'cell_line':
        return EntityNames.CellLine.entityID
      case 'document':
        return EntityNames.Document.entityID
      case 'assay':
        return EntityNames.Assay.entityID
      case 'target':
        return EntityNames.Target.entityID
      case 'compound':
        return EntityNames.Compound.entityID
      default:
        return null
    }
  },
  getEntityIDFromLookupEntityType(entityType) {
    switch (entityType) {
      case 'TISSUE':
        return EntityNames.Tissue.entityID
      case 'CELL':
        return EntityNames.CellLine.entityID
      case 'TARGET':
        return EntityNames.Target.entityID
      case 'DOCUMENT':
        return EntityNames.Document.entityID
      case 'ASSAY':
        return EntityNames.Assay.entityID
      case 'COMPOUND':
        return EntityNames.Compound.entityID
      default:
        return null
    }
  },
  async getIDDetails(chemblID) {
    try {
      const lookupResponse = await ESProxyService.getLookupData(chemblID)
      const lookupData = lookupResponse.data._source
      return {
        exists: true,
        downgraded: lookupData.status !== 'ACTIVE',
        entityID: this.getEntityIDFromLookupEntityType(lookupData.entity_type),
      }
    } catch (e) {
      if (e.response.status === 404) {
        return {
          exists: false,
        }
      }
      throw e
    }
  },
  redirectIfWrongEntity(entityID, idDetails, chemblID, redirect) {
    if (entityID !== idDetails.entityID) {
      redirect(`/id_lookup/${chemblID}`)
    }
  },
  redirectIfNonStandardID(chemblID, entityID, redirect) {
    const isChEMBLID = /^CHEMBL\d+$/.test(chemblID.toUpperCase())
    if (!isChEMBLID) {
      return
    }
    // check if the id contains lowercase letters
    const containsLowercaseLetters = /[a-z]/.test(chemblID)
    if (containsLowercaseLetters) {
      const standardURL = LinksToEntities[entityID].getLinkToReportCard(
        chemblID.toUpperCase()
      )
      redirect(standardURL)
    }
  },
  redirectIfDowngraded(idDetails, entityID, chemblID, redirect, error) {
    if (!idDetails.exists) {
      error({
        statusCode: 404,
        message: `No ${entityID.singularEntityName} found with id ${chemblID}`,
      })
      return
    }
    if (idDetails.downgraded) {
      redirect(`/id_lookup/${chemblID}`)
    }
  },
}

export default methods
